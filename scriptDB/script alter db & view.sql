// CHECKPOINT 26072019
/*
	change field type & add new field
*/
ALTER TABLE `tsm_live`.`timesheets` 
ADD COLUMN `custname` VARCHAR(50) NULL AFTER `customer_id`
ADD COLUMN `pic` VARCHAR(45) NULL default null AFTER `custname`,
CHANGE COLUMN `customer_id` `customer_id` VARCHAR(10) NULL DEFAULT NULL ;


// CHECKPOINT 27072019
/*
change rpt_timesheet view query
*/
SELECT 
        `a`.`id` AS `id`,
        `b`.`username` AS `username`,
        CONCAT(`b`.`firstname`, ' ', `b`.`lastname`) AS `fullname`,
        `a`.`start_time` AS `start_time`,
        `a`.`end_time` AS `end_time`,
        `a`.`end_time_edit` AS `end_time_edit`,
        `a`.`location_name` AS `location_name`,
        `a`.`location_name_end` AS `location_name_end`,
        `a`.`desc` AS `desc`,
        IFNULL(`a`.`customer_id`, `b`.`custid`) AS `CustId`,
        IFNULL(`a`.`custname`, `b`.`custname`) AS `CustName`,
        `a`.`km_awal` AS `km_awal`,
        `a`.`km_akhir` AS `km_akhir`,
        `a`.`out_of_town` AS `out_of_town`,
        `a`.`town` AS `town`,
        `a`.`nopol` AS `nopol`,
        `a`.`driver_gs` AS `driver_gs`,
        `a`.`type_of_use` AS `type_of_use`,
        `a`.`approved_by` AS `approved_by`,
        `a`.`approved_time` AS `approved_time`,
        `a`.`created_time` AS `created_time`,
        `a`.`updated_time` AS `updated_time`,
        `a`.`survey` AS `score`,
        IFNULL(`a`.`pic`, `b`.`pic`) AS `pic`,
        `c`.`platform` AS `platform`,
        `c`.`version` AS `version`,
        `c`.`manufacturer` AS `manufacturer`,
        `c`.`model` AS `model`
    FROM
        ((`timesheets` `a`
        LEFT JOIN `users` `b` ON (`a`.`user_id` = `b`.`id`))
        LEFT JOIN `timesheets_device` `c` ON (`a`.`id` = `c`.`id_timesheet`))
		

/*
change rpt_timesheet_2 view query
*/
SELECT 
        `a`.`id` AS `id`,
        `b`.`username` AS `username`,
        CONCAT(`b`.`firstname`, ' ', `b`.`lastname`) AS `fullname`,
        `a`.`start_time` AS `start_time`,
        IFNULL(`a`.`end_time_edit`, `a`.`end_time`) AS `end_time`,
        `a`.`desc` AS `desc`,
        IFNULL(`a`.`customer_id`, `b`.`custid`) AS `CustId`,
        IFNULL(`a`.`custname`, `b`.`custname`) AS `CustName`,
        `a`.`km_awal` AS `km_awal`,
        `a`.`km_akhir` AS `km_akhir`,
        `a`.`out_of_town` AS `out_of_town`,
        `a`.`town` AS `town`,
        `a`.`nopol` AS `nopol`,
        `a`.`driver_gs` AS `driver_gs`,
        `a`.`type_of_use` AS `type_of_use`,
        `a`.`approved_by` AS `approved_by`,
        `a`.`approved_time` AS `approved_time`,
        `a`.`updated_time` AS `updated_time`,
        `a`.`survey` AS `score`,
        IFNULL(`a`.`pic`, `b`.`pic`) AS `pic`,
        IF(`a`.`not_approve_time` IS NULL
                AND `a`.`edit_admin_time` IS NULL,
            0,
            IF(`a`.`not_approve_time` IS NOT NULL,
                IF(`a`.`edit_admin_time` IS NOT NULL,
                    IF(`a`.`not_approve_time` > `a`.`edit_admin_time`,
                        1,
                        2),
                    1),
                2)) AS `status`,
        `a`.`not_approve_by` AS `not_approve_by`,
        `a`.`not_approve_desc` AS `not_approve_desc`,
        `a`.`not_approve_time` AS `not_approve_time`,
        `a`.`edit_admin_by` AS `edit_admin_by`,
        `a`.`edit_admin_time` AS `edit_admin_time`,
        `b`.`user_type` AS `user_type`
    FROM
        (`timesheets` `a`
        LEFT JOIN `users` `b` ON (`a`.`user_id` = `b`.`id`))




/*
change rpt_timesheet_print view query
*/
SELECT 
        `a`.`id` AS `id`,
        `b`.`username` AS `username`,
        CONCAT(`b`.`firstname`, ' ', `b`.`lastname`) AS `fullname`,
        `a`.`start_time` AS `start_time`,
        IFNULL(`a`.`end_time_edit`, `a`.`end_time`) AS `end_time`,
        `a`.`desc` AS `desc`,
        IFNULL(`a`.`custname`, `b`.`custname`) AS `CustName`,
        `a`.`km_awal` AS `km_awal`,
        `a`.`km_akhir` AS `km_akhir`,
        `a`.`out_of_town` AS `out_of_town`,
        `a`.`town` AS `town`,
        `a`.`nopol` AS `nopol`,
        `a`.`driver_gs` AS `driver_gs`,
        `a`.`type_of_use` AS `type_of_use`,
        `a`.`approved_by` AS `approved_by`,
        `a`.`approved_time` AS `approved_time`,
        `a`.`updated_time` AS `updated_time`,
        IFNULL(`a`.`pic`, `b`.`pic`) AS `pic`
    FROM
        (`timesheets` `a`
        LEFT JOIN `users` `b` ON (`a`.`user_id` = `b`.`id`))