INSERT INTO `language` (`id`, `key`, `indonesia`, `english`, `japan`, `datetime`)
VALUES
	(85, 'menyetujui', 'Menyetujui', 'Approved', '承認する場合、', '2019-11-17 13:39:33'),
	(86, 'tdkMenyetujui', 'Tidak Menyetujui', 'Not Approved', '承認しない場合、', '2019-11-17 13:40:22'),
	(87, 'tdkKonek', 'Error, Tidak bisa terhubung ke jaringan', 'Error, Cannot connect to network', 'エラー\r\nネットワークに接続できません', '2019-11-17 13:43:09'),
    (88, 'reason1', 'Waktu absensi masuk tidak sesuai', 'Clock in time is incorrect', '開始時間が誤っている', '2019-11-18 15:17:26'),
    (89, 'reason2', 'Waktu absensi pulang tidak sesuai', 'Clock out time is incorrect', '終了時間が誤っている', '2019-11-18 15:17:26'),
    (90, 'reason3', 'Keterangan keluar kota salah', 'Out of Town information is incorrect', 'JABODETABEK外を走行された場合の情報に誤りがある', '2019-11-18 15:17:26'),
    (91, 'reason4', 'Driver ini bukan driver saya', 'This is not my driver', '運転手の氏名に誤りがある', '2019-11-18 15:17:26'),
    (92, 'reason5', 'Plat nomor salah', 'License Plate Number is incorrect', 'ライセンスプレート番号（ナンバープレート）に誤りがある', '2019-11-18 15:17:26'),
    (93, 'reason6', 'Kilometer tidak sesuai', 'Mileage Record is incorrect', '走行距離（Km）の入力に誤りがある', '2019-11-18 15:17:26');
    (94, 'reason7', 'Lain-lain', 'Others', 'その他', '2019-11-18 15:17:26');

delete `language` where `id` = 67

INSERT INTO `language` (`id`, `key`, `indonesia`, `english`, `japan`, `datetime`)
VALUES
	(67, 'tdksetujub', 'Terima kasih atas konfirmasi Anda. Timesheet akan kami revisi sesuai input user dalam 24 jam', 'Thank you for your confirmation. Timesheet will be revised according to your input within 24 hours', 'ご登録有難うございます。頂きました情報を24時間以内に修正させて頂きます。', '2018-06-11 16:21:44');
