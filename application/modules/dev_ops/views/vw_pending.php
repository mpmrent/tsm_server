<style>
    thead tr th {
        text-align: center;
    }
    
    /*tr {
        white-space: pre;
    }
    */
    .btn-floating {
        position: fixed; 
        bottom: 20px; 
        z-index: 100002; 
        right: 20px; 
        margin-bottom: 5px; 
        margin-top: -40px;
        -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        border-radius: 10px;
    }
    
    .fade.in {
        background: rgba(0, 0, 0, 0.4);
    }
    
    .input-mini {
        width: 85%;
        margin-right: 10px;
    }

    .radio-toolbar input[type="radio"] {
        display: none;
      }
      
      .radio-toolbar label {
        display: inline-block;
        padding: 2px 4px;
        font-family: Arial;
        font-size: 16px;
        cursor: pointer;
        width: 19.5%;
      }
      
      .radio-toolbar input[type="radio"]:checked + label {
        /*background-color: #bbb;*/
        clear: none;
      }

      label > img {
          width: 100%;
          height: 100%;
      }

      lebel
</style>

<div class="x_content">
<!--    <div class="btn-floating">
        <a onclick="return add()" class="btn btn-app btn-primary btn-xs">
            <i class="ace-icon fa fa-edit bigger-160"></i>
            Add
        </a>
    </div>-->
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="widget-box">
                <div class="widget-header widget-header-flat widget-header-small">
                    <h5 class="widget-title">
                        <i class="ace-icon fa fa-filter"></i>
                        Filter
                    </h5>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <!--<form id="formX" action="<?php echo base_url() ?>index.php/dev_ops/main/timesheets" method="get" class="form-horizontal form-label-left">-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="text" id="form-field-1" value="<?php echo (isset($_GET['driver'])) ? $_GET['driver'] : ''; ?>" name="driver" placeholder="Driver" class="col-xs-12">
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                            <input class="form-control" name="daterange" value="<?php echo (isset($_GET['daterange'])) ? $_GET['daterange'] : ''; ?>" placeholder="Date Range" type="text" id="id-date-range-picker-1" />
                                        </div>
                                    </div>
                                    <div class="col-md-5" style="text-align: right;">
                                        <a id="btn-clear" href="#" class="btn btn-default">Bersihkan</a>
                                        <a id="btn-filter" href="#" class="btn btn-success">Proses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php 
                $this->table->set_template(array('table_open' => '<table style="width: 100%;" id="timesheets" class="table table-striped table-bordered dataTable no-footer table-hover">'));
                $this->table->set_heading($thead);
                echo $this->table->generate($table);
            ?>
        </div>
    </div>
</div>


<div id="target" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="frmTarget" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modalTitle">Small modal</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Absen Pulang</label>
                                <input type="datetime-local" class="form-control" name="end_time" placeholder="Check Out Time">
                            </div>
                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea class="form-control" name="desc"></textarea> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Proses</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modal-not-approve" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="frmNotApprove" action="" method="post" onsubmit="return validateUnapproveForm();">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="modalTitle">Konfirmasi</h4>
                    <p>Masukan alasan mengapa anda tidak menyetujui, keterangan akan langsung diterima oleh admin kami.</p>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Alasan </label>
                                <select class="form-control __web-inspector-hide-shortcut__" name="not_approve_type" required="true" id="not_approve_type">
                                    <option value=""></option>
                                    <?php
                                        foreach ($notApproveType as $idx => $value) {
                                            echo "<option value='$idx'>".$value."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row row_optional" id="row_keterangan">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Keterangan </label>
                                <textarea class="form-control" id="not_approve_desc" name="not_approve_desc"></textarea> 
                            </div>
                        </div>
                    </div>
                    <!-- Cancelled request 30102019
                    <div class="row row_optional" id="row_absen_masuk">
                        <label class="col-sm-3 control-label" for="form-field-1"> Absen Masuk </label>
                        <div class="col-sm-9">
                            <input name="start_time" id="absen_masuk" placeholder="Absen Masuk" class="col-xs-12">
                        </div>
                    </div>
                    <div class="row row_optional" id="row_absen_pulang">
                        <label class="col-sm-3 control-label" for="form-field-1"> Absen Pulang </label>
                        <div class="col-sm-9">
                            <input name="end_time" id="absen_pulang" placeholder="Absen Pulang" class="col-xs-12">
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Proses</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready( function(){
        // alert(<?php echo json_encode($this->session->userdata('user_type')) ?>);
        function generateEditable(id, driver, daterange){
            $('#'+id).DataTable({
                destroy: true,
                "ajax": "<?php echo base_url(); ?>index.php/dev_ops/main/get_timesheets/pending?driver="+driver+"&daterange="+daterange,
                "deferRender": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv'
                ],
                responsive: true,  
                "columnDefs": [
                    { 
                        "targets": 19,
                        "render": function(data, type, row, meta){
                            var app = '';
                            if (row[4] == null) {
                                if (('<?php echo $this->session->userdata('user_type') ?>' == 'admin') || ('<?php echo $this->session->userdata('user_type') ?>' == 'administrator')) {
                                    app = '<a href="#" onclick="setModalTarget('+row[0]+', '+row[0]+')" data-toggle="modal" data-target="#target"><span class="label label-sm label-success arrowed arrowed-right"><span class="fa fa-clock-o"></span> Absen Pulang </span></a>';
                                }
                            } else {
                                app = '<a href="#" onclick="setModalNotApprove('+row[0]+')" data-toggle="modal" data-target="#modal-not-approve"><span class="label label-sm label-default arrowed arrowed-right"><span class="fa fa-close"></span> tidak disetujui | 却下 </span></a>';
                                app = app + '<a href="#" onclick="showPopUpConfirm(\'Confirmation\', \'\', \'<?php echo site_url()?>/dev_ops/main/pending/approve/'+row[0]+'/<?php echo (!isset($type)) ? '' : $type;?>\');"><span class="label label-sm label-primary arrowed arrowed-right"><span class="fa fa-check"></span> disetujui </span></a>';
                            }
                           return '<p style="text-align:center;">' + app + '</p>';  
                        }
                    }           
                ]    
            });
            
            $('[name="'+id+'_length"]').addClass("form-control input-sm");
            $('[type="search"][aria-controls="'+id+'"]').addClass("form-control input-sm");
        }
        generateEditable('timesheets', '', '');
        
        $('#btn-filter').click( function() {
            var driver = $('[name="driver"]').val();
            var daterange = $('[name="daterange"]').val();
            generateEditable('timesheets', driver, daterange);
        });
        
        $('.date-picker').datepicker({
                autoclose: true,
                todayHighlight: true
        });

        $('#not_approve_type').on('change', function () {
            var val = this.value;
            $('.row_optional').hide();
            $('#not_approve_desc').text('');
            if (val=='reason7') {
                $('#row_keterangan').show();
            }
        });

        $('#absen_masuk').datetimepicker().next().on(ace.click_event, function(){
            $(this).prev().focus();
        });
        
        $('#absen_pulang').datetimepicker().next().on(ace.click_event, function(){
            $(this).prev().focus();
        });
        
        $('.input-daterange').datepicker({autoclose:true});
        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('#id-date-range-picker-1').daterangepicker({
                'applyClass' : 'btn-sm btn-success',
                'cancelClass' : 'btn-sm btn-default',
                locale: {
                        applyLabel: 'Apply',
                        cancelLabel: 'Cancel',
                }
        })
        .prev().on(ace.click_event, function(){
                $(this).next().focus();
        });

        <?php if (($this->session->flashdata('message')['status'] != null)) { ?>
            showPopUp(
                    'Information',
                    '<?php echo $this->session->flashdata('message')['comment'] ?>',
                    '<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Ok</button>'
                );
        <?php } ?>
    });


    $('#btn-clear').click(function() {
        $('[name="driver"]').val('');
        $('[name="daterange"]').val('');
//        $('#formX').find("input[type=text], select").val("");
    });


    function setModalTarget(id, modalTitle) {
        $('#modalTitle').html('Check Out ('+modalTitle+')');
        $('#frmTarget').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/pending/check_out/'+id);
        // $.ajax({
        //     type: 'post',
        //     url: '<?php echo base_url() ?>index.php/dev_ops/main/pending/check_out/'+id,
        //     dataType: 'json',
        //     success: function (data) {
        //         $('[name="target1"]').val(data.target1);
        //         $('[name="target2"]').val(data.target2);
        //     }
        // });
    }

    function setModalNotApprove(id) {
        $('#not_approve_type').val('');
        $('#not_approve_desc').val('');
        $('.row_optional').hide();
        $('#frmNotApprove').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/not_approved/'+id);
    }

    function validateUnapproveForm() {
        var type = $('#not_approve_type').val();
        var desc = $('#not_approve_desc').val();
        console.log('type:'+type+', desc:'+desc);
        var result = true;
        if (type=='reason7' && (desc=='' || desc==null)) {
            alert('Silakan sertakan detil alasan pada kolom keterangan.');
            result = false;
        }
        return result;
    }
</script>