<style>
    thead tr th {
        text-align: center;
    }
    
    tr {
        white-space: pre;
    }
    
    .btn-floating {
        position: fixed; 
        bottom: 20px; 
        z-index: 1002; 
        right: 20px; 
        margin-bottom: 5px; 
        margin-top: -40px;
        -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        border-radius: 10px;
    }

    .chosen-container {
        width: 100% !important;
    }
    
    .fade.in {
        background: rgba(0, 0, 0, 0.4);
    }

    .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
        background: none;
    }
</style>

<div class="x_content">
    <div class="btn-floating">
        <a onclick="return add()" class="btn btn-app btn-primary btn-xs">
            <i class="ace-icon fa fa-edit bigger-160"></i>
            Tambah
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
        <form action="<?php echo base_url();?>index.php/dev_ops/main/update_pic" method="post" enctype="multipart/form-data">
            <div class="container">
                <div class="row" style="margin-bottom: 20px;">
                    <input class="btn btn-default col-md-4" placeholder="Update PIC" style="margin-right: 10px;" type="file" name="file"/>
                    <input class="btn btn-success col-md-2" type="submit" value="Proses"/>
                </div>
            </div>
        </form>
            <?php if ($this->session->flashdata('success') > -1) { ?>
                <div class="alert alert-success">
                  <strong>Infomasi!</strong> <br> (<?php echo $this->session->flashdata('success') ?>) data berhasil diubah.
                </div>
            <?php } ?>
            <?php 
                $this->table->set_template(array('table_open' => '<table style="width: 100%;" id="users" class="table table-bordered table-hover">'));
                $this->table->set_heading($thead);
                echo $this->table->generate($table);
            ?>
        </div>
        <div class="col-md-12">
            <div class="modal fade" style="z-index: 105110;" id="modalForm" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <form id="modalFormX" action="<?php echo base_url() ?>index.php/report/carapps/car_menu_priv/edit" method="post" class="form-horizontal form-label-left">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="modalTitle">
                                    Tambah Pengguna
                                </h4>
                            </div>
                            <input style="display: none;" name="id" value="">
                            <div class="modal-body" id="menu-priv">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Nama Pengguna </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="username" placeholder="Username" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Kata sandi </label>
                                    <div class="col-sm-9">
                                        <input type="password" id="form-field-1" name="password" placeholder="Password" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Tipe Pengguna </label>
                                    <div class="col-sm-9">
                                        <select class="form-control __web-inspector-hide-shortcut__" name="user_type" id="form-field-select-1">
                                            <option value="">~ Select User Type ~</option>
                                            <option value="driver">Driver</option>
                                            <!-- <option value="supervisor">Supervisor</option>
                                            <option value="customer">Customer</option> -->
                                            <option value="pic">PIC</option>
                                            <option value="superpic">Super PIC</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama Depan </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="firstname" placeholder="First Name" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama Belakang </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="lastname" placeholder="Last Name" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ID Pelanggan </label>
                                    <div class="col-sm-9">
                                        <select class="chosen-select form-control tag-input-style" id="form-field-select-3" name="CustId"  data-placeholder="Select Customers">
                                        <!-- <select class="form-control __web-inspector-hide-shortcut__" name="CustId" id="form-field-select-1"> -->
                                            <option value="">~ Pilih Pelanggan ~</option>
                                            <?php echo json_encode($custs->result()); foreach ($custs->result() as $cust) { ?>
                                                <option value="<?php echo $cust->OLDCODE ?>"><?php echo $cust->OLDCODE . ' - ' . $cust->CUSTNAME ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="debPIC">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> PIC Id </label>
                                    <div class="col-sm-9">
                                        <select class="form-control __web-inspector-hide-shortcut__" name="pic" id="form-field-select-1">
                                        <!-- <select class="chosen-select form-control" id="form-field-select-3" name="pic"  data-placeholder="Select PIC"> -->
                                            <option value="">~ Pilih PIC ~</option>
                                            <?php foreach ($pics->result() as $pic) { ?>
                                                <option class="pic" style="display: none;" debitor="<?php echo $pic->custid ?>" value="<?php echo $pic->username ?>"><?php echo $pic->username . ' - ' . $pic->firstname . ' ' . $pic->lastname  ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Status </label>
                                    <div class="col-sm-9">
                                        <select class="form-control __web-inspector-hide-shortcut__" name="status" id="form-field-select-1">
                                            <option value="active">active</option>
                                            <option value="inactive">inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group right">
                                    <div class="col-md-12">
                                        <button data-dismiss="modal" class="btn btn-default">Batal</button>
                                        <button id="edt_btn" type="submit" class="btn btn-success">Proses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="modal fade" id="modalPin" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="modalTitlePin">PIN</h4>
                        </div>
                        <div class="modal-body" id="modal-pin"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function(){
        if(!ace.vars['touch']) {
            $('.chosen-select').chosen({allow_single_deselect:true}); 
            //resize the chosen on window resize
    
            $(window)
            .off('resize.chosen')
            .on('resize.chosen', function() {
                $('.chosen-select').each(function() {
                     var $this = $(this);
                     $this.next().css({'width': $this.parent().width()});
                })
            }).trigger('resize.chosen');
            //resize chosen on sidebar collapse/expand
            $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
                if(event_name != 'sidebar_collapsed') return;
                $('.chosen-select').each(function() {
                     var $this = $(this);
                     $this.next().css({'width': $this.parent().width()});
                })
            });
        }

        $('[name="user_type"]').change(function() {
            if ($(this).val() == 'driver') {
                $('#debPIC').show();
            } else {
                $('#debPIC').hide();
            }
            $('[name="pic"]').val('');
        });

   

        function generateEditable(id, target, url, window){
            $('#'+id).DataTable({
                "ajax": "<?php echo base_url(); ?>/index.php/dev_ops/main/get_table/users",
                "deferRender": true,
                dom: 'Bfrtip',
                responsive: true,  
                buttons: [
                    'copy', 'csv', 'print'
                ],
                "columnDefs": [
                    { 
                        "targets": target,
                        "render": function(data, type, row, meta){
                            var edt = '<a href="#" onclick="return row_edit('+row[0]+')"><span class="label label-sm label-success arrowed arrowed-right"><span class="fa fa-edit"></span> ubah</span></a> ';
                            var del = '<a href="#" onclick="showPopUpConfirm(\'Confirmation\', \'Are you sure ?\', \'<?php echo site_url()?>/'+url+'/delete/'+row[0]+'/<?php echo (!isset($type)) ? '' : $type;?>\');"><span class="label label-sm label-'+((row[12] == 'inactive') ? 'primary' : 'danger')+' arrowed arrowed-right"><span class="fa fa-trash-o"></span> '+((row[12] == 'inactive') ? 'active' : 'inactive')+'</span></a>';
                           return '<p style="text-align:center;">' + edt + del + '</p>'; 
                        }
                    },
                    { 
                        "targets": 3,
                        "render": function(data, type, row, meta){
                           return '<a href="#" onclick="return view_pin(\''+row[3]+'\', \''+row[1]+'\')">'+row[3]+'</a>'; 
                        }
                    }             
                ]     
            });
            
            $('[name="'+id+'_length"]').addClass("form-control input-sm");
            $('[type"search"][aria-controls="'+id+'"]').addClass("form-control input-sm");
        };
        
        generateEditable('users',14, 'dev_ops/main/users');
    
     });
    $('[type="submit"]#edt_btn').click(function(e) {
        document.getElementById('modalFormX').submit(); 
    });

    $('[name="CustId"]').change( function() {
        $('.pic').each(function( index ) {
            $(this).hide();
        });

        $('[debitor="'+ $(this).val() +'"]').each(function( index ) {
            $(this).show();
        });
        $('[name="pic"]').val('');
    });
    
    function row_edit(id) {
        $('[name="username"]').attr('readonly', 'true');
        $('#modalTitle').html('Edit User');
        $('#modalFormX').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/users/update/'+id);
        $.ajax({
            type: 'post',
            url: '<?php echo base_url() ?>/index.php/dev_ops/main/users/edit/'+id,
            success: function (data) {                
                $('[name="username"]').val(data.username);
                $('[name="password"]').val(data.password);
                $('[name="user_type"]').val(data.user_type).change();
                $('[name="firstname"]').val(data.firstname);
                $('[name="lastname"]').val(data.lastname);
                $('[name="CustId"]').chosen().val(data.custid).change();
                $('[name="CustId"]').trigger('chosen:updated');

                if (data.user_type == 'driver') {
                    $('#debPIC').show();
                    $('[name="pic"]').val(data.pic).change();
                } else {
                    $('#debPIC').hide();
                    $('[name="pic"]').val('').change();
                }
                $('[name="status"]').val(data.status).change();
                $('#modalForm').modal('show');
                
            }
        });
    }
    
    function add(id) {
        $('#modalFormX').find("input[type=text], input[type=password], select").val("");
        $('#modalTitle').html('Add User');
        $('#modalFormX').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/users/add');
        $('#modalForm').modal('show');
    }
    
    function view_pin(id, username) {
        $.ajax({
            type: 'post',
            url: '<?php echo base_url() ?>/index.php/dev_ops/main/view_pin/'+id,
            success: function (data) {
                $('#modalTitlePin').html('PIN : '+username);
                $('#modal-pin').html(data);
                $('#modalPin').modal('show');
            }
        });
    }
</script>