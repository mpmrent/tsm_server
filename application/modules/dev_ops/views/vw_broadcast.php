<style>
    thead tr th {
        text-align: center;
    }
    
    /*tr {
        white-space: pre;
    }
    */
    .btn-floating {
        position: fixed; 
        bottom: 20px; 
        z-index: 100002; 
        right: 20px; 
        margin-bottom: 5px; 
        margin-top: -40px;
        -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        border-radius: 10px;
    }
    
    .fade.in {
        background: rgba(0, 0, 0, 0.4);
    }

    .chosen-container {
        width: 100% !important;
    }
    
    .input-mini {
        width: 85%;
        margin-right: 10px;
    }

    .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
        background: none;
    }
</style>

<div class="x_content">
    <div class="btn-floating">
        <a onclick="return add()" class="btn btn-app btn-primary btn-xs">
            <i class="ace-icon fa fa-edit bigger-160"></i>
            Kirim
        </a>
    </div>
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="widget-box">
                <div class="widget-header widget-header-flat widget-header-small">
                    <h5 class="widget-title">
                        <i class="ace-icon fa fa-filter"></i>
                        Filter
                    </h5>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <!--<form id="formX" action="<?php echo base_url() ?>index.php/dev_ops/main/timesheets" method="get" class="form-horizontal form-label-left">-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="text" id="form-field-1" value="<?php echo (isset($_GET['driver'])) ? $_GET['driver'] : ''; ?>" name="driver" placeholder="Driver" class="col-xs-12">
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                            <input class="form-control" name="daterange" value="<?php echo (isset($_GET['daterange'])) ? $_GET['daterange'] : ''; ?>" placeholder="Date Range" type="text" id="id-date-range-picker-1" />
                                        </div>
                                    </div>
                                    <div class="col-md-5" style="text-align: right;">
                                        <a id="btn-clear" href="#" class="btn btn-default">Bersihkan</a>
                                        <a id="btn-filter" href="#" class="btn btn-success">Proses</a>
                                        |
                                        <a id="btn-notif-approve" href="#" class="btn btn-danger">Notif Approve</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php 
                $this->table->set_template(array('table_open' => '<table style="width: 100%;" id="timesheets" class="table table-striped table-bordered dataTable no-footer table-hover">'));
                $this->table->set_heading($thead);
                echo $this->table->generate($table);
            ?>
        </div>
        <div class="col-md-12">
            <div class="modal fade" style="z-index: 105110;" id="modalForm" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <form id="modalFormX" action="<?php echo base_url() ?>index.php/report/carapps/car_menu_priv/edit" method="post" class="form-horizontal form-label-left">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="modalTitle">
                                    Kirim Pesan (Broadcast)
                                </h4>
                            </div>
                            <div class="modal-body" id="menu-priv">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Judul Pesan </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="title" placeholder="Judul pesan" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Pesan </label>
                                    <div class="col-sm-9">
                                        <textarea name="message" placeholder="Tulis pesan" class="col-xs-12"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Tujuan </label>
                                    <div class="col-sm-9">
                                        <select multiple="multiple" class="chosen-select tag-input-style form-control" id="form-field-select-3" name="drivers[]"  data-placeholder="Select Customers">
                                        <!-- <select class="form-control __web-inspector-hide-shortcut__" name="CustId" id="form-field-select-1"> -->
                                            <option value="%">~ All Driver ~</option>
                                            <?php foreach ($drivers->result() as $driver) { ?>
                                                <option value="<?php echo $driver->username ?>"><?php echo $driver->username . ' - ' . $driver->firstname . ' ' . $driver->lastname ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group right">
                                    <div class="col-md-12">
                                        <button data-dismiss="modal" class="btn btn-default">Batal</button>
                                        <button id="edt_btn" type="submit" class="btn btn-success">Proses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function(){

        if(!ace.vars['touch']) {
            $('.chosen-select').chosen({allow_single_deselect:true}); 
            //resize the chosen on window resize
    
            $(window)
            .off('resize.chosen')
            .on('resize.chosen', function() {
                $('.chosen-select').each(function() {
                     var $this = $(this);
                     $this.next().css({'width': $this.parent().width()});
                })
            }).trigger('resize.chosen');
            //resize chosen on sidebar collapse/expand
            $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
                if(event_name != 'sidebar_collapsed') return;
                $('.chosen-select').each(function() {
                     var $this = $(this);
                     $this.next().css({'width': $this.parent().width()});
                })
            });
        }

        function generateEditable(id, driver, daterange){
            $('#'+id).DataTable({
                destroy: true,
                "ajax": "<?php echo base_url(); ?>/index.php/dev_ops/main/broadcast/get?daterange="+daterange,
                "deferRender": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv'
                ],
                responsive: true,  
                // "columnDefs": [
                //     { 
                //         "targets": 8,
                //         "render": function(data, type, row, meta){
                //             var app = '';
                //             app = '<a href="#" onclick="setModalTarget('+row[0]+', '+row[0]+')" data-toggle="modal" data-target="#target"><span class="label label-sm label-primary arrowed arrowed-right"><span class="fa fa-clock-o"></span> Kirim Ulang </span></a>';
                //            return '<p style="text-align:center;">' + app + '</p>';  
                //         }
                //     }           
                // ]    
            });
            
            $('[name="'+id+'_length"]').addClass("form-control input-sm");
            $('[type="search"][aria-controls="'+id+'"]').addClass("form-control input-sm");
        };
        
        generateEditable('timesheets', '', '');
        
        $('#btn-filter').click( function() {
            var driver = $('[name="driver"]').val();
            var daterange = $('[name="daterange"]').val();
            generateEditable('timesheets', driver, daterange);
        });
        
        $('.date-picker').datepicker({
                autoclose: true,
                todayHighlight: true
        })
        
        $('.input-daterange').datepicker({autoclose:true});
        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('#id-date-range-picker-1').daterangepicker({
                'applyClass' : 'btn-sm btn-success',
                'cancelClass' : 'btn-sm btn-default',
                locale: {
                        applyLabel: 'Apply',
                        cancelLabel: 'Cancel',
                }
        })
        .prev().on(ace.click_event, function(){
                $(this).next().focus();
        });

        <?php if (($this->session->flashdata('message')['status'] != null)) { ?>
            showPopUp(
                    'Information',
                    '<?php echo $this->session->flashdata('message')['comment'] ?>',
                    '<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Ok</button>'
                );
        <?php } ?>
    });


    $('#btn-clear').click(function() {
        $('[name="driver"]').val('');
        $('[name="daterange"]').val('');
    });
    
    function add() {
        $('#modalFormX').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/broadcast/sending');
        $('#modalForm').modal('show');
    }

    $(document).ready(function() {
        socket.emit('notif_bc', { 
            title: 'tes',
            text: 'Trigger'
        });
    });

    $('#btn-notif-approve').click( function() {
        // alert('tes');
        socket.emit('notif_tsm', { 
            title: 'tes',
            text: 'Trigger'
        });
    });
</script>