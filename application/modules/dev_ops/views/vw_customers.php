<style>
    thead tr th {
        text-align: center;
    }
    
    tr {
        white-space: pre;
    }
    
    .btn-floating {
        position: fixed; 
        bottom: 20px; 
        z-index: 100002; 
        right: 20px; 
        margin-bottom: 5px; 
        margin-top: -40px;
        -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        border-radius: 10px;
    }
    
    .fade.in {
        background: rgba(0, 0, 0, 0.4);
    }
</style>

<div class="x_content">
    <div class="btn-floating">
        <a onclick="return add()" class="btn btn-app btn-primary btn-xs">
            <i class="ace-icon fa fa-edit bigger-160"></i>
            Tambah
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php 
                $this->table->set_template(array('table_open' => '<table style="width: 100%;" id="customers" class="table table-striped table-bordered dataTable no-footer table-hover">'));
                $this->table->set_heading($thead);
                echo $this->table->generate($table);
            ?>
        </div>
        <div class="col-md-12">
            <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <form id="modalFormX" action="<?php echo base_url() ?>index.php/report/carapps/car_menu_priv/edit" method="post" class="form-horizontal form-label-left">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="modalTitle"></h4>
                            </div>
                            <!-- <input style="display: none;" name="id" value=""> -->
                            <div class="modal-body" id="menu-priv">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> ID Pelanggan </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="ID Pelanggan" placeholder="Cust ID" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Nama </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="Name" placeholder="Nama" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Status </label>
                                    <div class="col-sm-9">
                                        <select class="form-control __web-inspector-hide-shortcut__" name="status" id="form-field-select-1">
                                            <option value="active">active</option>
                                            <option value="inactive">inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group right">
                                    <div class="col-md-12">
                                        <button data-dismiss="modal" class="btn btn-primary">Batal</button>
                                        <button id="edt_btn" type="submit" class="btn btn-success">Proses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function(){
        function generateEditable(id, target, url, window){
            $('#'+id).DataTable({
                "ajax": "<?php echo base_url(); ?>/index.php/dev_ops/main/get_table_carpro/customers/select",
                "deferRender": true,
                responsive: true,  

                // "columnDefs": [
                //     { 
                //         "targets": target,
                //         "render": function(data, type, row, meta){
                //             var edt = '<a href="#" onclick="return row_edit(\''+row[0]+'\')"><span class="label label-sm label-success arrowed arrowed-right"><span class="fa fa-edit"></span> edit</span></a> ';
                //             var del = '<a href="<?php echo site_url()?>/'+url+'/delete/'+row[0]+'/<?php echo (!isset($type)) ? '' : $type;?>" onclick="return confirm(\'Are you sure?\')"><span class="label label-sm label-'+((row[3] == 'inactive') ? 'primary' : 'danger')+' arrowed arrowed-right"><span class="fa fa-trash-o"></span> '+((row[3] == 'inactive') ? 'active' : 'inactive')+'</span></a>';
                //            return '<p style="text-align:center;">' + edt + del + '</p>';  
                //         }
                //     }           
                // ]     
            });
            
            $('[name="'+id+'_length"]').addClass("form-control input-sm");
            $('[type="search"][aria-controls="'+id+'"]').addClass("form-control input-sm");
        };
        
        generateEditable('customers',27, 'dev_ops/main/customers');
        
        <?php if ($this->session->flashdata('message') != NULL) { ?>
        new TabbedNotification({
            title: '<?php echo $this->session->flashdata('message')['title'] ?>',
            text: '<?php echo $this->session->flashdata('message')['message'] ?>',
            type: '<?php echo $this->session->flashdata('message')['class'] ?>',
            sound: false
        });
        <?php } ?>
    });
    
    // $('[type="submit"]#edt_btn').click(function(e) {
    //     document.getElementById('modalFormX').submit(); 
    // });
    
    // function row_edit(id) {
    //     $('[name="CustId"]').attr('readonly', 'true');
    //     $('#modalTitle').html('Edit Customer');
    //     $('#modalFormX').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/customers/update/'+id);
    //     $.ajax({
    //         type: 'post',
    //         url: '<?php echo base_url() ?>/index.php/dev_ops/main/customers/edit/'+id,
    //         success: function (data) {
    //             $('[name="CustId"]').val(data.CustId);
    //             $('[name="Name"]').val(data.Name);
    //             $('[name="ModifiedDateTime"]').val(data.ModifiedDateTime);
    //             $('[name="status"]').val(data.status).change();
    //             $('#modalForm').modal('show');
    //         }
    //     });
    // }
    
    // function add(id) {
    //     $('[name="CustId"]').removeAttr('readonly');
    //     $('#modalFormX').find("input[type=text], select").val("");
    //     $('#modalTitle').html('Add Customer');
    //     $('#modalFormX').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/customers/add');
    //     $('#modalForm').modal('show');
    // }
</script>