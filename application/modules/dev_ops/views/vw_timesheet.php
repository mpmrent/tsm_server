<style>
    thead tr th {
        text-align: center;
    }
    
    tr {
        white-space: pre;
    }
    
    .btn-floating {
        position: fixed; 
        bottom: 20px; 
        z-index: 100002; 
        right: 20px; 
        margin-bottom: 5px; 
        margin-top: -40px;
        -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        border-radius: 10px;
    }
    
    .fade.in {
        background: rgba(0, 0, 0, 0.4);
    }
    
    .input-mini {
        width: 85%;
        margin-right: 10px;
    }

    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
        white-space: normal;
    }
</style>

<div class="x_content">
<!--    <div class="btn-floating">
        <a onclick="return add()" class="btn btn-app btn-primary btn-xs">
            <i class="ace-icon fa fa-edit bigger-160"></i>
            Add
        </a>
    </div>-->
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="widget-box">
                <div class="widget-header widget-header-flat widget-header-small">
                    <h5 class="widget-title">
                        <i class="ace-icon fa fa-filter"></i>
                        Filter
                    </h5>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <!--<form id="formX" action="<?php echo base_url() ?>index.php/dev_ops/main/timesheets" method="get" class="form-horizontal form-label-left">-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="text" id="form-field-1" value="<?php echo (isset($_GET['driver'])) ? $_GET['driver'] : ''; ?>" name="driver" placeholder="Driver" class="col-xs-12">
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                            <input class="form-control" name="daterange" value="<?php echo (isset($_GET['daterange'])) ? $_GET['daterange'] : ''; ?>" placeholder="Date Range" type="text" id="id-date-range-picker-1" />
                                        </div>
                                    </div>
                                    <div class="col-md-5" style="text-align: right;">
                                        <a id="btn-clear" href="#" class="btn btn-default">Bersihkan</a>
                                        <a id="btn-filter" href="#" class="btn btn-success">Proses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="overflow: auto;">
            <?php 
                $this->table->set_template(array('table_open' => '<table style="width: 100%;" id="timesheets" class="table table-striped table-bordered dataTable no-footer table-hover">'));
                $this->table->set_heading($thead);
                echo $this->table->generate($table);
            ?>
        </div>

        <div class="col-md-12">
            <div class="modal fade" style="z-index: 105110;" id="modalForm" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <form id="modalFormX" action="<?php echo base_url() ?>index.php/report/carapps/car_menu_priv/edit" method="post" class="form-horizontal form-label-left">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="modalTitle2"></h4>
                            </div>
                            <input style="display: none;" name="id" value="">
                            <div class="modal-body" id="menu-priv">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> ID </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="id" placeholder="ID" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Absen Masuk </label>
                                    <div class="col-sm-9">
                                        <input type="datetime" id="date-timepicker1" name="start_time" placeholder="Absen Masuk" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Absen Pulang </label>
                                    <div class="col-sm-9">
                                        <input type="datetime" id="date-timepicker2" name="end_time" placeholder="Absen Pulang" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> KM Masuk </label>
                                    <div class="col-sm-9">
                                        <input type="number" id="form-field-1" name="km_awal" placeholder="KM Masuk" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> KM Pulang </label>
                                    <div class="col-sm-9">
                                        <input type="number" id="form-field-1" name="km_akhir" placeholder="KM Pulang" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Plat Nomor </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="nopol" placeholder="Plat Nomor" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Sopir GS </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="driver_gs" placeholder="Sopir GS" class="col-xs-12">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group right">
                                    <div class="col-md-12">
                                        <button data-dismiss="modal" class="btn btn-default">Batal</button>
                                        <button id="edt_btn" type="submit" class="btn btn-success">Proses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function(){
        $('#date-timepicker1').datetimepicker().next().on(ace.click_event, function(){
            $(this).prev().focus();
        });

        
        $('#date-timepicker2').datetimepicker().next().on(ace.click_event, function(){
            $(this).prev().focus();
        });
        function generateEditable(id, driver, daterange){
            $('#'+id).DataTable({
                destroy: true,
                "ajax": "<?php echo base_url(); ?>/index.php/dev_ops/main/get_timesheets_print?driver="+driver+"&daterange="+daterange,
                "deferRender": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'print'
                ],
                <?php if ($this->session->userdata('user_type') == 'administrator') { ?>
                "columnDefs": [
                    { 
                        "targets": 20,
                        "render": function(data, type, row, meta){
                            var edt = '<a href="#" onclick="return row_edit('+row[0]+')"><span class="label label-sm label-success arrowed arrowed-right"><span class="fa fa-edit"></span> ubah</span></a> ';
                           return '<p style="text-align:center;">' + edt + '</p>';  
                        }
                    }         
                ],   
                responsive: true, 
                <?php } ?> 
            });
            
            $('[name="'+id+'_length"]').addClass("form-control input-sm");
            $('[type="search"][aria-controls="'+id+'"]').addClass("form-control input-sm");
        };
        
        generateEditable('timesheets', '', '');
        
        $('#btn-filter').click( function() {
            var driver = $('[name="driver"]').val();
            var daterange = $('[name="daterange"]').val();
            generateEditable('timesheets', driver, daterange);
        });
        
        $('.date-picker').datepicker({
                autoclose: true,
                todayHighlight: true
        })
        
        $('.input-daterange').datepicker({autoclose:true});
        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('#id-date-range-picker-1').daterangepicker({
                'applyClass' : 'btn-sm btn-success',
                'cancelClass' : 'btn-sm btn-default',
                locale: {
                        applyLabel: 'Apply',
                        cancelLabel: 'Cancel',
                }
        })
        .prev().on(ace.click_event, function(){
                $(this).next().focus();
        });
    });

    <?php if (($this->session->flashdata('message')['status'] != null)) { ?>
        showPopUp(
                'Information',
                '<?php echo $this->session->flashdata('message')['comment'] ?>',
                '<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">OK</button>'
            );
    <?php } ?>
    
    function row_edit(id) {
        $('[name="id"]').attr('readonly', 'true');
        $('#modalTitle2').html('Edit Timesheets');
        $('#modalFormX').attr('action', '<?php echo base_url() ?>index.php/dev_ops/main/timesheets/update/'+id);
        $.ajax({
            type: 'post',
            url: '<?php echo base_url() ?>/index.php/dev_ops/main/timesheets/edit/'+id,
            success: function (data) {
                $('[name="id"]').val(data.id);
                $('[name="start_time"]').val(data.start_time);
                $('[name="end_time"]').val(data.end_time);
                $('[name="km_awal"]').val(data.km_awal);
                $('[name="km_akhir"]').val(data.km_akhir);
                $('[name="nopol"]').val(data.nopol);
                $('[name="driver_gs"]').val(data.driver_gs);
                $('#modalForm').modal('show');
            }
        });
    }
    
    
    $('#btn-clear').click(function() {
        $('[name="driver"]').val('');
        $('[name="daterange"]').val('');
//        $('#formX').find("input[type=text], select").val("");
    });
</script>