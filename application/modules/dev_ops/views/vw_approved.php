<style>
    thead tr th {
        text-align: center;
    }
    
   /* tr {
        white-space: pre;
    }*/
    
    .btn-floating {
        position: fixed; 
        bottom: 20px; 
        z-index: 100002; 
        right: 20px; 
        margin-bottom: 5px; 
        margin-top: -40px;
        -webkit-box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        box-shadow: 0 2px 4px rgba(0, 0, 0, .4);
        border-radius: 10px;
    }
    
    .fade.in {
        background: rgba(0, 0, 0, 0.4);
    }
    
    .input-mini {
        width: 85%;
        margin-right: 10px;
    }
</style>

<div class="x_content">
<!--    <div class="btn-floating">
        <a onclick="return add()" class="btn btn-app btn-primary btn-xs">
            <i class="ace-icon fa fa-edit bigger-160"></i>
            Add
        </a>
    </div>-->
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="widget-box">
                <div class="widget-header widget-header-flat widget-header-small">
                    <h5 class="widget-title">
                        <i class="ace-icon fa fa-filter"></i>
                        Filter
                    </h5>
                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <!--<form id="formX" action="<?php echo base_url() ?>index.php/dev_ops/main/timesheets" method="get" class="form-horizontal form-label-left">-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="text" id="form-field-1" value="<?php echo (isset($_GET['driver'])) ? $_GET['driver'] : ''; ?>" name="driver" placeholder="Kode Sopir" class="col-xs-12">
                                    </div>
                                    
                                    <!-- By wsw start -->
                                    <div class="col-sm-3">
                                        <select class="form-control __web-inspector-hide-shortcut__" name="approval_value" id="approval_value">
                                            <option value=""> - Pilih Tipe - </option>
                                            <option checked="<?php echo (isset($_GET['approval_value']) && $_GET['approval_value']=='Approved') ? true : false; ?>" value="Approved">Approved</option>
                                            <option checked="<?php echo (isset($_GET['approval_value']) && $_GET['approval_value']=='Rejected') ? true : false; ?>" value="Rejected">Rejected</option>
                                        </select>
                                    </div>
                                    <!-- By wsw end -->
                                    
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                            <input class="form-control" name="daterange" value="<?php echo (isset($_GET['daterange'])) ? $_GET['daterange'] : ''; ?>" placeholder="Date Range" type="text" id="id-date-range-picker-1" />
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="text-align: right;">
                                        <a id="btn-clear" href="#" class="btn btn-default">Bersihkan</a>
                                        <a id="btn-filter" href="#" class="btn btn-success">Proses</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php 
                $this->table->set_template(array('table_open' => '<table style="width: 100%;" id="timesheets" class="table table-striped table-bordered dataTable no-footer table-hover">'));
                $this->table->set_heading($thead);
                echo $this->table->generate($table);
            ?>
        </div>
    </div>
</div>

<script>
    $(document).ready( function(){
        function generateEditable(id, driver, daterange, approval_value){
            $('#'+id).DataTable({
                destroy: true,
                "ajax": "<?php echo base_url(); ?>/index.php/dev_ops/main/get_timesheets/approved?driver="+driver+"&daterange="+daterange+"&approval_value="+approval_value,
                "deferRender": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv'
                ],
                responsive: true,  
            });
            
            $('[name="'+id+'_length"]').addClass("form-control input-sm");
            $('[type="search"][aria-controls="'+id+'"]').addClass("form-control input-sm");
        }
        generateEditable('timesheets', '', '', '');
        
        $('#btn-filter').click( function() {
            var driver = $('[name="driver"]').val();
            var daterange = $('[name="daterange"]').val();
            // By wsw
            var approval_value = $('[name="approval_value"]').val();
            generateEditable('timesheets', driver, daterange, approval_value);
        });
        
        $('.date-picker').datepicker({
                autoclose: true,
                todayHighlight: true
        });
        
        $('.input-daterange').datepicker({autoclose:true});
        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('#id-date-range-picker-1').daterangepicker({
                'applyClass' : 'btn-sm btn-success',
                'cancelClass' : 'btn-sm btn-default',
                locale: {
                        applyLabel: 'Apply',
                        cancelLabel: 'Cancel',
                }
        })
        .prev().on(ace.click_event, function(){
                $(this).next().focus();
        });
    });
    
    
    $('#btn-clear').click(function() {
        $('[name="driver"]').val('');
        $('[name="daterange"]').val('');
//        $('#formX').find("input[type=text], select").val("");
    });
</script>