<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	function __construct(){
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model(array('m_user', 'm_function'));
        // $this->db = $this->load->database('dbcarpro', true);
        if (!$this->session->userdata('is_logged_in')){
            $this->session->set_userdata('last_page', current_url());
            redirect ('');
        }
	}

    public function changePassword() {
        $cek = $this->db->where('username', $this->session->userdata('username'))->get('users')->row_array();
        if ($_POST['new_password'] != $_POST['konf_new_password']) {
            $output = array(
                'status' => 2,
                'comment' => 'Konfirmasi kata sandi tidak cocok dengan kata sandi baru, silahkan dicoba kembali.'
            );
        } else if (($_POST['old_password']) != simple_decrypt($cek['password'])) {
            $output = array(
                'status' => 2,
                'comment' => 'Kata sandi lama anda salah, pastikan anda memasukan kata sandi dengan benar.'
            );
        } else {
            $data = array(
                    'password' => simple_encrypt($_POST['new_password'])
                );
            $this->db->where('username', $this->session->userdata('username'));
            if ($this->db->update('users', $data)) {
                $output = array(
                    'status' => 1,
                    'comment' => 'Ubah kata sandi berhasil.'
                );
            } else {
                $output = array(
                    'status' => 2,
                    'comment' => 'Ubah kata sandi gagal, silahkan dicoba kembali.'
                );
            }
        }
        $this->session->set_flashdata('message', $output);
        redirect('/main');
    }

    public function get_table_carpro($table, $select = '*') {
        ini_set('max_execution_time', 0); 
        ini_set('memory_limit', '-1');
        $output = array();
        $aColumns = $this->db->list_fields($table);
        if ($select != '*') {
            foreach ($this->columns as $sel) {
                $this->db->select($sel);
            }
        }
        $data = $this->data['table'] = $this->db->get($table);
        foreach ($data->result_array() as $aRow) {
            $row = array();
            foreach ($aColumns as $col) {
                $row[] = $aRow[$col];
            }
            $output['data'][] = $row;
        }
        echo json_encode($output);
    }

    public function update_pic() {
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        // $media = $this->upload->data('file');
        $inputFileName = './assets/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $this->db->truncate('import_pic');
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "driver_name"=> trim($rowData[0][0]),
                    "pic_name"=> trim($rowData[0][1])
                );
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("import_pic",$data);
                try {
                    // unlink($inputFileName);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                }
                     
            }

        $this->db->query("UPDATE users a
                LEFT JOIN import_pic b
                ON a.username = driver_name
                SET pic = pic_name
                WHERE pic_name != '';");
        $this->session->set_flashdata('success', $this->db->affected_rows());
        redirect('/dev_ops/main/users');
    }

    public function get_table($table) {
        $output = array();
        $aColumns = $this->db->list_fields($table);
        if ($table == 'users')
            if ($this->session->userdata('user_type') != 'administrator') 
                $this->db->where('user_type not like "admin%" ', null, true);
        $data = $this->data['table'] = $this->db->get($table);
        foreach ($data->result_array() as $aRow) {
            $row = array();
            foreach ($aColumns as $col) {
                if (($this->session->userdata('user_type') == 'administrator') && ($col=='password'))
                    $row[] = simple_decrypt($aRow[$col]);
                else 
                    $row[] = $aRow[$col];
            }
            $output['data'][] = $row;
        }
        echo json_encode($output);
    }

    public function users($action = null, $id = null){
        if ($action == null) {
            $this->set_menu_active(3);
            $this->data['table'] = $this->db->query('select "";');
            $this->data['thead'] = array('Id', 'Username', 'Alias', 'Password', 'User Type', 'First Name', 'Last Name', 'Phone', 'Email', 'Cust Id', 'Cust Name', 'PIC', 'Status', 'Created Date', 'Action');
            $this->db->select('OLDCODE, CUSTNAME');
            $this->db->distinct();
            $this->data['custs'] = $this->db->order_by('OLDCODE')->get('customers');
            $this->data['pics'] = $this->db->like('user_type', 'pic')->get('users');
            $this->load_theme('dev_ops/vw_users');
        } else if ($action == 'edit') {
            header("Content-Type: application/json", true);
            $data = $this->db->where('id', $id)->get('users')->row_array();
            $data['password'] = simple_decrypt($data['password']);
            echo json_encode($data);
        } else if ($action == 'update') {
            $data['id'] = $id;
            $data['password'] = simple_encrypt($_POST['password']);
            $data['custname'] = $this->db->where('OLDCODE', $_POST['custid'])->get('customers')->row_array()['CUSTNAME'];
            $datas = array_merge($_POST, $data);
            $this->db->where('username', $_POST['username'])->update('users', $datas);
           // echo json_encode($datas);
                redirect('/dev_ops/main/users');
        } else if ($action == 'add') {
        	$data['username'] = str_replace(' ', '', $_POST['username']);
            $data['password'] = simple_encrypt($_POST['password']);
            $datas = array_merge($_POST, $data);
            $this->db->insert('users', $datas);
            redirect('/dev_ops/main/users');
        } else if ($action == 'delete') {
            $row = $this->db->where('id', $id)->get('users')->row_array();
            if ($row['status'] == 'active') 
                echo $this->db->where('id', $id)->update('users', array('status' => 'inactive')) . $row['status'];
            else
                echo $this->db->where('id', $id)->update('users', array('status' => 'active'));
            redirect('/dev_ops/main/users');
        }
	}
        
	public function view_pin($id){
            echo simple_decrypt($id);
    }

    public function batch_insert_users () {
        $query = "INSERT INTO `tsm_live`.`users` 
 (`username`, 
 `password`, 
 `user_type`, 
 `firstname`, 
 `lastname`, 
 `custid`, 
 `custname`
 )
 VALUES
 ('binus3',
 '".simple_encrypt('123456')."', 
 'pic', 
 'YAYASAN BINA NUSANTARA', 
 '', 
 '41479', 
 'YAYASAN BINA NUSANTARA'
 );
";
        if ($this->db->query($query))
            echo 'Success';

    }

	// public function customers($action = null, $id = null){
 //            if ($action == null) {
 //                $this->set_menu_active(4);
 //                $this->data['table'] = $this->db->query('select "";');
 //                $this->data['thead'] = array('Id', 'Name', 'Modified Date', 'Status', 'Action');
 //                $this->load_theme('dev_ops/vw_customers');
 //            } else if ($action == 'edit') {
 //                header("Content-Type: application/json", true); 
 //                $data = $this->db->where('CustId', $id)->get('custtable');
 //                echo json_encode($data->row_array());
 //            } else if ($action == 'update') {
 //                $this->db->where('CustId', $_POST['CustId'])->update('custtable', $_POST);
 //                redirect('/dev_ops/main/customers');
 //            } else if ($action == 'add') {
 //                $this->db->insert('custtable', $_POST);
 //                redirect('/dev_ops/main/customers');
 //            } else if ($action == 'delete') {
 //                $row = $this->db->where('CustId', $id)->get('custtable')->row_array();
 //                if ($row['status'] == 'active') 
 //                    $this->db->where('CustId', $id)->update('custtable', array('status' => 'inactive'));
 //                else
 //                    $this->db->where('CustId', $id)->update('custtable', array('status' => 'active'));
 //                redirect('/dev_ops/main/customers');
 //            }
	// }

    public function customers($action = null, $id = null){
            $this->set_menu_active(4);
            $this->columns = array('DATAAREAID', 'ACCOUNTNUM', 'OLDCODE', 'CUSTNAME',
                'LOCATOR', 'PICSALES');
            $this->data['table'] = $this->db->query('select "";');
            $this->data['thead'] = array('DATA AREA', 'ACCOUNTNUM', 'OLD CODE', 'CUST NAME',
                'LOCATOR', 'PIC SALES');
            $this->load_theme('dev_ops/vw_customers');
    }

    public function eapproval($action = null, $id = null){
            $this->set_menu_active(27);
            $this->columns = array('ID', 'USERNAME', 'IP_ADDRESS', 'DEVICE', 'BROWSER', 'DATETIME');
            $this->data['table'] = $this->db->query('select "";');
            $this->data['thead'] = $this->columns;
            $this->load_theme('dev_ops/vw_e_approval');
    }

    public function resume(){
            $this->set_menu_active(23);
            $this->data['table'] = $this->db->query('select "";');
            $this->data['thead'] = array('ID', 'Driver Code','Fullname','Check In','Check Out','Check Out Edit','Location In','Location Out','Note','Cust Id','Cust Name','Start KM','End KM','Out of Town','Town','License Plate','Temporary Driver','Type of Use','Approved By','Approved Time','Create Time','Update Time', 'Score','PIC','Platform','Version','Manufacturer','Model');
            $this->load_theme('dev_ops/vw_resume');
    }

    public function import_ax($action = null){
        if ($action == null) {
            $this->set_menu_active(25);
            $this->data['table'] = $this->db->limit(1)->get('import_ax_reg');
            $this->load_theme('dev_ops/vw_import');
        } else if ($action == 'filter') {
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $output = array();
            $aColumns = $this->db->list_fields('import_ax_reg');
            $from = date('Y-m-d', strtotime('first day of this month'));
            $to =  date('Y-m-d', strtotime('first day of next month'));
            if (isset($_GET['daterange']) && $_GET['daterange'] != '') { 
                $date = explode('-', $_GET['daterange']); 
                $from = date("Y-m-d", strtotime($date[0]));
                $to = date("Y-m-d", strtotime( "+1 Day" , strtotime($date[1])));
            }
            if (isset($_GET['driver'])) 
                $this->db->like('Personnel_Number', $_GET['driver']);
            $this->db->where("Date between '$from' and '$to'");
            $data = $this->data['table'] = $this->db->limit(100000)->get('import_ax_reg');
            foreach ($data->result_array() as $aRow) {
                $row = array();
                foreach ($aColumns as $col) {
                    // if ($col != 'CustId') 
                        if (isset($aRow[$col])) 
                            $row[] = $aRow[$col];
                        else
                            $row[] = '';
                }
                $output['data'][] = $row;
            }
            echo json_encode($output);
        } 
    }

    public function import_carpro_ax($action = null){
        if ($action == null) {
            $this->set_menu_active(26);
            $query = "select top 1 concat( ltrim(rtrim(c.First_Name)), ' ', ltrim(rtrim(c.Last_Name ))) as 'Personnel', 
                dtl.CHAUFFEUR_ID as 'Personnel_Number', cast(cast(A_DATE as varchar(10)) as date) as 'Date', 
                 CONVERT(char(5), DATEADD(second, convert(int,FROM_TIME), '19000101'), 108) 'Clock_In', CONVERT(char(5), DATEADD(second, convert(int,TO_TIME), '19000101'), 108) 'Clock_Out', 
                'No' as 'Overdays', AGREEMENT_NO as 'DO_Number', 'No' as 'Overnight',
                 'Standby' = (case PICKET 
                 when 0 then 'No'
                 when 1 then 'Yes'
                 end),
                 'PP' = (case PP 
                 when 0 then 'No'
                 else 'Yes'
                 end), REMARKS as 'Remark'
                 from CHF_LT_ATTENDANCE_DTL dtl
                 left join CHAUFFEUR_PROFILE c
                 on dtl.CHAUFFEUR_ID = c.ACTUAL_CHF_ID;";
            $this->data['table'] = $this->db->query($query);
            $this->load_theme('dev_ops/vw_import_carpro');
        } else if ($action == 'filter') {
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $output = array();
            $aColumns = $this->db->list_fields('import_ax_reg');
            $from = date('Ymd', strtotime('first day of this month'));
            $to =  date('Ymd', strtotime('first day of next month'));
            if (isset($_GET['daterange']) && $_GET['daterange'] != '') { 
                $date = explode('-', $_GET['daterange']); 
                $from = date("Ymd", strtotime($date[0]));
                $to = date("Ymd", strtotime( "+1 Day" , strtotime($date[1])));
            }
            if (isset($_GET['driver'])) 
                $driver = $_GET['driver'];
            $data = $this->data['table'] = $this->db->limit(100000)->query("select concat( ltrim(rtrim(c.First_Name)), ' ', ltrim(rtrim(c.Last_Name ))) as 'Personnel', 
                dtl.CHAUFFEUR_ID as 'Personnel_Number', cast(cast(A_DATE as varchar(10)) as date) as 'Date', 
                 CONVERT(char(5), DATEADD(second, convert(int,FROM_TIME), '19000101'), 108) 'Clock_In', CONVERT(char(5), DATEADD(second, convert(int,TO_TIME), '19000101'), 108) 'Clock_Out', 
                'No' as 'Overdays', AGREEMENT_NO as 'DO_Number', 'No' as 'Overnight',
                 'Standby' = (case PICKET 
                 when 0 then 'No'
                 when 1 then 'Yes'
                 end),
                 'PP' = (case PP 
                 when 0 then 'No'
                 else 'Yes'
                 end), REMARKS as 'Remark'
                 from CHF_LT_ATTENDANCE_DTL dtl
                 left join CHAUFFEUR_PROFILE c
                 on dtl.CHAUFFEUR_ID = c.ACTUAL_CHF_ID
                 where dtl.CHAUFFEUR_ID like '%$driver%'
                 and A_DATE between '$from' and '$to';");
            foreach ($data->result_array() as $aRow) {
                $row = array();
                foreach ($aColumns as $col) {
                    // if ($col != 'CustId') 
                        if (isset($aRow[$col])) 
                            $row[] = $aRow[$col];
                        else
                            $row[] = '';
                }
                $output['data'][] = $row;
            }
            echo json_encode($output);
        } 
    }

    public function tes1 () {
        echo date("Y-m-d H:i:s");  
    }

	public function timesheets($action = null, $id = null){
        if ($action == null) {
            $this->set_menu_active(13);
            $this->data['table'] = $this->db->query('select "";');
            if ($this->session->userdata('user_type') == 'administrator') {
                $this->data['thead'] = array('ID', 'Driver Code','Fullname','Check In','Check Out', 'Note' , 'Cust Id','Cust Name','Start KM','End KM','Out of Town','Town','License Plate','Temporary Driver','Type of Use','Approved By','Approved Time','Update Time', 'Score','PIC', 'Action');
            } else {
                $this->data['thead'] = array('ID', 'Driver Code','Fullname','Check In','Check Out', 'Note', 'Cust Id' ,'Cust Name','Start KM','End KM','Out of Town','Town','License Plate','Temporary Driver','Type of Use','Approved By','Approved Time','Update Time', 'Score','PIC');
            }
            $this->load_theme('dev_ops/vw_timesheet');
        } else if ($action == 'edit') {
            header("Content-Type: application/json", true);
            $data = $this->db->where('id', $id)->get('timesheets')->row_array();
            $data['start_time'] = date('m/d/Y g:i A', strtotime($data['start_time']));
            $data['end_time'] = date('m/d/Y g:i A', strtotime($data['end_time']));
            echo json_encode($data);
        } else if ($action == 'update') {
            // echo json_encode($_POST);
            $update = array(
                    'km_awal' => $_POST['km_awal'],
                    'km_akhir' => $_POST['km_akhir'],
                    'nopol' => $_POST['nopol'],
                    'driver_gs'=> $_POST['driver_gs'],
                    'start_time_edit' => date('Y-m-d H:i', strtotime($_POST['start_time'])),
                    'end_time_edit' => date('Y-m-d H:i', strtotime($_POST['end_time'])),
                    'edit_admin_by' => $this->session->userdata('username'),
                    'edit_admin_time' => date("Y-m-d H:i:s")
                );
            if ($this->db->where('id', $id)->update('timesheets', $update)) {
                $output = array(
                    'status' => 1,
                    'comment' => 'Edit timesheet berhasil.'
                );
            } else {
                $output = array(
                    'status' => 2,
                    'comment' => 'Edit timesheet gagal, silahkan dicoba kembali.'
                );
            }
            $this->session->set_flashdata('message', $output);
            // echo json_encode($update);
            // echo $_POST['end_time'];
            redirect('/dev_ops/main/timesheets');
        }
	}

    public function pending($action = null, $id = null, $survey = null){
    		if ($action == null) {
                $this->set_menu_active(21);
				$this->data['table'] = $this->db->query('select "";');
				$this->data['thead'] = array('ID', 'Driver Code','Fullname','Check In','Check Out', 'Note','Cust Id','Cust Name','Start KM','End KM','Out of Town','Town','License Plate','Temporary Driver','Type of Use','Approved By','Approved Time','Update Time','PIC', 'Action');
                $this->data['notApproveType'] = $this->m_function->getEnum('timesheets', 'not_approve_type');
                $this->load_theme('dev_ops/vw_pending');
				$timesheet = $this->db->where('id', $id)->get('rpt_timesheet_2')->row_array();
            } else if ($action == 'approve') {
                $check_end = $this->db->where('id', $id)
                        ->where('end_time', null)
                        ->get('timesheets');
                if ($check_end->num_rows() == 0) {
                    $data = array(
                        'approved_by' => $this->session->userdata('username'),
                        'survey' => $survey,
                        'approved_time' => date('Y-m-d H:i:s')
                    );
                    $this->db->where('id', $id)->update('timesheets', $data);
                    $output = array(
                        'status' => 1,
                        'comment' => 'Approve timesheet berhasil.'
					);
					if (($survey/2) <= 2) {
						$timesheet = $this->db->where('id', $id)->get('rpt_timesheet_2')->row_array();
						$data_tsm['data_timesheet'] = array(
							'Timesheet ID' 	   => $timesheet['id'], 
							'Username' 	       => $timesheet['username'],
							'Fullname'         => $timesheet['fullname'],
							'Check In'         => $timesheet['start_time'],
							'Check Out'        => $timesheet['end_time'], 
							'Note'        	   => $timesheet['desc'],
							'Cust Id'          => $timesheet['CustId'],
							'Cust Name'        => $timesheet['CustName'],
							'License Plate'    => $timesheet['nopol'],
							'Score' 		   => ($survey/2)
						);
						$to = array('muhamad.arifin@mpm-rent.com', 'yudi.e@mpm-rent.com','contact@mpm-rent.com','rachman.purnama@mpm-rent.com');
						$this->send_mail($to, 'Driver Score for Timesheet: '. $id, 'email/low_star', $data_tsm);	   
					}
                    $this->session->set_flashdata('message', $output);
                    redirect('/dev_ops/main/pending');
                } else {
                    $output = array(
                        'status' => 2,
                        'comment' => 'Approve timesheets gagal, karena driver belum check out.'
                    );
                    $this->session->set_flashdata('message', $output);
                    redirect('/dev_ops/main/pending');
                }
            } else if ($action == 'check_out')  {
                $data = array(
                    'check_out_by' => $this->session->userdata('username'),
                    'end_time' => $_POST['end_time'],
                    'desc' => $_POST['desc']
                );
                if ($this->db->where('id', $id)->update('timesheets', $data)) {
                    $output = array(
                        'status' => 1,
                        'comment' => 'Check out timesheet berhasil.'
                    );
                } else {
                    $output = array(
                        'status' => 2,
                        'comment' => 'Check out timesheet gagal, silahkan dicoba kembali.'
                    );
                }
                $this->session->set_flashdata('message', $output);
                redirect('/dev_ops/main/pending');
            }
	}

    public function not_approved($id = null){
        $data = array(
                'not_approve_by' => $this->session->userdata('username'),
                'not_approve_type' => $_POST['not_approve_type'],
//                 'not_approve_desc' => $_POST['not_approve_desc'],
				'not_approve_desc' => $_POST['not_approve_type']=='reason7'?$_POST['not_approve_desc']:$_POST['not_approve_type'],
                'not_approve_time' => date("Y-m-d H:i:s")
            );
        $timesheet = $this->db->where('id', $id)->get('rpt_timesheet_2')->row_array();
        $this->db->where('id', $id);
        if ($this->db->update('timesheets', $data)) {
            $output = array(
                'status' => 1,
                'comment' => 'Tidak menyetujui absensi driver berhasil disampaikan dan akan divalidasi oleh admin kami, Terima kasih.'
            );
            $data_tsm['data_timesheet'] = array(
                'ID'        => $timesheet['id'], 
                'Username'        => $timesheet['username'],
                'Fullname'        => $timesheet['fullname'],
                'Check In'        => $timesheet['start_time'],
                'Check Out'        => $timesheet['end_time'], 
                'Note'        => $timesheet['desc'],
                'Cust Id'        => $timesheet['CustId'],
                'Cust Name'        => $timesheet['CustName'],
                'Start KM'        => $timesheet['km_awal'],
                'End KM'        => $timesheet['km_akhir'],
                'Out of Town'        => $timesheet['out_of_town'],
                'Town'        => $timesheet['town'],
                'License Plate'        => $timesheet['nopol'],
                'Temporary Driver'        => $timesheet['driver_gs'],
                'Type of Use'        => $timesheet['type_of_use'],
                'Approved By'        => $timesheet['approved_by'],
                'Approved Time'        => $timesheet['approved_time'],
                'Update Time'        => $timesheet['updated_time'],
                'PIC'        => $timesheet['pic']
            );
            $data_tsm['description'] = 'Tidak di setujui oleh '. $this->session->userdata('firstname') . ' ('.$this->session->userdata('username').') dengan keterangan : ' . $_POST['not_approve_desc'] . '.';
            $to = array('muhamad.arifin@mpm-rent.com', 'febriana.eka@mpm-rent.com', 'irene.s@mpm-rent.com');
            $this->send_mail($to, 'Timesheet Report - Not Approve '. $id, 'email/not_approved', $data_tsm);
        } else {
            $output = array(
                'status' => 2,
                'comment' => 'Aksi gagal, mohon coba kembali.'
            );
        }
        $this->session->set_flashdata('message', $output);
        redirect('/dev_ops/main/pending');
    }

    public function approved(){
            $this->set_menu_active(22);
            $this->data['table'] = $this->db->query('select "";');
            $this->data['thead'] = array('ID', 'Driver Code','Fullname','Check In','Check Out', 'Note','Cust Id','Cust Name','Start KM','End KM','Out of Town','Town','License Plate','Temporary Driver','Type of Use','Approved By','Approved Time','Update Time','Score');
            $this->load_theme('dev_ops/vw_approved');
    }

	public function get_timesheets($type = ''){
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $arif = $this->session->userdata('custid');
//             echo "<script type='text/javascript'>alert('$arif');</script>";
            $output = array();
            $aColumns = $this->db->list_fields('rpt_timesheet_2');
            $from = date('Y-m-d', strtotime('first day of this month'));
            $to =  date('Y-m-d', strtotime('first day of next month'));
            if (isset($_GET['daterange']) && $_GET['daterange'] != '') { 
                $date = explode('-', $_GET['daterange']); 
                $from = date("Y-m-d", strtotime($date[0]));
                $to = date("Y-m-d", strtotime( "+1 Day" , strtotime($date[1])));
            }
            // echo json_encode($arif);
            if (isset($_GET['driver'])) 
                $this->db->like('username', $_GET['driver']);
            $this->db->where("start_time between '$from' and '$to'");
            if ($type == 'pending') {
                $this->db->where('approved_by', null);
                if ($this->session->userdata('user_type') == 'superpic') 
                    $this->db->where(" (`CustId` = '".$this->session->userdata('custid')."') ", null, true);
                else
                    $this->db->where('pic', $this->session->userdata('username'));
            } else if ($type == 'approved') {
//                 $this->db->where('approved_by IS NOT NULL', null, false);
				// By wsw started
                if (isset($_GET['approval_value']) && $_GET['approval_value']=='Rejected') {
                    $this->db->where('(not_approve_by IS NOT NULL and approved_by IS NULL)', null, false);
                } else if (isset($_GET['approval_value']) && $_GET['approval_value']=='Approved') {
                $this->db->where('approved_by IS NOT NULL', null, false);
                }
                // By wsw ended
                else {
                    $this->db->where('(approved_by IS NOT NULL or not_approve_by IS NOT NULL)', null, false);
                }
                if ($this->session->userdata('user_type') == 'superpic') 
                    $this->db->where(" (`CustId` = '".$this->session->userdata('custid')."') ", null, true);
                else
                    $this->db->where('pic', $this->session->userdata('username'));
            }
            $data = $this->data['table'] = $this->db->limit(100000)->get('rpt_timesheet_2');
            foreach ($data->result_array() as $aRow) {
                $row = array();
                foreach ($aColumns as $col) {
                    // if ($col != 'CustId') 
                        if (isset($aRow[$col])) 
                            $row[] = $aRow[$col];
                        else
                            $row[] = '';
                }
                $output['data'][] = $row;
            }
            echo json_encode($output);
	}

    public function get_timesheets_print($type = ''){
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $output = array();
            $aColumns = $this->db->list_fields('rpt_timesheet_2');
            $from = date('Y-m-d', strtotime('first day of this month'));
            $to =  date('Y-m-d', strtotime('first day of next month'));
            if (isset($_GET['daterange']) && $_GET['daterange'] != '') { 
                $date = explode('-', $_GET['daterange']); 
                $from = date("Y-m-d", strtotime($date[0]));
                $to = date("Y-m-d", strtotime( "+1 Day" , strtotime($date[1])));
            }
            if (isset($_GET['driver'])) 
                $this->db->like('username', $_GET['driver']);
            $this->db->where("start_time between '$from' and '$to'");
            $data = $this->data['table'] = $this->db->limit(100000)->get('rpt_timesheet_2');
            foreach ($data->result_array() as $aRow) {
                $row = array();
                foreach ($aColumns as $col) {
                    // if ($col != 'CustId') 
                        if (isset($aRow[$col])) 
                            $row[] = $aRow[$col];
                        else
                            $row[] = '';
                }
                $output['data'][] = $row;
            }
            echo json_encode($output);
    }

    public function get_timesheets_report($type = ''){
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $output = array();
            $aColumns = $this->db->list_fields('rpt_timesheet');
            $from = date('Y-m-d', strtotime('first day of this month'));
            $to =  date('Y-m-d', strtotime('first day of next month'));
            if (isset($_GET['daterange']) && $_GET['daterange'] != '') { 
                $date = explode('-', $_GET['daterange']); 
                $from = date("Y-m-d", strtotime($date[0]));
                $to = date("Y-m-d", strtotime( "+1 Day" , strtotime($date[1])));
            }
            if (isset($_GET['driver'])) 
                $this->db->like('username', $_GET['driver']);
            $this->db->where("start_time between '$from' and '$to'");
            $data = $this->data['table'] = $this->db->limit(100000)->get('rpt_timesheet');
            foreach ($data->result_array() as $aRow) {
                $row = array();
                foreach ($aColumns as $col) {
                    if (isset($aRow[$col])) 
                        $row[] = $aRow[$col];
                    else
                        $row[] = '';
                }
                $output['data'][] = $row;
            }
            echo json_encode($output);
    }

    public function broadcast($action = null){
            if ($action == null) {
                $this->set_menu_active(24);
                $this->data['table'] = $this->db->query('select "";');
                $this->data['thead'] = array('ID', 'Title', 'Message', 'For','Created By', 'Datetime', 'Recieved', 'Read');
                $this->db->select('id, username, firstname, lastname');
                $this->data['drivers'] = $this->db->where('user_type', 'driver')->get('users');
                $this->load_theme('dev_ops/vw_broadcast');
            } else if ($action == 'sending') {
                $output = array();
                $data = array(
                        'title' => $_POST['title'],
                        'text' => $_POST['message'],
                        'for' => json_encode($_POST['drivers']),
                        'create_by' => $this->session->userdata('username'),
                    );
                if ($this->db->insert('broadcast_message', $data)) {
                    $output = array(
                        'status' => 1,
                        'comment' => 'Pesan berhasil dikirim.'
                    );
                } else {
                    $output = array(
                        'status' => 2,
                        'comment' => 'Pesan gagal dikirim, silahkan coba lagi.'
                    );
                }
                $this->session->set_flashdata('message', $output);  
                redirect('/dev_ops/main/broadcast');
            } else if ($action == 'get') {
                $output = array();
                $aColumns = $this->db->list_fields('vw_broadcast');
                $from = date('Y-m-d', strtotime('first day of this month'));
                $to =  date('Y-m-d', strtotime('first day of next month'));
                if (isset($_GET['daterange']) && $_GET['daterange'] != '') { 
                    $date = explode('-', $_GET['daterange']); 
                    $from = date("Y-m-d", strtotime($date[0]));
                    $to = date("Y-m-d", strtotime( "+1 Day" , strtotime($date[1])));
                }
                $this->db->select("id, title, text, for, create_by, `datetime`, COUNT(received) 'received', COUNT(`read`) AS 'read'");
                $this->db->group_by('id');
                $data = $this->data['table'] = $this->db->get('vw_broadcast');
                foreach ($data->result_array() as $aRow) {
                    $row = array();
                    foreach ($aColumns as $col) {
                        if ($col != 'username') {
                            if (isset($aRow[$col])) 
                                $row[] = $aRow[$col];
                            else
                                $row[] = '';
                        }
                    }
                    $output['data'][] = $row;
                }
                echo json_encode($output);
            }
    }
    
    public function tes() {
        $output['result'] = 1;
        echo json_encode($output);
    }
}