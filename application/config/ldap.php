<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['ldapp'] = 'mpmrent1!';
$config['ldapus'] = 'portal@mpm-rent.local';
$config['ldap_server'] = '192.168.0.18';
$config['ldap_port']=389;
$config['ldap_base_dn'] = 'DC=mpm-rent,DC=local';
$config['dc'] = "mpm-rent.local";
$config['user_prefix'] = "";
$config['user_suffix'] = "";