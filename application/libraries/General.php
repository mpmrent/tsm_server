<?php

/*
 * Name : General Library
 * Author : Sugiarto (sugiarto@gie-art.com)
 * Date : December 24, 2015
 */

class General {

    var $CI;

    function __construct() {
        $this->CI = &get_instance();
//        $this->isLogin();
    }

    function generateRandomCode($length = 8) {
        // Available characters
        $chars = '0123456789abcdefghjkmnoprstvwxyz';

        $Code = '';
        // Generate code
        for ($i = 0; $i < $length; ++$i) {
            $Code .= substr($chars, (((int) mt_rand(0, strlen($chars))) - 1), 1);
        }
        return strtoupper($Code);
    }

    public function humanDate($datetime) {
        return date("D, d M Y H:i:s", strtotime($datetime));
    }

    public function humanDate2($date) {
        return date("D, d M Y", strtotime($date));
    }

    function generateUniqueName($fileName) {

        return $this->CI->session->userdata('session_id') . md5(date("Y-m-d H:i:s")) . md5($fileName) . '.' . $this->getFileExtension($fileName);
    }

    function getFileExtension($fileName) {
        return substr(strrchr($fileName, '.'), 1);
    }
    
    
    function getBirthdayNow() {
        $query = "SELECT *
                , CONCAT(MONTHNAME(birthday),' ',DAY(birthday)) AS 'birth' 
              FROM `users_birthday`
              WHERE STR_TO_DATE(CONCAT(YEAR(NOW()),'-',MONTH(birthday),'-',DAY(birthday)), '%Y-%m-%d') 
              = CURDATE()
              ORDER BY STR_TO_DATE(CONCAT(YEAR(NOW()),'-',MONTH(birthday),'-',DAY(birthday)), '%Y-%m-%d');";
        $birthday = $this->CI->db->query($query)->result();
        return $birthday;
    }

    function getBirthday($days) {
        $query = "SELECT *
                , CONCAT(MONTHNAME(birthday),' ',DAY(birthday)) AS 'birth' 
              FROM `users_birthday`
              WHERE STR_TO_DATE(CONCAT(YEAR(NOW()),'-',MONTH(birthday),'-',DAY(birthday)), '%Y-%m-%d') 
              BETWEEN CURDATE() AND (CURDATE() + INTERVAL ".$days." DAY)
              ORDER BY STR_TO_DATE(CONCAT(YEAR(NOW()),'-',MONTH(birthday),'-',DAY(birthday)), '%Y-%m-%d');";
        $birthday = $this->CI->db->query($query)->result();
        return $birthday;
    }

    function getBirthdayMonth($month, $limitdays = NULL) {
        $and = '';
        if ($limitdays != NULL)
            $and = "and STR_TO_DATE(CONCAT(YEAR(NOW()),'-',MONTH(birthday),'-',DAY(birthday)), '%Y-%m-%d') 
              BETWEEN STR_TO_DATE(CONCAT(YEAR(NOW()),'-',". $month .",'-',1), '%Y-%m-%d') AND (STR_TO_DATE(CONCAT(YEAR(NOW()),'-',". $month .",'-',1), '%Y-%m-%d') + INTERVAL ".$limitdays." DAY)";
        $query = "SELECT *
                , CONCAT(MONTHNAME(birthday),' ',DAY(birthday)) AS 'birth' 
              FROM `users_birthday`
              WHERE MONTH(birthday) = ".$month." " . $and ."
              ORDER BY STR_TO_DATE(CONCAT(YEAR(NOW()),'-',MONTH(birthday),'-',DAY(birthday)), '%Y-%m-%d');";
        $birthday = $this->CI->db->query($query)->result();
        return $birthday;
    }
    
    function zodiac($birthdate) {
        $zodiac = ""; 

        list ($year, $month, $day) = explode ("-", $birthdate); 

        if     ( ( $month == 3 && $day > 20 ) || ( $month == 4 && $day < 20 ) ) { $zodiac = "Aries"; } 
        elseif ( ( $month == 4 && $day > 19 ) || ( $month == 5 && $day < 21 ) ) { $zodiac = "Taurus"; } 
        elseif ( ( $month == 5 && $day > 20 ) || ( $month == 6 && $day < 21 ) ) { $zodiac = "Gemini"; } 
        elseif ( ( $month == 6 && $day > 20 ) || ( $month == 7 && $day < 23 ) ) { $zodiac = "Cancer"; } 
        elseif ( ( $month == 7 && $day > 22 ) || ( $month == 8 && $day < 23 ) ) { $zodiac = "Leo"; } 
        elseif ( ( $month == 8 && $day > 22 ) || ( $month == 9 && $day < 23 ) ) { $zodiac = "Virgo"; } 
        elseif ( ( $month == 9 && $day > 22 ) || ( $month == 10 && $day < 23 ) ) { $zodiac = "Libra"; } 
        elseif ( ( $month == 10 && $day > 22 ) || ( $month == 11 && $day < 22 ) ) { $zodiac = "Scorpio"; } 
        elseif ( ( $month == 11 && $day > 21 ) || ( $month == 12 && $day < 22 ) ) { $zodiac = "Sagittarius"; } 
        elseif ( ( $month == 12 && $day > 21 ) || ( $month == 1 && $day < 20 ) ) { $zodiac = "Capricorn"; } 
        elseif ( ( $month == 1 && $day > 19 ) || ( $month == 2 && $day < 19 ) ) { $zodiac = "Aquarius"; } 
        elseif ( ( $month == 2 && $day > 18 ) || ( $month == 3 && $day < 21 ) ) { $zodiac = "Pisces"; } 

       return $zodiac; 
    } 
   
    function getCategories() {
        $this->CI->load->model('Category');
        $categories = $this->CI->Category->find_active();
        return $categories;
    }

    function getTags(){
        $this->CI->load->model('Tag');
        return  $this->CI->Tag->find_active();
    }

    function getRecentPosts($limit = null){
        $this->CI->load->model('Post');
        return $this->CI->Post->find_active_top($limit);
    }
    
    function getTopPosts($limit = null, $top = true){
        $this->CI->load->model('Post');
        return $this->CI->Post->find_active_top($limit, $top);
    }

    function isExistFile($filename) {
//        echo $filename;exit;
        if (file_exists($filename)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    
    function isExistNextMenu($position) {
        $this->CI->load->model('Menu');
        $slide = $this->CI->Menu->getNextMenu($position);
        if (!empty($slide)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function isExistPrevMenu($position) {
        $this->CI->load->model('Menu');
        $slide = $this->CI->Menu->getPrevMenu($position);
        if (!empty($slide)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    

    function getSettingByKey($key) {
        $this->CI->load->model('Setting');
        $setting = $this->CI->Setting->findByKey($key);
        if(!empty($setting)){
            return $setting;
        }else{
            return $key;
        }
    }


    function setDefaultLang() {
        $lang = $this->CI->session->userdata('language');

        if (!empty($lang)) {
            $this->CI->config->set_item('language', $this->CI->session->userdata('language'));
        } else {
            $this->CI->config->set_item('language', 'indonesian');
        }
    }

    function getDefaultLang() {
        return $this->CI->config->item('language');
    }

    function getYears() {
        $years = array();

        for ($i = date("Y"); $i > 1945; $i--) {
            $years[$i] = $i;
        }
        return $years;
    }


    function multilevel_select($array,$parent_id = 0,$parents = array(),$selected = null) {
        
        static $i=0;
        if($parent_id==0)
        {
            foreach ($array as $element) {
                if (($element['parent_id'] != 0) && !in_array($element['parent_id'],$parents)) {
                    $parents[] = $element['parent_id'];
                }
            }
        }

        $menu_html = '';
        foreach($array as $element){
            $selected_item = '';
            if($element['parent_id']==$parent_id){
                if($element['id'] == $selected){
                    $selected_item = 'selected';
                }

                $menu_html .= '<option value="'.$element['id'].'" '.$selected_item.'>';
                for($j=0; $j<$i; $j++) {
                    $menu_html .= '&mdash;';
                }
                $menu_html .= $element['name'].'</option>';
                if(in_array($element['id'], $parents)){
                    $i++;
                    $menu_html .= $this->multilevel_select($array, $element['id'], $parents, $selected);
                }
            }
        }
        $i--;
        return $menu_html;
    }


    function bootstrap_menu($array,$parent_id = 0,$parents = array(), $submenu = false)
    {
        if($parent_id==0)
        {
            foreach ($array as $element) {
                if (($element['parent_id'] != 0) && !in_array($element['parent_id'],$parents)) {
                    $parents[] = $element['parent_id'];
                }
            }
        }
        $menu_html = '';
        foreach($array as $element)
        {
            if($element['parent_id']==$parent_id)
            {
                if(in_array($element['id'],$parents))
                {
                    if ($submenu) {
                        $menu_html .= '<li class="dropdown-submenu">';
                        $menu_html .= '<a href="'.site_url($element['url']).'" class="dropdown-toggle" data-toggle="dropdown">'.$element['name'].' <span class="caret"></span></a>';
                    } else {
                        $menu_html .= '<li class="dropdown">';
                        $menu_html .= '<a href="'.site_url($element['url']).'" class="dropdown-toggle" data-toggle="dropdown">'.$element['name'].' <span class="caret"></span></a>';
                    }
                }
                else {
                    $col = '';
                    if ($element['parent_id'] > 0)
                        $col = 'class="col-md-4"';
                    $menu_html .= '<li '.$col.' id='. str_replace(' ', '', $element['name']) .'>';
                    $menu_html .= '<a href="' . site_url($element['url']) . '">' . $element['name'] . '</a>';
                }
                if(in_array($element['id'],$parents))
                {
                    $menu_html .= '<ul class="dropdown-menu columns-3" class="row">';
                    $menu_html .= $this->bootstrap_menu($array, $element['id'], $parents, true);
                    $menu_html .= '</ul>';
                }
                $menu_html .= '</li>';
            }
        }
        return $menu_html;
    }
    
    function sidebar_menu($array,$parent_id = 0,$parents = array(), $submenu = false)
    {
        if ($array != NULL) {
            if($parent_id==0)
            {
                foreach ($array as $element) {
                    if (($element['parent_id'] != 0) && !in_array($element['parent_id'],$parents)) {
                        $parents[] = $element['parent_id'];
                    }
                }
            }
            $menu_html = '';
            foreach($array as $element)
            {
                if($element['parent_id']==$parent_id)
                {
                    $url = site_url($element['url']);
                    if (strpos($element['url'], 'http') !== false) {
                        $url = 'href="' .$element['url']. '"';
                    } else {
                        $url = 'href="' .$url. '"';
                        if($element['url']=='#')
                            $url = '';
                    }
                    if(in_array($element['id_a'],$parents))
                    {
                        if ($submenu) {
                            $menu_html .= '<li data-toggle="collapse" data-target="#' . str_replace(' ', '', $element['name']) . '" class="collapsed">';
                            $menu_html .= '<a '.$url.'><i class="fa '. $element['icon'] .' fa-lg"></i> '.$element['name'].' <span class="arrow"></span></a>';
                        } else {
                            $menu_html .= '<li data-toggle="collapse" data-target="#' . str_replace(' ', '', $element['name']) . '" class="collapsed">';
                            $menu_html .= '<a '.$url.'><i class="fa '. $element['icon'] .' fa-lg"></i> '.$element['name'].' <span class="arrow"></span></a>';
                        }
                    }
                    else {
                        $icon = '';
                        if ($element['icon'] != '')
                            $icon = '<i class="fa '. $element['icon'] .' fa-lg"></i> ';
                        $menu_html .= '<li class="">';
                        $menu_html .= '<a '. $url . '> '. $icon . $element['name'] . '</a>';
                    }
                    $menu_html .= '</li>';
                    if(in_array($element['id_a'],$parents))
                    {
                        $menu_html .= '<ul class="sub-menu collapse" id="'. str_replace(' ', '', $element['name']) .'">';
                        $menu_html .= $this->sidebar_menu($array, $element['id_a'], $parents, true);
                        $menu_html .= '</ul>';
                    }
                }
            }
            return $menu_html;
        } else {
            return NULL;
        }
        
    }
   
}

?>
