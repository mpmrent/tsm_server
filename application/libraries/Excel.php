<?php
class Excel {

    private $excel;

    public function __construct() {
        // initialise the reference to the codeigniter instance
        require_once APPPATH.'libraries/PHPExcel.php';
        $this->excel = new PHPExcel();    
    }

    public function load($path) {
        $objReader = PHPExcel_IOFactory::createReader('Excel5');
        $this->excel = $objReader->load($path);
    }

    public function save($path) {
        // Write out as the new file
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save($path);
    }

    public function stream($filename) {       
        header('Content-type: application/ms-excel');
        header("Content-Disposition: attachment; filename=\"".$filename."\""); 
        header("Cache-control: private");        
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');    
    }

    public function  __call($name, $arguments) {  
        // make sure our child object has this method  
        if(method_exists($this->excel, $name)) {  
            // forward the call to our child object  
            return call_user_func_array(array($this->excel, $name), $arguments);  
        }  
        return null;  
    }  
    
    public function table_export($dataTables, $nameFile) {
        ini_set('memory_limit', '512M');
        // Instantiate a new PHPExcel object 
        $objPHPExcel = new PHPExcel();  
        $objPHPExcel->getProperties()->setCreator("Dimas Lazuardy I")
                             ->setLastModifiedBy("Dimas Lazuardy I")
                             ->setTitle("PHPExcel")
                             ->setSubject("PHPExcel")
                             ->setDescription("PHPExcel, generated using PHP classes.")
                             ->setKeywords("office PHPExcel php")
                             ->setCategory("Result");

        $sheet = 0;
        foreach ($dataTables as $name => $table)
        {
            $objPHPExcel->setActiveSheetIndex($sheet);
            if ($name != 'Tickets') {
                //start of printing column names as names of MySQL fields  
                $colInd = 'A';  
                //Initialise the Excel row number 
                $rowInd = 1;  
                foreach ($table->list_fields() as $field)
                {
                    $objPHPExcel->getActiveSheet()->setTitle($name);
                    $objPHPExcel->getActiveSheet()->setCellValue($colInd.$rowInd, $field);
                    //change the font size
                    $objPHPExcel->getActiveSheet()->getStyle($colInd.$rowInd)->getFont()->setSize(10);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($colInd)->setWidth(strlen($field)+5);
                    //make the font become bold
                    $objPHPExcel->getActiveSheet()->getStyle($colInd.$rowInd)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle($colInd.$rowInd)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $colInd++;
                }
                $rowInd = 2;  
                foreach ($table->result_array() as $row)
                {
                    $colInd = 'A'; 
                    foreach ($row as $column)
                    {
                        //print_r($column);
                        $objPHPExcel->getActiveSheet()->setCellValue($colInd.$rowInd, $column);
                        //change the font size
                        $objPHPExcel->getActiveSheet()->getStyle($colInd.$rowInd)->getFont()->setSize(10);
                        $colInd++;
                    }
                    $rowInd++;
                }
            } else {
                $this->getTicket($table, $objPHPExcel);
            }
            $sheet++;
            $objPHPExcel->createSheet($sheet);
        }
        $objPHPExcel->removeSheetByIndex($sheet);
        $objPHPExcel->setActiveSheetIndex(0);
        $date = date("y m d h-i");
        $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( ' memoryCacheSize ' => '512MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        // Save Excel 2007 file
        #echo date('H:i:s') . " Write to Excel2007 format\n";
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'. $date .' ' . $nameFile . '.xlsx"');
        $objWriter->save('php://output');
        return TRUE;
    }
    
    public function getTicket($tickets, $excel) {
        $excel->getActiveSheet()->setTitle('Tickets');
        $excel->getActiveSheet()->setCellValue('A1', 'All Tickets');
        $excel->getActiveSheet()->mergeCells('A1:W1');
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
//        //set column width
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        
        // set table header
        $header = array('Ticket Id', 'Subject',
                    'Description', 'Ticket Category',
                    'Status', 'Priority', 'PIC',
                    'User Contact', 'Created Date',
                    'Created By', 'Completed Date Old', 'Completed Date',
                    'Completed By', 'Assigned Date',
                    'Assigned By', 'Closed Date Old', 'Closed Date',
                    'Closed By', 'Took Date Old', 'Took Date',
                    'Took By', 'By Vendor Date',
//                    'Tes', 
//                    'Response Time', 'Queued Time',
                    'Done Time'
//                    , 'Solved Time'
                ); 	
        $alfa = 'A';		
        foreach ($header as $head) {
            $excel->getActiveSheet()->setCellValue($alfa.'3', $head);
            $alfa++;
        }
        $excel->getActiveSheet()->getStyle('A3:W3')->getFont()->setBold(true);
        $cellRow=3;
        foreach ($tickets->result() as $row) { 
            $cellRow++;
            $status = $row->TICKET_STATUS;
            $do_date = $row->DO_DATE;
            if ($status=='Assigned') {
                $done_time = "";
            } else if ($status=='Progress') {
                $done_time = "";
            } else if ($status=='Complete') {
                if ($row->DO_DATE!="") {
//                    $cekHour = date('H', strtotime($row->LAST_EDIT_DATE));
                    $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->DO_DATE))/60,0);
                    if ($done_time > 5760 ) {
                        $done_time = 5760;
                        $do_date = date('Y-m-d H:i:s', strtotime($row->LAST_EDIT_DATE." -4 days"));
                    } else {
                        $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->DO_DATE))/60,0);
                        $date1 = date('z', strtotime($row->DO_DATE." -1 days"));
                        $date2 = date('z', strtotime($row->LAST_EDIT_DATE." -1 days"));
                        $diff = $date2 - $date1;
                        if (($diff > 1) && ($done_time > 900)) {
                            $done_time = $done_time - ($diff * 15 * 60);
                        }
                    }
                } else {
                    $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->LAST_EDIT_DATE))/60,0);
                }
                $solved_time = "";
            } else if ($status=='Closed') {
                if ($row->LAST_EDIT_DATE=="") {
                    if ($row->DO_DATE!="") {
                        $done_time = round(abs(strtotime($row->CLOSE_DATE) - strtotime($row->DO_DATE))/60,0);
                        $date1 = date('z', strtotime($row->DO_DATE." -1 days"));
                        $date2 = date('z', strtotime($row->CLOSE_DATE." -1 days"));
                        $diff = $date2 - $date1;
                        if (($diff > 1) && ($done_time > 900)) {
                            $done_time = $done_time - ($diff * 15 * 60);
                        }
                    } else {
                        $done_time = round(abs(strtotime($row->CLOSE_DATE) - strtotime($row->CLOSE_DATE))/60,0);
                    }
                } else {
                    if ($row->DO_DATE!="") {
                        $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->DO_DATE))/60,0);
                        $date1 = date('z', strtotime($row->DO_DATE." -1 days"));
                        $date2 = date('z', strtotime($row->LAST_EDIT_DATE." -1 days"));
                        $diff = $date2 - $date1;
                        if (($diff > 1) && ($done_time > 900)) {
                            $done_time = $done_time - ($diff * 15 * 60);
                        }
                    } else {
                        $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->LAST_EDIT_DATE))/60,0);
                    }
                }   
            } else {
                $done_time = "";
            }
            
            $value_row = array($row->TICKET_ID, $row->SUBJECT,
                            $row->DESCRIPTION, $row->TICKET_CATEGORY,
                            $row->TICKET_STATUS, $row->PRIORITY, $row->PIC_USERNAME,
                            $row->USER_CONTACT, $row->CREATE_DATE,
                            $row->CREATED_BY, $row->LAST_EDIT_DATE_OLD, $row->LAST_EDIT_DATE,
                            $row->LAST_EDIT_BY, $row->ASSIGNED_DATE,
                            $row->ASSIGNED_BY, $row->CLOSE_DATE_OLD, $row->CLOSE_DATE,
                            $row->CLOSED_BY, $row->DO_DATE_OLD, $do_date,
                            $row->DO_BY, $row->BY_VENDOR_DATE, 
//                            $row->tes,
//                            $response_time, $queue_time, 
                            $done_time
//                            , $solved_time
                        );			

            $alfa = 'A';
            foreach ($value_row as $val) {
                $excel->getActiveSheet()->setCellValue($alfa . $cellRow, $val);
                $alfa++;
            }
        }
    }
    
    public function all_ticket_export_excel($tickets) {
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Ticket');
        $this->excel->getActiveSheet()->setCellValue('A1', 'All Tickets');
        $this->excel->getActiveSheet()->mergeCells('A1:W1');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
//        //set column width
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        
        // set table header
        $header = array('Ticket Id', 'Subject',
                    'Description', 'Ticket Category',
                    'Status', 'Priority', 'PIC',
                    'User Contact', 'Created Date',
                    'Created By', 'Completed Date Old', 'Completed Date',
                    'Completed By', 'Assigned Date',
                    'Assigned By', 'Closed Date Old', 'Closed Date',
                    'Closed By', 'Took Date Old', 'Took Date',
                    'Took By', 'By Vendor Date',
//                    'Tes', 
//                    'Response Time', 'Queued Time',
                    'Done Time'
//                    , 'Solved Time'
                ); 	
        $alfa = 'A';		
        foreach ($header as $head) {
            $this->excel->getActiveSheet()->setCellValue($alfa.'3', $head);
            $alfa++;
        }
        $this->excel->getActiveSheet()->getStyle('A3:W3')->getFont()->setBold(true);
        $cellRow=3;
        foreach ($tickets->result() as $row) { 
            $cellRow++;
            $status = $row->TICKET_STATUS;
            $do_date = $row->DO_DATE;
            if ($status=='Assigned') {
                $done_time = "";
            } else if ($status=='Progress') {
                $done_time = "";
            } else if ($status=='Complete') {
                if ($row->DO_DATE!="") {
//                    $cekHour = date('H', strtotime($row->LAST_EDIT_DATE));
                    $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->DO_DATE))/60,0);
                    if ($done_time > 5760 ) {
                        $done_time = 5760;
                        $do_date = date('Y-m-d H:i:s', strtotime($row->LAST_EDIT_DATE." -4 days"));
                    } else {
                        $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->DO_DATE))/60,0);
                        $date1 = date('z', strtotime($row->DO_DATE." -1 days"));
                        $date2 = date('z', strtotime($row->LAST_EDIT_DATE." -1 days"));
                        $diff = $date2 - $date1;
                        if (($diff > 0) && ($done_time > 900)) {
                            $done_time = $done_time - ($diff * 15 * 60);
                        }
                    }
                } else {
                    $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->LAST_EDIT_DATE))/60,0);
                }
                $solved_time = "";
            } else if ($status=='Closed') {
                if ($row->LAST_EDIT_DATE=="") {
                    if ($row->DO_DATE!="") {
                        $done_time = round(abs(strtotime($row->CLOSE_DATE) - strtotime($row->DO_DATE))/60,0);
                        $date1 = date('z', strtotime($row->DO_DATE." -1 days"));
                        $date2 = date('z', strtotime($row->CLOSE_DATE." -1 days"));
                        $diff = $date2 - $date1;
                        if (($diff > 0) && ($done_time > 900)) {
                            $done_time = $done_time - ($diff * 15 * 60);
                        }
                    } else {
                        $done_time = round(abs(strtotime($row->CLOSE_DATE) - strtotime($row->CLOSE_DATE))/60,0);
                    }
                } else {
                    if ($row->DO_DATE!="") {
                        $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->DO_DATE))/60,0);
                        $date1 = date('z', strtotime($row->DO_DATE." -1 days"));
                        $date2 = date('z', strtotime($row->LAST_EDIT_DATE." -1 days"));
                        $diff = $date2 - $date1;
                        if (($diff > 0) && ($done_time > 900)) {
                            $done_time = $done_time - ($diff * 15 * 60);
                        }
                    } else {
                        $done_time = round(abs(strtotime($row->LAST_EDIT_DATE) - strtotime($row->LAST_EDIT_DATE))/60,0);
                    }
                }   
            } else {
                $done_time = "";
            }
            
            $value_row = array($row->TICKET_ID, $row->SUBJECT,
                            $row->DESCRIPTION, $row->TICKET_CATEGORY,
                            $row->TICKET_STATUS, $row->PRIORITY, $row->PIC_USERNAME,
                            $row->USER_CONTACT, $row->CREATE_DATE,
                            $row->CREATED_BY, $row->LAST_EDIT_DATE_OLD, $row->LAST_EDIT_DATE,
                            $row->LAST_EDIT_BY, $row->ASSIGNED_DATE,
                            $row->ASSIGNED_BY, $row->CLOSE_DATE_OLD, $row->CLOSE_DATE,
                            $row->CLOSED_BY, $row->DO_DATE_OLD, $do_date,
                            $row->DO_BY, $row->BY_VENDOR_DATE, 
//                            $row->tes,
//                            $response_time, $queue_time, 
                            $done_time
//                            , $solved_time
                        );			

            $alfa = 'A';
            foreach ($value_row as $val) {
                $this->excel->getActiveSheet()->setCellValue($alfa . $cellRow, $val);
                $alfa++;
            }
        }

        $filename = 'tickets.xlsx';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $filename . '"'); //tell the browser what's the filename
        header('Cache-Control: max-age=0'); // no chache
        //save it to Excel5 format(excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want ti save it as .XLSX excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }
}

?>