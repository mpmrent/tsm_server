<ul class="nav nav-list">
    <?php 
        $menu_active = $this->session->userdata('menu_active');
        foreach ($menu->result() as $value) {
    ?>
    <?php if (($value->parent == 0) && ($value->group == 'main_menu')) { ?>
    <li class="<?php echo (!isset($menu_active)) ? ($value->id == 1) ? 'active' : '' : ($menu_active['id'] == $value->id) ? 'active' : ($menu_active['parent'] == $value->id) ? 'active' : '' ; ?>">
        <a href="<?php echo ($value->url == '#') ? '#" class="dropdown-toggle"' : base_url($value->url); ?>">
            <i class="menu-icon fa <?php echo $value->icon ?>"></i>
            <span class="menu-text"> <?php echo $value->menu ?> </span>
            <b class="arrow <?php echo ($value->url == '#') ? 'fa fa-angle-down' : ''; ?>"></b>
        </a>
        <?php if ($value->url == '#') { ?>
            <ul class="submenu">
            <?php foreach ($menu->result() as $submenu) { ?>
            <?php if ($value->id == $submenu->parent) { ?>
            <li id="<?php echo $submenu->id ?>" class="<?php echo (isset($menu_active)) ? ($menu_active['id'] == $submenu->id) ? 'active' : '' : '' ; ?>">
                <a href="<?php echo base_url($submenu->url)?>">
                    <i class="menu-icon fa <?php echo $submenu->icon ?>"></i>
                    <?php echo $submenu->menu ?>
                </a>

                <b class="arrow"></b>
            </li>
            <?php } ?>
            <?php } ?>
            </ul>
        <?php } ?>
    </li>
    <?php }} ?>
    <li>
    </li>
</ul>
