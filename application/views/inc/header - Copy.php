<style type="text/css">
    .ace-nav>li>a:hover {
        background-color: rgba(242, 111, 33, 1) ;
    }
</style>

<div id="navbar" class="navbar navbar-default navbar-fixed-top">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {}
    </script>
    <div class="navbar-container" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="navbar-header pull-left">
            <a href="#" class="navbar-brand">
                <small>
                    <img style="height: 25px;" src="<?php echo base_url(); ?>assets/images/MPM_OPCO_sm.png">
                    <span class="white">Timesheet</span>
                    <span class="mpm-orange" style="font-weight: bold;" id="id-text2">Mobile</span>
                </small>
            </a>
        </div>
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="grey">
                    <a style="background-color: transparent;" data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="ace-icon fa fa-tasks"></i>
                        <span class="badge badge-important"><?php echo $overtime->num_rows() ?></span>
                    </a>
                    <ul id="pending" class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                        <li class="dropdown-header">
                            <i class="ace-icon fa fa-tasks"></i> <?php echo $overtime->num_rows() ?> Kerja lembur
                        </li>
                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar">
                            <?php foreach ($overtime->result() as $row) { ?>
                                <li>
                                    <a href="#">
                                        <div class="clearfix">
                                            <span class="pull-left" style="width: 100px;">
                                                <i class="btn btn-xs no-hover btn-pink fa fa-clock-o"></i>
                                                <?php echo $row->fullname ?>
                                                <i class="btn btn-xs no-hover btn-pink">
                                                    <?php echo $row->id ?>
                                                </i>
                                            </span>
                                            <span style="margin-bottom: 2px;" class="pull-right badge badge-info"><?php echo $row->start_time ?></span>
                                            <span class="pull-right badge badge-info"><?php echo $row->end_time ?></span>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                            </ul>
                        </li>
                        <?php if ($overtime->num_rows() == 0) { ?>
                        <li class="dropdown-footer">
                            <a href="#">
                                Tidak ada sopir yang berkerja lembur...
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="grey">
                    <a style="background-color: transparent;" data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="ace-icon fa fa-bell <?php echo ($pending->num_rows() > 0) ? 'icon-animated-bell' : ''  ?>"></i>
                        <span class="badge badge-important"><?php echo $pending->num_rows() ?></span>
                    </a>
                    <ul style="left: -100px;" class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
                        <li class="dropdown-header">
                            <i class="ace-icon fa fa-exclamation-triangle"></i> <?php echo $pending->num_rows() ?> Menunggu Persetujuan
                        </li>
                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar navbar-pink">
                            <?php foreach ($pending->result() as $row) { ?>
                                <li>
                                    <a href="#">
                                        <div class="clearfix">
                                            <span class="pull-left">
                                                <i class="btn btn-xs no-hover btn-pink fa fa-check"></i>
                                                <?php echo $row->fullname  ?>
                                            </span>
                                            <span class="pull-right badge badge-info"><?php echo $row->start_time ?></span>
                                            <span class="pull-right badge badge-info"><?php echo $row->end_time ?></span>
                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <?php if ($pending->num_rows() > 0) { ?>
                                <a href="<?php echo base_url() ?>index.php/dev_ops/main/pending">
                                    Lihat semua permintaan persetujuan
                                    <i class="ace-icon fa fa-arrow-right"></i>
                                </a>
                            <?php } else { ?>
                                <a href="#">
                                    Tidak ada permintaan persetujuan...
                                </a>
                            <?php } ?>
                        </li>
                    </ul>
                </li>
                <li class="light-blue">
                    <a style="background-color: transparent;" data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="<?php echo base_url(); ?>assets/images/rentoguchi-box.png" alt="Jason's Photo" />
                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>
                    <ul style="left: 32%!important;" class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <div style="padding-left: 20px; color: rgba(0, 175, 240, 1);">
                            <small>Selamat datang,</small>
                            <?php echo $this->session->userdata('firstname') ?>
                        </div>
                        <?php 
                            foreach ($menu->result() as $value) {
                        ?>
                        <?php if (($value->parent == 0) && ($value->group == 'admin')) { ?>
                            <li>
                                <a href="<?php echo $value->url ?>">
                                    <i class="ace-icon fa <?php echo $value->icon ?>"></i>
                                    <?php echo $value->menu ?>
                                </a>
                            </li>
                            <?php } } ?>
                            <li>
                                  <a data-toggle="modal" href="#changePasswordModal">
                                      <i class="ace-icon fa fa-key"></i>
                                      Ubah Kata Sandi
                                  </a>
                            </li><hr style="margin: 0px;">
                            <li>
                                  <a href="<?php echo base_url() ?>/index.php/main/logout" id="tutup_kasir">
                                      <i class="ace-icon fa fa-power-off"></i>
                                      Keluar
                                  </a>
                            </li>
                    </ul>
                </li>
                <!-- <li class="green">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="ace-icon fa fa-envelope icon-animated-vertical"></i>
                        <span class="badge badge-success">5</span>
                    </a>
                    <ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
                        <li class="dropdown-header">
                            <i class="ace-icon fa fa-envelope-o"></i> 5 Messages
                        </li>
                        <li class="dropdown-content">
                            <ul class="dropdown-menu dropdown-navbar">
                                <li>
                                    <a href="#" class="clearfix">
                                        <img src="<?php echo base_url(); ?>assets/avatars/avatar.png" class="msg-photo" alt="Alex's Avatar" />
                                        <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">Alex:</span> Ciao sociis natoque penatibus et auctor ...
                                        </span>
                                        <span class="msg-time">
                                            <i class="ace-icon fa fa-clock-o"></i>
                                            <span>a moment ago</span>
                                        </span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="clearfix">
                                        <img src="<?php echo base_url(); ?>assets/avatars/avatar3.png" class="msg-photo" alt="Susan's Avatar" />
                                        <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">Susan:</span> Vestibulum id ligula porta felis euismod ...
                                        </span>
                                        <span class="msg-time">
                                            <i class="ace-icon fa fa-clock-o"></i>
                                            <span>20 minutes ago</span>
                                        </span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="clearfix">
                                        <img src="<?php echo base_url(); ?>assets/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar" />
                                        <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">Bob:</span> Nullam quis risus eget urna mollis ornare ...
                                        </span>
                                        <span class="msg-time">
                                            <i class="ace-icon fa fa-clock-o"></i>
                                            <span>3:15 pm</span>
                                        </span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="clearfix">
                                        <img src="<?php echo base_url(); ?>assets/avatars/avatar2.png" class="msg-photo" alt="Kate's Avatar" />
                                        <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">Kate:</span> Ciao sociis natoque eget urna mollis ornare ...
                                        </span>
                                        <span class="msg-time">
                                            <i class="ace-icon fa fa-clock-o"></i>
                                            <span>1:33 pm</span>
                                        </span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="clearfix">
                                        <img src="<?php echo base_url(); ?>assets/avatars/avatar5.png" class="msg-photo" alt="Fred's Avatar" />
                                        <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">Fred:</span> Vestibulum id penatibus et auctor ...
                                        </span>
                                        <span class="msg-time">
                                            <i class="ace-icon fa fa-clock-o"></i>
                                            <span>10:09 am</span>
                                        </span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="inbox.html">
                                See all messages
                                <i class="ace-icon fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li> -->
            </ul>
        </div>
    </div>
    <!-- /.navbar-container -->
<div class="row">
    <div class="col-md-12">
            <div class="modal fade" style="z-index: 105110;" id="changePasswordModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <form id="changePasswordForm" action="<?php echo base_url() ?>index.php/dev_ops/main/changePassword" method="post" class="form-horizontal form-label-left">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="modalTitle">Ubah Password</h4>
                            </div>
                            <div class="modal-body" id="menu-priv">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Kata sandi Lama </label>
                                    <div class="col-sm-9">
                                        <input type="Password" id="form-field-1" name="old_password" placeholder="Kata sandi Lama" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Kata sandi Baru </label>
                                    <div class="col-sm-9">
                                        <input type="Password" id="form-field-1" name="new_password" placeholder="Kata sandi Baru" class="col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-field-1"> Konfirmasi Kata sandi </label>
                                    <div class="col-sm-9">
                                        <input type="Password" id="form-field-1" name="konf_new_password" placeholder="Konfirmasi Kata sandi" class="col-xs-12">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group right">
                                    <div class="col-md-12">
                                        <button data-dismiss="modal" class="btn btn-default">Batal</button>
                                        <button id="edt_btn" type="submit" class="btn btn-success">Proses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>
</div>
