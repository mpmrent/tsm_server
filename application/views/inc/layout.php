<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Timesheet Mobile - MPM Rent</title>
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/images/icon.png'); ?>">

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/4.2.0/css/font-awesome.min.css" />
    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/fonts.googleapis.com.css" />-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-timepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/daterangepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables.net-bs/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables.net-responsive-bs/css/responsive.bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url(); ?>assets/js/ace-extra.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.custom.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.min.css" />
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAq2dSBNr0wUca3LUGZSwKOdeCDuIHLA0E&callback=initMap"
  type="text/javascript"></script>
    
    <style>
        * {
            font-family: 'Roboto', sans-serif;
        }
        
        .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
            padding: 4px;
            /* line-height: 0; */
            vertical-align: middle;
            border-top: 1px solid #ddd;
        }
        
        td p {
            margin: auto;
        }
    </style>

</head>

<body class="no-skin">
    <?php echo $header; ?>
    <div class="main-container" id="main-container">
        <script type="text/javascript">
            try {
                ace.settings.check('main-container', 'fixed')
            } catch (e) {}
        </script>
        <div id="sidebar" class="sidebar responsive">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {}
            </script>

            <!-- <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                    <button class="btn btn-success disabled">
                        <i class="ace-icon fa fa-signal"></i>
                    </button>
                    <button class="btn btn-info disabled">
                        <i class="ace-icon fa fa-pencil"></i>
                    </button>
                    <button class="btn btn-warning disabled">
                        <i class="ace-icon fa fa-users"></i>
                    </button>
                    <button class="btn btn-danger disabled">
                        <i class="ace-icon fa fa-cogs"></i>
                    </button>
                </div>
                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span>
                    <span class="btn btn-info"></span>
                    <span class="btn btn-warning"></span>
                    <span class="btn btn-danger"></span>
                </div>
            </div> -->
            <!-- /.sidebar-shortcuts -->
            <?php echo $sidemenu ?>
            <!-- /.nav-list -->
            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {}
            </script>
        </div>
        <div class="main-content" style="overflow: hidden;">
            <div class="main-content-inner">
                <div class="breadcrumbs" id="breadcrumbs">
                    <script type="text/javascript">
                        try {
                            ace.settings.check('breadcrumbs', 'fixed')
                        } catch (e) {}
                    </script>
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="<?php echo base_url() ?>">Beranda</a>
                        </li>
                        <?php if ($this->session->userdata('menu_active')['parent'] != 0) { ?>
                        <li class="active">
                            <i class="ace-icon fa <?php echo $this->session->userdata('menu_active')['parent_icon'] ?> home-icon"></i>
                            <?php echo $this->session->userdata('menu_active')['parent_title'] ?>
                        </li>
                        <li class="active"><?php echo $this->session->userdata('menu_active')['title'] ?></li>
                        <?php } ?>
                    </ul>
                    <!-- /.breadcrumb -->
                </div>
                <div class="page-content">
                    <?php echo $tools ?>
                    <!-- /.main-container -->
                    <script src="<?php echo base_url(); ?>assets/js/jquery.2.1.1.min.js"></script>
                    <!--[if !IE]> -->
                    <script type="text/javascript">
                        window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.min.js'>" + "<" + "/script>");
                    </script>
                    <script type="text/javascript">
                        if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
                    </script>

                    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
                    <script src="<?php echo base_url();?>assets/plugins/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
                    <script src="<?php echo base_url();?>assets/plugins/datatables.net-responsive/js/dataTables.responsive.js" type="text/javascript"></script>  
                    <script src="<?php echo base_url();?>assets/plugins/datatables.net-responsive-bs/js/responsive.bootstrap.js" type="text/javascript"></script> 
                    <script src="<?php echo base_url()?>assets/js/jquery.dataTables.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/fileinput.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
                    <script src="<?php echo base_url()?>assets/plugins/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
                    <script src="<?php echo base_url()?>assets/plugins/datatables.net-buttons-bs/js/buttons.bootstrap.js"></script>
                    <script src="<?php echo base_url()?>assets/plugins/datatables.net-buttons/js/buttons.flash.js"></script>
                    <script src="<?php echo base_url()?>assets/plugins/datatables.net-buttons/js/buttons.html5.js"></script>
                    <script src="<?php echo base_url()?>assets/plugins/datatables.net-buttons/js/buttons.colVis.js"></script>
                    <script src="<?php echo base_url()?>assets/plugins/datatables.net-buttons/js/buttons.print.js"></script>
                    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine.js"></script>
                    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js"></script>
                    <script src="<?php echo base_url()?>assets/js/dataTables.tableTools.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/dataTables.colVis.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/bootstrap-timepicker.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/moment.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/daterangepicker.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/jquery-ui.custom.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/jquery.ui.touch-punch.min.js"></script>
                    <script src="<?php echo base_url()?>assets/js/chosen.jquery.min.js"></script>
                    <!--<script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>-->  
                    <!-- jquery.inputmask -->
                    <!--<script src="<?php echo base_url();?>assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>-->


                    <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div id="popUp-header" class="modal-header bg-primary">
                                    <!-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                    </button> -->
                                    <h4 class="modal-title" id="popUp-title">PIN</h4>
                                </div>
                                <div class="modal-body" id="popUp-body"></div>
                                <div class="modal-footer" id="popUp-footer"></div>
                            </div>
                        </div>
                    </div>
                    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

                    <script>
                        function showPopUp(title, body, footer) {
                            $('#popUp-title').html(title);
                            $('#popUp-body').html(body);
                            $('#popUp-footer').html(footer);
                            $('#popUp').modal('show');
                        }


                        // function showPopUpConfirm(title, body, url) {
                        //     $('#popUp-title').html(title);
                        //     $('#popUp-body').html(body+ '<span style="font-size: 15px;">Berikan nilai untuk service yang anda terima.<br>あなたが受け取ったサービスに価値を与える<br>'+'<div class="radio-toolbar" style="text-align: center;">'+
                        //                     '<input onclick="disb(\''+url+'\', 1);" type="radio"  id="radioImg1" ng-model="datapin.radio" value="2" required name="radio1" id="radio1"/>'+
                        //                     '<label for="radio1" class="rad">'+
                        //                     '<img src="<?php echo base_url(); ?>assets/images/Bintang-kuning.png">'+
                        //                     '</label>'+
                        //                     '<input onclick="disb(\''+url+'\', 2);" type="radio" id="radioImg2" ng-model="datapin.radio" value="4" name="radio" id="radio2"/>'+
                        //                     '<label for="radio2" class="rad">'+
                        //                     '<img src="<?php echo base_url(); ?>assets/images/Bintang-kuning.png">'+
                        //                     '</label>'+
                        //                     '<input onclick="disb(\''+url+'\', 3);" type="radio" id="radioImg3" ng-model="datapin.radio" value="6" name="radio" id="radio3" />'+
                        //                     '<label for="radio3" class="rad">'+
                        //                     '<img src="<?php echo base_url(); ?>assets/images/Bintang-kuning.png">'+
                        //                     '</label>'+
                        //                     '<input onclick="disb(\''+url+'\', 4);" type="radio" id="radioImg4" ng-model="datapin.radio" value="8" name="radio" id="radio4"/>'+
                        //                     '<label for="radio4" class="rad">'+
                        //                     '<img src="<?php echo base_url(); ?>assets/images/Bintang-kuning.png">'+
                        //                     '</label>'+
                        //                     '</div>');
                        //     $('#popUp-footer').html(
                        //         '<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>'
                        //         + '<a type="button" id="btn-approve-dis" disabled href="'+url+'" class="btn btn-sm btn-primary">Ok</a>'
                        //     );
                        //     $('#popUp').modal('show');
                        //     // alert('tes');
                        // }

                        // function disb(url, val) {
                        //     $('input:radio').change(function(){
                        //          $('#btn-approve-dis').removeAttr('disabled');
                        //          $('#btn-approve-dis').attr('href', url + $('input[name=radio]:checked').val());
                        //          // alert($('input[name=radio]:checked').val());
                        //         // for (var i = 1; i <= 4; i++) {
                        //         //     document.getElementById('radioImg'+i).src='img/Bintang-Abu.png';
                        //         // } 
                        //         // for (var i = 1; i <= val; i++) {
                        //         //     document.getElementById('radioImg'+i).src='img/Bintang-Kuning.png';
                        //         // }
                        //     });      
                        // };


                        function showPopUpConfirm(title, body, url) {
                            $('#popUp-title').html(title);
                            $('#popUp-body').html(body+ '<span style="font-size: 15px;">Berikan nilai untuk service yang anda terima.<br>あなたが受け取ったサービスに価値を与える<br>'+'<div class="radio-toolbar" style="text-align: center;">'+
                                            '<input onclick="disb(\''+url+'\', 1);"  type="radio" ng-model="datapin.radio" value="1" required name="radio1" id="radio1"/>'+
                                            '<label for="radio1" class="rad">'+
                                            '<img  id="radioImg1" src="<?php echo base_url(); ?>assets/images/Bintang-Abu.png">'+
                                            '</label>'+
                                            '<input onclick="disb(\''+url+'\', 2);" type="radio" ng-model="datapin.radio" value="2" name="radio" id="radio2"/>'+
                                            '<label for="radio2" class="rad">'+
                                            '<img id="radioImg2" src="<?php echo base_url(); ?>assets/images/Bintang-Abu.png">'+
                                            '</label>'+
                                            '<input onclick="disb(\''+url+'\', 3);" type="radio" ng-model="datapin.radio" value="3" name="radio" id="radio3" />'+
                                            '<label for="radio3" class="rad">'+
                                            '<img id="radioImg3" src="<?php echo base_url(); ?>assets/images/Bintang-Abu.png">'+
                                            '</label>'+
                                            '<input onclick="disb(\''+url+'\', 4);" type="radio" ng-model="datapin.radio" value="4" name="radio" id="radio4"/>'+
                                            '<label for="radio4" class="rad">'+
                                            '<img id="radioImg4" src="<?php echo base_url(); ?>assets/images/Bintang-Abu.png">'+
                                            '</label>'+
                                            '<input onclick="disb(\''+url+'\', 5);" type="radio" ng-model="datapin.radio" value="5" name="radio" id="radio5"/>'+
                                            '<label for="radio5" class="rad">'+
                                            '<img id="radioImg5" src="<?php echo base_url(); ?>assets/images/Bintang-Abu.png">'+
                                            '</label>'+
                                            '</div>');
                            $('#popUp-footer').html(
                                '<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>'
                                + '<a type="button" id="btn-approve-dis" disabled href="'+url+'" class="btn btn-sm btn-primary">Ok</a>'
                            );
                            $('#popUp').modal('show');
                            // alert('tes');
                        }

                        function disb(url, val) {
                            $('input:radio').change(function(){
                                 $('#btn-approve-dis').removeAttr('disabled');
                                 $('#btn-approve-dis').attr('href', url + $('input[name=radio]:checked').val());
                                 // alert($('input[name=radio]:checked').val());
                            });      
                            for (var i = 1; i <= 5; i++) {
                                document.getElementById('radioImg'+i).src='<?php echo base_url(); ?>assets/images/Bintang-Abu.png';
                            } 
                            for (var i = 1; i <= val; i++) {
                                document.getElementById('radioImg'+i).src='<?php echo base_url(); ?>assets/images/Bintang-Kuning.png';
                            }
                        }
                    </script>

                    <?php echo $content ?>

                    <hr>
                    <div class="col-xs-12 col-sm-12 widget-container-col">
                        <div class="widget-box widget-color-dark light-border">
                            <div class="widget-header">
                                <h5 class="widget-title smaller">Informasi</h5>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main padding-2">
                                    <div class="alert alert-info" style="padding: 2px; ">
                                        <p class="MsoNormal" style="text-align: justify; margin: 10px 10px; font-size: 11px;">
                                        Pelanggan dan pengguna jasa (user) wajib melakukan pemeriksaan dan memberikan persetujuan terhadap Timesheet Mobile. Dalam hal user tidak atau belum melakukan persetujuan terhadap Timesheet Mobile yang diajukan oleh Pengemudi selambatnya 5 (lima) hari kalender setelah tanggal aktual pelaksanaan pekerjaan yang dilakukan Pengemudi, maka Timesheet Mobile secara otomatis dianggap sudah diperiksa dan disetujui oleh User dan Pelanggan, oleh karena itu Perusahaan berhak untuk menyampaikan tagihan berdasarkan Timesheet Mobile dan Pelanggan wajib untuk melakukan pembayaran sesuai jumlah yang tercantum dalam tagihan dan dalam batas waktu yang telah ditentukan.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.page-content -->
            </div>
        </div>
        <!-- /.main-content -->
        <?php echo $footer; ?>
        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
            <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </a>
    </div>
    <!-- ace scripts -->
    <script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.custom.min.js"></script>
    <!--<script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>-->
<!--    <script src="<?php echo base_url(); ?>assets/js/jquery.easypiechart.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.flot.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.flot.pie.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.flot.resize.min.js"></script> -->
</body>

<script src="<?php echo base_url('node_modules/socket.io/node_modules/socket.io-client/socket.io.js');?>"></script>

<script>
        var socket = io.connect( 'http://'+window.location.hostname+':3003' );

        // $(document).ready(function() {
        //     socket.emit('notif_bc', { 
        //         title: 'tes',
        //         text: 'Pesan'
        //     });
        // });
</script>

</html>