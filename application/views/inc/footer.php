<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
            <span class="bigger-120"> Copyright &copy; 2017 |
                <span class="mpm-orange" style="font-weight: bold;" id="id-text2">MPM Rent</span>
            </span>
            &nbsp; &nbsp;
            <span class="action-buttons" id="btn-playstore">
                <a target="_blank" href="https://play.google.com/store/apps/details?id=timesheet.mobile.mpm_rent.com&hl=in">
                    <i class="ace-icon fa fa-play-circle-o red bigger-150"></i>
                </a>
            </span>
        </div>
    </div>
</div>