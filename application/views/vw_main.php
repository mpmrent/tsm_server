


<style type="text/css">
    .fade.in {
        background: rgba(0, 0, 0, 0.4);
    }
</style>

<div class="z_content">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px;">
        	<a href="https://play.google.com/store/apps/details?id=com.mpm_rent.mobile.TimesheetsApprove&hl=in" target="_blank">
        		<img style="width: 100%;" src="http://timesheet.mpm-rent.net/tsm_live/assets/images/download.png">
        	</a>
        </div>
    </div>
</div>	

<div class="x_content">
    <div class="row">
        <div class="col-md-12">
			<div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-hidden="true">
			    <div class="modal-dialog modal-lg">
			        <div class="modal-content">
			            <div class="modal-header bg-primary">
			                <h4 class="modal-title" id="modalTitlePin"><i class="ace-icon fa fa-info-circle"></i> Perhatian</h4>
			            </div>
			            <div class="modal-body" id="modal-pin">
			            	<p class="MsoNormal" style="text-align: justify;"><span style="font-size: large;"><span lang="EN-US"><span style="color: red;">Pelanggan dan pengguna jasa (user) wajib melakukan pemeriksaan dan memberikan persetujuan terhadap <i>Timesheet Mobile</i></span>. Dalam hal user tidak atau belum melakukan persetujuan terhadap <i>Timesheet Mobile</i> yang diajukan oleh Pengemudi selambatnya <span style="color: red;">5 (lima) hari</span> kalender setelah tanggal aktual pelaksanaan pekerjaan yang dilakukan Pengemudi, maka <i>Timesheet Mobile</i> <span style="color: red;">secara otomatis dianggap sudah diperiksa dan disetujui oleh User dan Pelanggan</span>, oleh karena itu Perusahaan berhak untuk menyampaikan tagihan berdasarkan <i>Timesheet Mobile</i> dan <span style="color: red;">Pelanggan wajib untuk melakukan pembayaran sesuai jumlah yang tercantum dalam tagihan dan dalam batas waktu yang telah ditentukan.</span></span></span></p>

							<hr>

							<hr>
							<label class="middle">
								<input class="ace" id="check-agree" type="checkbox" id="id-disable-check" />
								<span class="lbl"> I have read and agree to the agreement presented above.</span>
							</label>
			            </div>
			            <div class="modal-footer">
			                <button type="button" disabled id="btn-agree" class="btn btn-success"><i class="ace-icon fa fa-check"></i> I Agree
			                </button>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready( function() {

		<?php if (($this->session->flashdata('message')['status'] != null)) { ?>
		    showPopUp(
		            'Information',
		            '<?php echo $this->session->flashdata('message')['comment'] ?>',
		            '<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">OK</button>'
		        );
		<?php } ?>


		<?php if ($agree == 0) { ?>
		$('#alert').modal({backdrop: 'static', keyboard: false});
		<?php } ?>
	});

	$('#check-agree').change(function() {
        if($(this).is(":checked")) {
        	$('#btn-agree').removeAttr('disabled');
		} else {
        	$('#btn-agree').attr('disabled', 'disabled');
		}
	});

	$('#btn-agree').click(function() {
		var browser = 'Unknown';
		var isIE = null;
		if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
			browser = 'Opera';
	    } else if (typeof InstallTrigger !== 'undefined') {
			browser = 'Opera';
	    } else if (/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification)) {
			browser = 'Safari';
	    } else if (/*@cc_on!@*/false || !!document.documentMode) {
			browser = 'IE';
			isIE = /*@cc_on!@*/false || !!document.documentMode;
	    } else if (!isIE && !!window.StyleMedia) {
			browser = 'IE Edge';
	    } else if (!!window.chrome && !!window.chrome.webstore) {
			browser = 'Chrome';
	    }

        $.ajax({
            type: 'post',
            data: {
            	'browser' : browser,
            	'device' : navigator.userAgent
            },
            url: '<?php echo base_url() ?>index.php/main/agree/',
            success: function (data) {
            	if (data) {
					$('#alert').modal('hide');
				}
            }
        });
	});
</script>