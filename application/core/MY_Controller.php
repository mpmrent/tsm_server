<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('BASE_URI', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('THEMES_DIR', 'themes');
define('ADMIN_THEMES_DIR','admin');

class MY_Controller extends CI_Controller {

	protected $data = array();
	protected $assets_path = 'assets/uploads/Post/';
	protected $assets_video_path = 'assets/uploads/Videos/';
	protected $assets_profile_path = 'assets/uploads/Avatar/';
	protected $current_user = array();
	protected $current_groups = array();
	protected $current_groups_ids = array();
	protected $base_assets_url = '';
    protected $parent_menu = '';
    protected $page_title = '';
    protected $theme = '';
    protected $columns = array();

    function __construct() {
        parent::__construct();

        // ======== Load Helper =========
        $this->load->helper(array('form','url','text','language','general', 'directory'));
        // ======== Load Library ========
        $this->load->library(array('session', 'table', 'form_validation','pagination','general', 'email'));
        $this->load->config('ci-blog');

        $this->data['current_user'] = $this->current_user;
        $this->data['current_groups'] = $this->current_groups;
        $this->data['current_groups_ids'] = $this->current_groups_ids;

        if(empty($this->data['page_title'])){
            $this->data['page_title'] = $this->config->item('ci_blog_sitename');
        }
        $this->data['theme'] = 'default';
    }

    function insertlog($id_act, $activity, $type_act){
            $ip_address = $this->input->ip_address();
            $data = array(
                'datetime'      => date("Y-m-d H:i:s"),
                'ip_address'    => $ip_address,
                'username'      => $this->session->userdata('username'),
                'activity'      => $activity,
                'id_act'        => $id_act,
                'type_act'      => $type_act,
            );
            if($this->db->insert('logs_user', $data)) {
                return TRUE;
            } else {
                return FALSE;
            }
    }
    
    public function getPicture($url = null) {
        $all = array();
        $dir = './assets/uploads/';
        $map = directory_map($dir, 1);
//        print_r();
        if (is_array($map) || is_object($map)) {
            foreach ($map as $album) {
                $file = array(str_replace('\\', '', $album), directory_map($dir.'/'.$album, 1));
                $urls = array();
                foreach ($file as $url) {
                    $imgs = array();
                    $ind = 0;
                    if (is_array($url) || is_object($url))
                    {
                        foreach ($url as $img) {
                            $imgs[$ind] = $dir.'/'.str_replace('\\', '', $album).'/'.$img;
                            $ind++;
                        }
                        $urls[str_replace('\\', '', $album)] = $imgs;
                    }
                }
                $all = array_merge($all, $urls);
            }
        }
        return $all;
//        echo json_encode($all);
    }

    protected function load_theme($content = null, $title = '', $layout = true) {
        $group = $this->session->userdata('user_type');
        $this->data['title'] = $title;
        $this->data['menu'] = $this->db->where('user_type', $group)->get("vw_menu");
        $this->data['pending'] = $this->get_timesheetss('pending');
        $this->data['overtime'] = $this->get_timesheetss('overtime');
        $this->data['header'] = $this->load->view('inc/header',$this->data, TRUE);
        $this->data['footer'] = $this->load->view('inc/footer',$this->data, TRUE);
        $this->data['sidemenu'] = $this->load->view('inc/sidemenu',$this->data, TRUE);
        $this->data['tools'] = $this->load->view('inc/tools',$this->data, TRUE);
        $this->data['content'] = $this->load->view($content, $this->data, TRUE);
        if ($layout) {
            $this->load->view('inc/layout', $this->data);
        }
    }

    protected function get_timesheetss($type = ''){
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $output = array();
            $aColumns = $this->db->list_fields('rpt_timesheet');
            $from = date('Y-m-d', strtotime('first day of this month'));
            $to =  date('Y-m-d', strtotime('first day of next month'));
            if (isset($_GET['daterange']) && $_GET['daterange'] != '') { 
                $date = explode('-', $_GET['daterange']); 
                $from = date("Y-m-d", strtotime($date[0]));
                $to = date("Y-m-d", strtotime( "+1 Day" , strtotime($date[1])));
            }
            if (isset($_GET['driver'])) 
                $this->db->like('username', $_GET['driver']);
                $this->db->where("start_time between '$from' and '$to'");
            if ($type == 'pending') {
                $this->db->where('approved_by', null);
                if ($this->session->userdata('user_type') == 'superpic') 
                    $this->db->where(" (`CustId` = '".$this->session->userdata('custid')."') ", null, true);
                else
                    $this->db->where('pic', $this->session->userdata('username'));
            }
            if ($type == 'overtime') {
                $this->db->where('approved_by', null);
                $this->db->where(' (TIME_TO_SEC(TIMEDIFF( IFNULL(end_time_edit, end_time), start_time)) / 3600) >= 12 ', null, true);
                if ($this->session->userdata('user_type') == 'superpic') 
                    $this->db->where(" (`CustId` = '".$this->session->userdata('custid')."') ", null, true);
                else
                    $this->db->where('pic', $this->session->userdata('username'));
            }
            if ($type == 'approved') {
                $this->db->where('approved_by IS NOT NULL', null, false);
                if ($this->session->userdata('user_type') == 'superpic') 
                    $this->db->where(" (`CustId` = '".$this->session->userdata('custid')."') ", null, true);
                else
                    $this->db->where('pic', $this->session->userdata('username'));
            }
            $data = $this->db->limit(100000)->get('rpt_timesheet');
            return $data;
    }
    
    protected function bootstrap_pagination($paging_config = array()){
        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config = array_merge($paging_config,$config);
        $this->pagination->initialize($config);
        return $this->pagination->create_links(); 
    }

    protected function allow_group_access($menu_id = NULL){
        $groups = array('admin');
        $this->db->select('b.name');
        $this->db->join('groups b', 'a.group_id = b.id ', 'left');
        $this->db->join('menus c', 'a.menu_id = c.id', 'left');
        $this->db->where('menu_id', $menu_id);
        $allow = $this->db->get('group_privileges a');
        foreach ($allow->result() as $value) {
            $groups[] = $value->name;
        }

        $allow_access = false;

        $match_group_allowed = array_intersect($this->current_groups(), $groups);

        $allow_access = !empty($match_group_allowed);

        if($allow_access == false){
            $this->session->set_flashdata('message', message_box('You are not allowed to access this page!','danger'));
            redirect('signin','refresh');
        }
    }

    protected function current_groups(){
        $current_groups = array();
        if(!empty($this->current_user['groups'])){
                $current_groups = explode(',', $this->current_user['groups']);
        }
        return $current_groups;
    }

    protected function generate_acl_db(){
        $controllers = array();
        $this->load->helper('file');

        // Scan files in the /application/controllers directory
        // Set the second param to TRUE or remove it if you 
        // don't have controllers in sub directories
        $files = get_dir_file_info(APPPATH.'controllers');

        // Loop through file names removing .php extension
        foreach ($files as $file)
        {

            $controller = array(
                    'name' => $file['name'],
                    'path' => $file['server_path'],
                    'parent_id' => 0,
            );

            if($file['name'] != 'admin'){

                    $methods = get_class_methods(str_replace('.php', '', $file['name']));

            }

            if($file['name'] == 'admin'){
                    $admin_files = get_dir_file_info(APPPATH.'controllers/admin');
                    print_data($admin_files);exit;
            }
        }
    }

    public function send_mail($to, $subject, $mail_type, $message = null, $attach = NULL, $from = 'admin.it') {
        //$this->load->library('email');
        $this->load->library('PHPMailerAutoload');
        $mail = new PHPMailer();

        // set smtp
        $mail->isSMTP();
        $mail->Host = 'ssl://ems.indo.net.id';
        $mail->Port = '465';
        $mail->SMTPAuth = true;
        $mail->Username = 'admin.it@mpm-rent.com';
        $mail->Password = 'mpmrx123';
//              $mail->Username = 'dimas.lazuardy@mpm-rent.com';
//              $mail->Password = 'mpmrent1!';
        $mail->WordWrap = 50;  
//            $mail->SMTPDebug = 2;
//            $mail->Debugoutput = 'html';

        // set email content
        $mail->setFrom($from . '@mpm-rent.com');

        foreach ($to as $t) {
            $mail->addAddress($t);
        }

        $mail->Subject = $subject;
        $messages = $this->load->view($mail_type, $message, TRUE);
        $mail->Body = $messages;
        $mail->IsHTML(true);   
        if ($attach != NULL)
            $mail->AddAttachment($attach);
        $mail->send();
    }

    function clean($string) {
        $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '_', $string); // Removes special chars.
    }
        
    function getAllUsers(){
        $this->db->where('username <>', $this->session->userdata('username'), true);
        $this->db->where('active', 1);
        $this->db->order_by('last_act', 'DESC');
        $users = $this->db->get('vw_users');
        return $users;
    }
    
    public function decrypt($text) {
        echo simple_decrypt($text);
    }

    public function encrypt($text) {
        echo simple_encrypt($text);
    }
    
    public function get_userid($username) {
        $user = $this->m_user->get_user_id($username);
        foreach ($user->result() as $detail) {
            $id = $detail->id;
        }
        return $id;
    } 
    
    public function set_menu_active($id_menu) {
        $row = $this->db->where('id', $id_menu)->get('menu')->row_array();
        $parent_row = $this->db->where('id', $row['parent'])->get('menu')->row_array();
        $data = array(
            "id" => $id_menu,
            "parent" => $row['parent'],
            "title" => $row['menu'],
            "parent_title" => $parent_row['menu'],
            "parent_icon" => $parent_row['icon'],
        );
        $this->session->set_userdata("menu_active", $data);
    }
}