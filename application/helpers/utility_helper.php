<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_error_message'))
{
	function get_error_message($error_code)
	{
		$ERROR_MESSAGES = array(
				ERROR_INVALID_PARAMETERS => 'Invalid parameters',
				ERROR_ACCESS_DENIED => 'Access denied',
				ERROR_INVALID_USERNAME_OR_PASSWORD => 'Invalid username or password',
				ERROR_ALREADY_LOGGED_IN => 'Already logged in',
				ERROR_INVALID_PIN => 'Invalid PIN',
				ERROR_NOT_ENOUGH_PRIVILEGE => 'Sorry, you don\'t have enough privilege!',
				ERROR_OVERLAPPED_ENTRY => 'Sorry, your start/end time overlapped with other timesheet!'
		);

		return $ERROR_MESSAGES[$error_code];
	}
}

if ( ! function_exists('get_error_response'))
{
	function get_error_response($error_code)
	{
		$output = array('error' => array('code' => $error_code, 'message' => get_error_message($error_code)));
		return json_encode($output);
	}
}

if ( ! function_exists('format_response'))
{
	function format_response($response)
	{
		$output = array('data' => $response);
		return json_encode($output, JSON_NUMERIC_CHECK);
	}
}

if ( ! function_exists('require_session'))
{
	function require_session()
    {
    	$CI = &get_instance();
    	
    	if (!$CI->session->is_logged_in())
		{
			echo get_error_response(ERROR_ACCESS_DENIED);
			die();
		}
    }
}

if ( ! function_exists('get_auth_challenge'))
{
	function get_auth_challenge()
    {
    	$n = rand(10e16, 10e20);
		$challenge = base_convert($n, 10, 36);
		return $challenge;
    }
}

if ( ! function_exists('simple_encrypt'))
{
	function simple_encrypt($s)
	{
		$result = '';
		$base64 = base64_encode($s);
		$len = strlen($base64);
		for ($n = 0; $n < $len; $n+=2)
		{
			$result .= $base64[$n + 1];
			$result .= $base64[$n];
		}
		
		$n = rand(10e16, 10e20);
		$salt = substr(base_convert($n, 10, 36)."".time(), 0, 7);
		$salt = "abcdefg";
		
		return base64_encode($salt.$result);
	}
}

if ( ! function_exists('simple_decrypt'))
{
	function simple_decrypt($s)
	{
		$result = '';
		$base64 = base64_decode($s);
		$base64 = substr($base64, 7);
		
		$len = strlen($base64);
		for ($n = 0; $n < $len; $n+=2)
		{
			$result .= $base64[$n + 1];
			$result .= $base64[$n];
		}
		return base64_decode($result);
	}
}

if ( ! function_exists('hash_password'))
{
	function hash_password($password, $challenge='', $salt='')
    {
    	if ($challenge == null || strlen($challenge) == 0)
    	{
    		return simple_encrypt($password);
    	}
    	
    	$decrypt = simple_decrypt($password);
    	return md5(md5($challenge).md5($decrypt));
    }
}
	
if ( ! function_exists('hash_string'))
{
	function hash_string($string)
	{
		$hashcode = 1;
		
		$len = strlen($string);
		for ($n = 0; $n < $len; $n++)
		{
			$hashcode = 31 * $hashcode + ord($string[$n]);
		}
		
		return abs($hashcode) % 100;
	}
}

/* End of file utility_helper.php */
/* Location: ./system/helpers/utility_helper.php */