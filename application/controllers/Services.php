<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Description of Timesheet_services
 *
 * @author wawan.setiawan
 */
class Services extends MY_Controller {

    protected $table_get = "timesheets";
    protected $pk = "id";

    public function __construct() {
        parent::__construct();
        $this->load->helper('utility');
        $this->load->model(array('m_timesheet'));
        $this->load->model(array('m_user'));
        // $this->webportal = $this->load->database('webportal', TRUE);
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, x-xsrf-token");
    }

    public function tes() {
        $get = file_get_contents('https://maps.googleapis.com/maps/api/timezone/
            json?location=-8.6583369,115.2125341&timestamp=1458000000&key=AIzaSyCB_ovBat9n54w1qD3yvJrN96PGRux5bKY');
        $json_get = json_decode($get);
        echo json_encode($json_get->timeZoneId);
    }

    public function agree(){
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $username = $json_get->username;
        $device = json_encode($json_get->device);
        $data = array(
            'username' => $username,
            'ip_address' => $this->input->ip_address(),
            'device' => $device,
            'browser' => 'Timesheet Mobile'
        );
        if ($this->db->insert('users_agree', $data)) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function cekagree(){
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $uname = $json_get->uname;
        $data = $this->db->where('username', $uname)->get('users_agree');
        if ($data->num_rows() == 0) {
            echo false;
        } else {
            echo true;
        }
    }

    public function encrypt_pass() {
        $pass = array();

            foreach($pass as $p) {
                echo simple_encrypt($p).'<br>';
            }
    }

    public function tes_web_service() {
        $this->load->view('tes');
    }


    // public function changeAllpass() {
    //     $data = $this->db->get('users');
    //     foreach($data->result() as $row) {
    //         $upd = array(
    //                 'password' => simple_encrypt('0'.simple_decrypt(($row->password)))
    //             );
    //         if (strlen(simple_decrypt(($row->password)))<6) {
    //             $this->db->where('id', $row->id)->update('users', $upd);
    //             echo json_encode($upd).'<br>';
    //         }
    //     }
    // }

    // public function tes() {
    //     $query = null;
    //     try {
    //         $this->db->where('NOW() > DATE_ADD(`created_time`,INTERVAL -1 WEEK)', null, true);
    //         $this->db->where('approved_by IS NULL', null, true);
    //         $this->db->where('end_time IS not NULL', null, true);
    //         $auto_approve = $this->db->get('timesheets');
    //         $arr_data = array();
    //         foreach($auto_approve->result() as $row) {
    //             $data = array(
    //                 'id' => $row->id,
    //                 'approved_by' => "system",
    //                 'approved_time' => date('Y-m-d H:i:s')
    //             );
    //             array_push($arr_data, $data);
    //         }
            
    //         if ($auto_approve->num_rows()>0)    
    //             $query = $this->m_timesheet->update_batch('timesheets', $arr_data, 'id');
    //     } catch(Exception $ex) {
    //         $query = false;
    //     }
    // }

    public function doLogin() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        if ($json_get == null) {
            $pwd = $_POST['txtPwd'];
            $una = $_POST['txtUname'];
        } else {
            $pwd = $json_get->passwd;
            $una = $json_get->uname;
        }
        
        $query = $this->m_user->service_validate($pwd, $una);
        $rows_array = $query->row_array();
        $row = $query->num_rows();

        //get user id
        $uid = $this->_get_user_id($una);
        if ($row != 0) {
            $this->db->where('driver_gs', $una);
            $this->db->where('end_time', null);
            $check_gs =  $this->db->get('timesheets');
            if ($check_gs->num_rows() == 0) {
                $session = array (
    //                'session_id' => md5(rand()),
                    'session_id' => md5(uniqid($uid, true)),
                    'ip_address' => $this->input->ip_address(),
                    'user_agent' => $this->input->user_agent(),
                    'last_activity' => date('ymdHi'),
                    'user_data' => '{username:'.$una.',id:'.$uid.', logged in}'
                );
                
                $session2 = array (
                    'user_id' => $uid,
                    'is_logged_in' => true,
    //                'session_id' => md5(rand()),
                    'session_id' => $session['session_id'],
                    'ip_address' => $this->input->ip_address(),
                    'user_agent' => $this->input->user_agent(),
                    'last_activity' => date('ymdHi'),
                    'user_data' => '{username:'.$una.',id:'.$uid.', logged in}'
                );

                $alias = '';
                if ($rows_array['alias'] != '') {
                    $alias = ' ('.$rows_array['alias'].')';
                }
                
                $this->m_timesheet->add('sessions', $session);
                $json['uid'] = $uid;
                $json['status'] = 1;
                $json['username'] = $una;
                $json['name'] = $rows_array['firstname'] . ' ' . $rows_array['lastname'] . $alias;
                $json['token'] = md5(rand());
                $this->session->set_userdata($session2);

                // IF ((DATE('D')=='Sat') || (DATE('D')=='Sun') || (DATE('D')=='Mon')){
                    $this->db->where("NOW() > IF(DAYNAME(start_time)='Thursday',DATE_ADD(`start_time`,INTERVAL 5 DAY),IF(DAYNAME(start_time)='Friday',DATE_ADD(`start_time`,INTERVAL 5 DAY),DATE_ADD(`start_time`,INTERVAL 5 DAY)))", null, true);
                    $this->db->where('approved_by IS NULL', null, true);
                    $this->db->where('end_time IS not NULL', null, true);
                    $auto_approve = $this->db->get('timesheets');
                    $arr_data = array();
                    foreach($auto_approve->result() as $row) {
                        $data = array(
                            'id' => $row->id,
                            'approved_by' => "system",
                            'approved_time' => date('Y-m-d H:i:s')
                        );
                        array_push($arr_data, $data);
                    }
                    
                    if ($auto_approve->num_rows()>0)    
                        $query = $this->m_timesheet->update_batch('timesheets', $arr_data, 'id');
                // }


            } else {
                $json['uid'] = null;
                $json['status'] = 3;
                $json['token'] = null;
            }
        } else {
            $json['uid'] = null;
            $json['status'] = 0;
            $json['token'] = null;
        }
        
        if ($json_get == null) {
            redirect('home/main');
        } else {
            echo json_encode($json);
        }
    }

    public function doCheckTimesheet($uid = '') {
        $time = $this->m_timesheet->get_all_limit($uid, 1000000);
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($time->result()));
    }

    public function doUploadImage() {

    }

    public function lists($uid = '') {
        $time = $this->m_timesheet->get_all_limit($uid, 100);
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($time->result()));
    }

    public function list_details($id = '') {
        $time = $this->m_timesheet->get_id_timesheet($id);
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($time->result()));
    }

    public function approved_lists($uid = '') {
        $approved = $this->m_timesheet->get_all_approved_limit($uid, 100);
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($approved->result()));
    }
    
    public function lists_period () {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);
        
        $json_from = $json_get->from;
        $json_to = $json_get->to;

        $from = date('Y-m-d', strtotime($json_from)).' 00:00:00';
        $to = date('Y-m-d', strtotime($json_to)).' 23:59:59';
        $user_id = $json_get->user_id;
        
        if ($json_from==null && $json_to != null) {
            //return date to to_date
            $list_filtered = $this->m_timesheet->get_all_to($user_id, $to);
        } else if ($json_from!=null && $json_to==null){
            //return date from from_date
            $list_filtered = $this->m_timesheet->get_all_from($user_id, $from);
        } else if ($json_from!=null && $json_to!=null) {
            //return filtered date
            $list_filtered = $this->m_timesheet->get_all_period($user_id,$from ,$to);            
        } else {
            //return default date
            $list_filtered = $this->m_timesheet->get_all_limit($user_id, 100);
        }
        
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($list_filtered->result()));
    }
    
    public function approved_lists_period () {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);
        
        $json_from = $json_get->from;
        $json_to = $json_get->to;

        $from = date('Y-m-d', strtotime($json_from)).' 00:00:00';
        $to = date('Y-m-d', strtotime($json_to)).' 23:59:59';
        $user_id = $json_get->user_id;
        
        if ($json_from==null && $json_to != null) {
            //return date to to_date
            $approved_filtered = $this->m_timesheet->get_all_approved_to($user_id, $to);
        } else if ($json_from!=null && $json_to==null){
            //return date from from_date
            $approved_filtered = $this->m_timesheet->get_all_approved_from($user_id, $from);
        } else if ($json_from!=null && $json_to!=null) {
            //return filtered date
            $approved_filtered = $this->m_timesheet->get_all_approved_period($user_id,$from ,$to);            
        } else {
            //return default date
            $approved_filtered = $this->m_timesheet->get_all_approved_limit($user_id, 100);
        }
        
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($approved_filtered->result()));
    }

    // private function _is_overlapped($user_id, $start_time, $end_time) {
    //     $sql = "SELECT * FROM timesheets "
    //             . "WHERE ((" . $this->db->escape($start_time) . " >= start_time "
    //             . "AND " . $this->db->escape($start_time) . " <= end_time) "
    //             . "OR (" . $this->db->escape($end_time) . " >= start_time "
    //             . "AND " . $this->db->escape($end_time) . " <= end_time)) "
    //             . "AND user_id=" . $this->db->escape($user_id) . " "
    //             . "AND deleted=0 LIMIT 1";

    //     $query = $this->db->query($sql);
    //     return $query->num_rows() > 0;
    // }

    private function _is_overlapped($user_id, $start_time) {
        $sql = "SELECT * FROM timesheets "
                // . "WHERE (date(start_time) = " . $this->db->escape($start_time) . " ) "
                . "WHERE (date(end_time) is null ) "
                . "AND user_id=" . $this->db->escape($user_id) . " "
                . "AND deleted=0 LIMIT 1;";

        $query = $this->db->query($sql);
        $row_cnt = $query->num_rows();
        return $row_cnt > 0;
    }

    public function get_location () {
    	$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
	    echo json_encode($query);
    }

    public function addLocation() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);


        $last = $this->db->where('user_id' , $json_get->created_by)
            ->limit(1)
            ->order_by('id', 'DESC')
            ->get('timesheets')->row_array();

        $data = array(
                'location' => $json_get->location,
                'location_name' => $json_get->location_name
            );
        $this->db->where('id', $last['id']);
        $this->db->update('timesheets', $data);
        echo $this->db->affected_rows();
    }

    public function get_driver() {
        $output = array();
        $aColumns = $this->db->list_fields('users');
        $this->db->select('username, firstname, lastname');
        $this->db->where('user_type', 'driver');
        $this->db->order_by('username');
        $data = $this->data['table'] = $this->db->get('users');
        // foreach ($data->result_array() as $aRow) {
        //     $row = array();
        //     foreach ($aColumns as $col) {
        //         $row[] = $aRow[$col];
        //     }
        //     $output[] = $row;
        // }
        echo json_encode($data->result_array());
    }

    public function get_province() {
        $output = array();
        $data = $this->data['table'] = $this->db->get('province_timezone');
        echo json_encode($data->result_array());
    }

    public function check_update($version) {
        $this->db->where('version_app > "'.$version.'"' , null, true);
        $this->db->order_by('id', 'DESC');
        $result = $this->db->limit(1)->get('app_versions');
        $row = $result->row_array();
        $data = array(
                'row' => $result->num_rows(),
                'version_app' => $row['version_app'],
                'update_desc' => $row['update_desc'],
                'datetime' => $row['datetime'],
            );
        echo json_encode($data);
    }

    public function add() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $start_date = date('Y-m-d', strtotime($json_get->start_date));
        $start_time = date('H:i', strtotime($json_get->start_time));
        $time = $start_date . " " . $start_time;
        // $end_date = date('Y-m-d', strtotime($json_get->end_date));
        // $end_time = date('H:i', strtotime($json_get->end_time));
        $ot = $json_get->ot;
        $province = '';
        if (isset($json_get->province)) {
            $province = $json_get->province;
            $data_prov = $this->db->where('id', $province)->get('province_timezone')->row_array();
            $time = date('Y-m-d H:i', strtotime($time)+(60*60*($data_prov['timezone']-7)));
        }
        $town = '';
        if (isset($json_get->town))
                $town = $json_get->town;
        $town2 = '';
        if (isset($json_get->town2))
            if (strpos($json_get->town2, 'undefined') !== true)
                $town2 = $json_get->town2;
        $username = $this->db->select('username,pic,custid,custname')->where('id', $json_get->created_by)->get('users')->row_array();
        $driver_gs = '';
        $picId = $username['pic'];
        $custId = $username['custid'];
        $custName = $username['custname'];
        if($json_get->driver_gs != '' && $username['username'] != strtoupper($json_get->driver_gs)) {
        echo "console.log( 'Debug Objects: masuk condition' );";
            $driver_gs = $json_get->driver_gs;
            $userGS = $this->db->select('username,pic,custid,custname')->where('username', $json_get->driver_gs)->get('users')->row_array();
            $picId = $userGS['pic'];
            $custId = $userGS['custid'];
            $custName = $userGS['custname'];
        }
        $used_for = $json_get->used_for;
        $overtime = '0';
        if (isset($json_get->overtime))
            $overtime = $json_get->overtime;
        $created_by = $json_get->created_by;
        $km = '';
        if (isset($json_get->km))
            $km = $json_get->km;
        $nopol = '';
        if (isset($json_get->nopol))
            $nopol = $json_get->nopol;
        $location = '';
        if (isset($json_get->location))
        	$location = $json_get->location;
        $locationName = '';
        if (isset($json_get->location_name))
        	$locationName = $json_get->location_name;
        $uuid = $json_get->uuid;
        $model = $json_get->model;
        $manufacturer = $json_get->manufacturer;
        $version = $json_get->version;
        $platform = $json_get->platform;
        $version_app = '';
        if (isset($json_get->version_app))
            $version_app = $json_get->version_app;
        // $picture = $json_get->picture;

        // $loc = explode(',', $location);
        // $get = file_get_contents('https://maps.googleapis.com/maps/api/timezone/json?location='.$loc[0].','.$loc[1].'&timestamp=1458000000&key=AIzaSyCB_ovBat9n54w1qD3yvJrN96PGRux5bKY');
        // $json_get = json_decode($get);
        // $origin_dtz = new DateTimeZone($json_get->timeZoneId);
        // $origin_dt = new DateTime($start_date . " " . $start_time, $origin_dtz);
		// echo "console.log( 'Debug Objects: " . $picId . "' );";
        $data = array(
            'user_id' => $created_by,
            'start_time' => $time,
            // 'end_time' => $end_date . " " . $end_time,
            'customer_id' => $custId,
            'custname' => $custName,
            'pic' => $picId,
            'overtime' => $overtime,
            'out_of_town' => $ot,
            'province' => $province,
            'town' => $town,
            'desc' => $town2,
            'location' => $location,
            'location_name' => $locationName,
            'driver_gs' => $driver_gs,
            'km_awal' => $km,
            'nopol' => $nopol,
            'type_of_use' => $used_for,
            // 'picture' => $picture,
            'created_time' => date('Y-m-d H:i:s'),
            'deleted' => 0
        );

        $log = $data;
		$is_still_checkin = $this->_is_overlapped($created_by, $start_date);
		if ($is_still_checkin > 0) {
            $json['status'] = 2;
        } else {
            if (strtotime($start_date . " " . $start_time) - strftime(now())<21600 && strtotime($start_date . " " . $start_time) - strftime(now())>-900) {
                $sql = $this->m_timesheet->add($this->table_get, $data);
                $insert_id = $this->db->insert_id();
                if ($sql) {
                    $json['status'] = 1;

                    // $where = array(
                    //     'user_id' => $created_by,
                    //     'start_time' => $start_date . " " . $start_time,
                    // );
                    // $this->db->where($where);
                    // $dataID = $this->db->get('timesheets')->row_array();

                    $data = array(
                            'id_timesheet' => $insert_id,
                            'uuid' => $uuid,
                            'model' => $model,
                            'manufacturer' => $manufacturer,
                            'version' => $version,
                            'platform' => $platform,
                            'version_app' => $version_app,
                        );
                    $this->db->insert('timesheets_device', $data);
                } else {
                    $json['status'] = 0;
                    $json['message'] = "Gagal menginput data.";
                    $error = array(
                            'username' => $username['username'],
                            'activity' => 'Check In',
                            'desc' => 'Insert gagal. | '. json_encode($log),
                        );
                    $this->db->insert('logs_errors', $error);
                }
            } else {
                $json['status'] = 0;
                $json['message'] = "Waktu check in tidak sesuai dengan waktu saat ini.";

                $error = array(
                        'username' => $username['username'],
                        'activity' => 'Check In',
                        'desc' => 'Waktu check in tidak sesuai. | ' . json_encode($log),
                    );
                $this->db->insert('logs_errors', $error);
            }
        }
        echo json_encode($json);
    }
    
    public function addOld() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $start_date = date('Y-m-d', strtotime($json_get->start_date));
        $start_time = intval($json_get->start_time / 3600) . ":" . ($json_get->start_time % 3600) / 60;
        $end_date = date('Y-m-d', strtotime($json_get->end_date));
        $end_time = intval($json_get->end_time / 3600) . ":" . ($json_get->end_time % 3600) / 60;
        $ot = $json_get->ot;
        $town = $json_get->town;
        $driver_gs = $json_get->driver_gs;
        $used_for = $json_get->used_for;
        $created_by = $json_get->created_by;

        $data = array(
            'user_id' => $created_by,
            'start_time' => $start_date . " " . $start_time,
            'end_time' => $end_date . " " . $end_time,
            'out_of_town' => $ot,
            'town' => $town,
            'driver_gs' => $driver_gs,
            'type_of_use' => $used_for,
            'created_time' => date('Y-m-d H:i:s'),
            'deleted' => 0
        );

        if ($this->_is_overlapped($created_by, $start_date . " " . $start_time, $end_date . " " . $end_time)) {
            $json['status'] = 2;
        } else {
            $sql = $this->m_timesheet->add($this->table_get, $data);
            if ($sql) {
                $json['status'] = 1;
            } else {
                $json['status'] = 0;
            }
        }
        echo json_encode($json);
    }

    public function update() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $id = $json_get->id;
        $start_date = date('Y-m-d', strtotime($json_get->start_date));
        $start_time = date('H:i', strtotime($json_get->start_time));
        $end_date = date('Y-m-d', strtotime($json_get->end_date));
        $end_time = date('H:i', strtotime($json_get->end_time));
        $ot = $json_get->ot;
        $town = $json_get->town;
        // $driver_gs = $json_get->driver_gs;
        $km = $json_get->km;

        $town2 = '';
        if (isset($json_get->town2))
            if (strpos($json_get->town2, 'undefined') !== true)
                $town2 = $json_get->town2;
        $desc = $town2 . '|' . $json_get->desc;
        $used_for = $json_get->used_for;
        $location_end = $json_get->location_end;
        $location_name_end = $json_get->location_name_end;


        $time = $end_date . " " . $end_time;
        // $cek = $this->m_timesheet->get_id_timesheet($id);
        // // $end_date = date('Y-m-d', strtotime($json_get->end_date));
        // // $end_time = date('H:i', strtotime($json_get->end_time));

        // $province = $json_get->province;
        // $data_prov = $this->db->where('id', $province)->get('province_timezone')->row_array();
        // $interval = ($data_prov['timezone']-7);

        $province = $this->db->select('province')->where('id', $json_get->id)->get('timesheets')->row_array();
        $data_prov = $this->db->where('id', $province['province'])->get('province_timezone')->row_array();
        $interval = ($data_prov['timezone']-7);
        // echo json_encode($data_prov);
        // if ($interval < 0)
        // 	$interval = 0;
        $time = date('Y-m-d H:i', strtotime($time)+(60*60*$interval));

        // if ($cek->row_array()['end_time'] == null) {
            $data = array(
                // 'start_time' => $start_date . " " . $start_time,
                'end_time' => $time,
                'out_of_town' => $ot,
                'town' => $town,
                // 'driver_gs' => $driver_gs,
                'km_akhir' => $km,
                'desc' => $desc,
                'type_of_use' => $used_for,
                'updated_time' => date('Y-m-d H:i:s'),
                'deleted' => 0,
                'location_end' => $location_end,
                'location_name_end' => $location_name_end
            );
            $log = $data;
        // } else {
        //     $data = array(
        //         // 'start_time_edit' => $start_date . " " . $start_time,
        //         'end_time_edit' => $time,
        //         // 'out_of_town' => $ot,
        //         // 'town' => $town,
        //         // 'driver_gs' => $driver_gs,
        //         'km_akhir' => $km,
        //         'desc' => $desc,
        //         'type_of_use' => $used_for,
        //         'updated_time' => date('Y-m-d H:i:s'),
        //         'deleted' => 0,
        //         'location_end' => $location_end,
        //         'location_name_end' => $location_name_end
        //     );
        // }
        $sql = $this->m_timesheet->update($this->table_get, $this->pk, $id, $data);
        if ($sql) {
            $json['status'] = 1;
            $json['id'] = $id;
            $json['start_date'] = $start_date;
            $json['start_time'] = $start_time;
            $json['end_date'] = $end_date;
            $json['end_time'] = $end_time;
            // $json['ot'] = $ot;
            // $json['town'] = $town;
            // $json['driver_gs'] = $driver_gs;
            $json['km'] = $km;
            $json['desc'] = $desc;
            $json['used_for'] = $used_for;
        } else {
            $json['status'] = 0;
            $error = array(
                    'username' => $id,
                    'activity' => 'Check Out',
                    'desc' => json_encode($log),
                );
            $this->db->insert('logs_errors', $error);
        }
        echo json_encode($json);
    }
    
    public function updateOld() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $id = $json_get->id;
        $start_date = date('Y-m-d', strtotime($json_get->start_date));
        $start_time = intval($json_get->start_time / 3600) . ":" . ($json_get->start_time % 3600) / 60;
        $end_date = date('Y-m-d', strtotime($json_get->end_date));
        $end_time = intval($json_get->end_time / 3600) . ":" . ($json_get->end_time % 3600) / 60;
        $ot = $json_get->ot;
        $town = $json_get->town;
        $driver_gs = $json_get->driver_gs;
        $used_for = $json_get->used_for;

        $data = array(
            'start_time' => $start_date . " " . $start_time,
            'end_time' => $end_date . " " . $end_time,
            'out_of_town' => $ot,
            'town' => $town,
            'driver_gs' => $driver_gs,
            'type_of_use' => $used_for,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted' => 0
        );
        $sql = $this->m_timesheet->update($this->table_get, $this->pk, $id, $data);
        if ($sql) {
            $json['status'] = 1;
            $json['id'] = $id;
            $json['start_date'] = $start_date;
            $json['start_time'] = $start_time;
            $json['end_date'] = $end_date;
            $json['end_time'] = $end_time;
            $json['ot'] = $ot;
            $json['town'] = $town;
            $json['driver_gs'] = $driver_gs;
            $json['used_for'] = $used_for;
        } else {
            $json['status'] = 0;
        }
        echo json_encode($json);
    }

    public function delete($id) {
        $sql = $this->m_timesheet->delete($this->table_get, $this->pk, $id);
        if ($sql) {
            $json['status'] = 1;
            $json['message'] = "id " . $id . " deleted";
        } else {
            $json['status'] = 0;
            $json['message'] = "failed to delete id " . $id;
        }
        echo json_encode($json);
    }

    public function delete_batch() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $array_id = $json_get->id;

        $sql = $this->m_timesheet->delete_batch($this->table_get, $this->pk, $array_id);
        if ($sql) {
            $json['status'] = 1;
            $json['message'] = "deleted " . json_encode($array_id);
        } else {
            $json['status'] = 0;
            $json['message'] = "failed to delete";
        }
        echo json_encode($json);
    }

    private function _get_user_id($uname) {
        $sql = $this->m_user->get_user_id($uname);
        $uid = '';
        foreach ($sql->result() as $id) {
            $uid = $id->id;
        }
        return $uid;
    }

    public function get_pic_approve() {
        $get = file_get_contents('php://input');
        $json = array();
        $json_get = json_decode($get);

        $uid = $json_get->uid;

        $getPIC = $this->db->where('id', $uid)->get('users')->row_array();
        $getPICName = $this->db->query("SELECT * FROM `users` 
            WHERE `username` = '".$getPIC['pic']."' 
            OR (`custid` = '".$getPIC['custid']."' 
            AND `user_type` = 'superpic') 
            LIMIT 100;");

        if ($getPICName->num_rows() > 0) {
            $PIC = $getPICName->row_array();
            $json['status'] = 1;
            $json['name'] = $PIC['firstname'] . ' ' . $PIC['lastname'];
        } else {
            $json['status'] = 0;
            $json['name'] = '';
        }
        echo json_encode($json);
    }

    public function approve() {
        $get = file_get_contents('php://input');
        $json = array();
        $json_get = json_decode($get);

        $uidPIC = $json_get->uid;
        $pwd = $json_get->pwd;
        $epwd = simple_encrypt($pwd);
        $array_id = $json_get->ids;
        $total_id = count($array_id);
        $survey = 0;
        if (isset($json_get->survey))
            $survey = $json_get->survey;

        $getPIC = $this->db->where('id', $uidPIC)->get('users')->row_array();
        $this->db->where('username', $getPIC['pic']);
        $sql = $this->m_user->get_id('users', 'password', $epwd);
        $row = $sql->num_rows();

        // check user pin
        if ($row != 0) {
            foreach ($sql->result() as $user) {
                $uid = $user->id;
                $username = $user->username;
                $CustId = $user->custid;
                $user_type = $user->user_type;
            }

            // check user type
            if ($getPIC['pic'] != $username) {
                // $json['status'] = 1;
                $arr_data = array();
                for ($x = 0; $x < $total_id; $x++) {
                    $data = array(
                        'id' => $array_id[$x],
                        'survey' => $survey,
                        'approved_by' => $username,
                        'approved_time' => date('Y-m-d H:i:s')
                    );
                    $check_gs_pic = $this->db->where('id', $array_id[$x])->get('timesheets')->row_array();
                    if (($check_gs_pic['driver_gs'] == 'standby') && (($user_type == 'admin') || ($user_type == 'administrator'))) {
                        $json['status'] = 3;
                        $json['message'] = '0001';
                    } else if (($check_gs_pic['driver_gs'] != '') && ($check_gs_pic['driver_gs'] != null)) {
                        $getPICgs = $this->db->where('username', $check_gs_pic['driver_gs'])->get('users')->row_array();
                        if ($getPICgs['pic'] != $username) {
                            $json['status'] = 6;
                            $json['message'] = '0002';
                            break;
                        }
                    } else {
                        $json['status'] = 1;
                        $json['message'] = '00031' . json_encode($sql->result());
                        break;
                    }
                }

                if (($json['status'] != 1) && ($json['status'] != 6)) {
                    // update all
                    $query = $this->m_timesheet->update_batch('timesheets', $arr_data, 'id');
                    if ($query) {
                        $json['status'] = 3;
                    } else {
                        $json['status'] = 4;
                    }
                }
            } else if (($user_type == "superpic")) {
                $json['status'] = 3;
                $arr_data = array();
                for ($x = 0; $x < $total_id; $x++) {
                    $data = array(
                        'id' => $array_id[$x],
                        'survey' => $survey,
                        'approved_by' => $username,
                        'approved_time' => date('Y-m-d H:i:s')
                    );
                    array_push($arr_data, $data);
                    $cek = $this->m_timesheet->get_id_timesheet($array_id[$x]);
                    if ($cek->row_array()['end_time'] == null) {
                        $json['status'] = 5;
                        break;
                    }
                    $check_gs_pic = $this->db->where('id', $array_id[$x])->get('timesheets')->row_array();
                    if (($check_gs_pic['driver_gs'] == 'standby') && (($user_type == 'admin') || ($user_type == 'administrator'))) {
                        $json['status'] = 3;
                        $json['message'] = '0005';
                    } else if (($check_gs_pic['driver_gs'] != '') && ($check_gs_pic['driver_gs'] != null)) {
                        $getPICgs = $this->db->where('username', $check_gs_pic['driver_gs'])->get('users')->row_array();
                        if ($getPICgs['pic'] != $username) {
                            $json['status'] = 6;
                            $json['message'] = '0004';
                            break;
                        }
                    }
                    $getPICgs = $this->db->where('id', $check_gs_pic['id'])->get('users')->row_array();
                    if ($getPICgs['custid'] != $CustId) {
                        $json['status'] = 7;
                        $json['message'] = '0004';
                        break;
                    }
                }

                if (($json['status'] != 5) && ($json['status'] != 6)) {
                    // update all
                    $query = $this->m_timesheet->update_batch('timesheets', $arr_data, 'id');
                    if ($query) {
                        $json['status'] = 3;
                    } else {
                        $json['status'] = 4;
                    }
                }
            } else if ($user_type == "driver") {
                $json['status'] = 1;
            } else {    
                $json['status'] = 3;
                $arr_data = array();
                for ($x = 0; $x < $total_id; $x++) {
                    $data = array(
                        'id' => $array_id[$x],
                        'survey' => $survey,
                        'approved_by' => $username,
                        'approved_time' => date('Y-m-d H:i:s')
                    );
                    array_push($arr_data, $data);
                    $cek = $this->m_timesheet->get_id_timesheet($array_id[$x]);
                    if ($cek->row_array()['end_time'] == null) {
                        $json['status'] = 5;
                        break;
                    }
                    $check_gs_pic = $this->db->where('id', $array_id[$x])->get('timesheets')->row_array();
                    if (($check_gs_pic['driver_gs'] == 'standby') && (($user_type == 'admin') || ($user_type == 'administrator'))) {
                        $json['status'] = 3;
                        $json['message'] = '0005';
                    } else if (($check_gs_pic['driver_gs'] != '') && ($check_gs_pic['driver_gs'] != null)) {
                        $getPICgs = $this->db->where('username', $check_gs_pic['driver_gs'])->get('users')->row_array();
                        if ($getPICgs['pic'] != $username) {
                            $json['status'] = 6;
                            $json['message'] = '0004';
                            break;
                        }
                    }
                }

                if (($json['status'] != 5) && ($json['status'] != 6)) {
                    // update all
                    $query = $this->m_timesheet->update_batch('timesheets', $arr_data, 'id');
                    if ($query) {
                        $json['status'] = 3;
                    } else {
                        $json['status'] = 4;
                    }
                }
            }
        } else {
            $json['status'] = 0;
        }
        echo json_encode($json);
    }

    public function check_pending_timesheet() {
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $username = $_POST['username'];
            $dataUser = $this->db->where('username', $username)->get('users')->row_array();
            $user_type = $dataUser['user_type'];
            $custid = $dataUser['custid'];
            $output = array();
            $this->db->where('approved_by', null);
            if ($user_type == 'superpic') 
                $this->db->where(" (`CustId` = '".$custid."') ", null, true);
            else
                $this->db->where('pic', $username);
            $table = $this->db->limit(10000)->get('rpt_timesheet_2');
            $output['total'] = $table->num_rows();
            echo json_encode($output);
    }

    public function getArticles($limit = 100) {
            // ini_set('max_execution_time', 0); 
            // ini_set('memory_limit', '-1');
            $this->webportal->select('*,  DATE_FORMAT(created, \'%d %M %Y @%H:%i\') created_article, `c`.`id` AS category_id, a.id as id_post');
            $this->webportal->join('`posts_categories` b', 'a.id = b.post_id', 'left');
            $this->webportal->join('(SELECT    *,
                    FLOOR(RAND() * 401) AS `position_rand`
                    FROM 
                    `webportal`.`categories`) c', 'b.category_id = c.id', 'left');
            $this->webportal->where('type', 'post');
            $this->webportal->where('a.status', '1');
//                Selain kompetisi
            $this->webportal->where('b.category_id <>', '22');
            if ($limit == 100) {
                $this->webportal->order_by('c.position_rand, a.id DESC');
            } else {
                $this->webportal->order_by('a.id DESC');
            }
            $output['posts'] = $this->webportal->limit($limit)->get('`posts` a')->result_array();
            $output['categories'] = $this->webportal
                ->where('position is not null ', null, true)
                ->get('categories')->result_array();
            echo json_encode($output);
    }

    public function getArticleDetail($id = '') {
        $url_img =  'http://timesheet.mpm-rent.net/tsm_dev/';
        $this->webportal->select('*,  DATE_FORMAT(created, \'%d %M %Y @%H:%i\') created_article');
        $post = $this->webportal->where('id' , $id)->get('posts');
        $data = $post->row_array();
        $data['featured_image'] = $url_img . $data['featured_image'];
        $data['body'] = str_replace('<img', '<img style="width: 100%;"', $data['body']);
        $data['body'] = str_replace('/../webportal', $url_img, $data['body']);
        $data['body'] = str_replace('/webportal', $url_img, $data['body']);
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }

    public function getMessage($user, $bool = 0) {
        if ($bool == 1) {
            $data = $this->db
                ->select('id, title, text')
                ->where('received IS NULL', null, true)
                ->where('(for like "%' . $user .'%"', null, true)
                ->or_where('for = \'["%"]\')', null, true)
                ->get('vw_broadcast');
            $this->db->select('id, title, text, "file:///android_asset/www/assets/img/mpmr-icon.png" as smallIcon, "file:///android_asset/www/assets/img/mpmr-icon.png" as icon');
            $this->db->where('receive_name', $user);
            $this->db->where('id is not null', null, true);
            $this->db->where('received IS NULL', null, true);
            $data = $this->db->get('vw_broadcast_check');
            if ($data->num_rows() > 0) {
                foreach($data->result() as $row) {
                    $ins = array(
                            'id_message' => $row->id,
                            'username' => $user,
                            'received' => date('Y-m-d H:i:s')
                        );
                    $this->db->insert('broadcast_logs', $ins);
                }
            }
            return $this->output->set_content_type('application/json')
                            ->set_output(json_encode($data->result()));
        }
    }

    public function getUnreadMessage($user, $limit = 100) {
            // ini_set('max_execution_time', 0); 
            // ini_set('memory_limit', '-1');
            $this->db->select('id, title, text, create_by, DATE_FORMAT(datetime, \'%d %M %Y @%H:%i\') as datetime');
            $this->db->where('username', $user);
            $this->db->where('id is not null', null, true);
            $this->db->where('read is null', null, true);
            $result = $this->db->limit($limit)->get('vw_broadcast');
            // $result = $this->db->limit($limit)->get('vw_broadcast_user');
            $output['unread'] = $result->num_rows();
            // $output['messages'] = $result->result_array();
            echo json_encode($output);
    }

    public function test($user, $id = 1) {
        $upd = array(
                'read' => date('Y-m-d H:i:s')
            );
        $this->db->where('id_message' , $id)->where('username', $user)->update('broadcast_logs', $upd);
        $this->db->select('id, title, concat("", text, "") as text, create_by, DATE_FORMAT(datetime, \'%d %M %Y @%H:%i\') as datetime');
        $post = $this->db->where('id' , $id)->where('username', $user)->get('vw_broadcast');
        $data = $post->row_array();
        $data['text'] = "<p>" . $data['text'] . "</p>";
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }

    public function getAllMessage($user, $limit = 100) {
            // ini_set('max_execution_time', 0); 
            // ini_set('memory_limit', '-1');
            $this->db->select('id, title, text, create_by, DATE_FORMAT(datetime, \'%d %M %Y @%H:%i\') as datetime, read');
            $this->db->where('username', $user);
            $this->db->order_by('id', 'DESC');
            $output['messages'] = $this->db->limit($limit)->get('vw_broadcast')->result_array();
            echo json_encode($output);
    }

    public function getMessageDetail($user, $id = '') {
        $upd = array(
                'read' => date('Y-m-d H:i:s')
            );
        $this->db->where('id_message' , $id)->where('username', $user)->update('broadcast_logs', $upd);
        $this->db->select('id, title, concat("", text, "") as text, create_by, DATE_FORMAT(datetime, \'%d %M %Y @%H:%i\') as datetime');
        $post = $this->db->where('id' , $id)->where('username', $user)->get('vw_broadcast');
        $data = $post->row_array();
        $data['text'] = $data['text'] ;
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }

    // public function tes() {
    //     echo (strtotime('2017-11-07 07:27:05') - strftime(now())<900 && strtotime('2017-11-07 07:27:05') - strftime(now())>-900) ? 'true' : 'false';  
    //     echo '<hr>'. (strtotime('2017-11-07 07:27:05') - strftime(now())) . ' | ' . (strtotime('2017-11-07 10:27:05') - strftime(now()));
    // }

}
