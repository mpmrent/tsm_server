<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model(array('m_user'));
        if (!$this->session->userdata('is_logged_in')){
            $this->session->set_userdata('last_page', current_url());
            redirect ('');
        }
	}

	public function index(){
        $this->set_menu_active(1);
        $this->db->select('*, ""');
        $this->data['table'] = $this->db->limit(100)->get('users');
        $this->data['thead'] = array('Id', 'Username', 'Password', 'User Type', 'First Name', 'Last Name', 'Cust Id', 'Status', 'Action');
        $this->data['agree'] = $this->db->where('username', $this->session->userdata('username'))->get('users_agree')->num_rows();
        $this->load_theme('vw_main');
	}

    public function agree(){
        $data = array(
            'username' => $this->session->userdata('username'),
            'ip_address' => $this->input->ip_address(),
            'device' => $_POST['device'],
            'browser' => $_POST['browser']
        );
        if ($this->db->insert('users_agree', $data)) {
            echo 'true';
        } else {
            echo 'false';
        }
    }

    public function logout() {
        $this->session->unset_userdata();
        $this->session->sess_destroy();

        $this->output->set_header("HTTP/1.0 200 OK");
        $this->output->set_header("HTTP/1.1 200 OK");
        $this->output->set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $last_update) . ' GMT');
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");

        redirect('');
    }
}