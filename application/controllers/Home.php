<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct(){
		parent::__construct();
                $this->load->model(array('m_user'));
	}

    function check_login() {
            if ($this->session->userdata('is_logged_in')){
                $this->session->set_userdata('last_page', current_url());
                redirect ('main');
            }
    }

    public function getMac() {

        // $_IP_ADDRESS = $_SERVER['REMOTE_ADDR'];
        // $_PERINTAH = "nbtstat -A $_IP_ADDRESS";
        // $_HASIL = exec($_PERINTAH);
        // $_PECAH = strstr($_HASIL, $_IP_ADDRESS);
        // $_PECAH_STRING = explode($_IP_ADDRESS, str_replace(" ", "", $_PECAH));
        // // $_HASIL = substr($_PECAH_STRING[1], 0, 17);
        // echo $_HASIL . $_IP_ADDRESS;

        echo '<script language="javascript">
                function showMacAddress(){
                 
                    var obj = new ActiveXObject("WbemScripting.SWbemLocator");
                    var s = obj.ConnectServer(".");
                    var properties = s.ExecQuery("SELECT * FROM Win32_NetworkAdapterConfiguration");
                    var e = new Enumerator (properties);

                 
                    var output;
                    output=\'<table border="0" cellPadding="5px" cellSpacing="1px" bgColor="#CCCCCC">\';
                    output=output + \'<tr bgColor="#EAEAEA"><td>Caption</td><td>MACAddress</td></tr>\';
                    while(!e.atEnd())

                    {
                        e.moveNext();
                        var p = e.item ();
                        if(!p) continue;
                        output=output + \'<tr bgColor="#FFFFFF">\';
                        output=output + \'<td>\' + p.Caption; + \'</td>\';
                        output=output + \'<td>\' + p.MACAddress + \'</td>\';
                        output=output + \'</tr>\';
                    }

                    output=output + \'</table>\';
                    print_r(output);
                }
                showMacAddress();
                </script>';
    }

	public function index(){
            $this->check_login();
            // $this->Activity_user->insertlog('Login', 'Home', 'Employee');
            $this->load->view('vw_login');
	}

	public function login($username = null, $password = null){
            $this->check_login();
            if ($username == null) 
                $username = $this->input->post('txtUname');
            if ($password == null)
                $password = $this->input->post('txtPwd');
            $query = $this->m_user->validate($username, $password);
            $row = $query->num_rows();

            if ($row != 0) {
                $data = array(
                    'user_id' => $this->get_userid($username),
                    'is_logged_in' => TRUE
                );
                $data = array_merge($data, $query->row_array());
                $this->session->set_userdata($data);
                redirect('main');
            } else {
                $error_msg = '<div class="alert alert-danger">Username or password you typing are wrong. Plase try again!</div>';
                $this->session->set_flashdata('message', $error_msg);
                redirect('');
            }
	}
        
    public function login_android($username = null, $password = null){
            if ($username == null) 
                $username = $this->input->post('txtUname');
            if ($password == null)
                $password = $this->input->post('txtPwd');
            $query = $this->m_user->validate($username, $password);
            $row = $query->num_rows();

            if ($row != 0) {
                $data = array(
                    'user_id' => $this->get_userid($username),
                    'is_logged_in' => TRUE
                );
                $data = array_merge($data, $query->row_array());
                $this->set_session($data);
                echo "true";
            } else {
                echo "Username or password are incorect. Please try again!";
            }
    }
        
    public function tes(){
            echo "true";
    }

	public function register(){
            $this->check_login();
            $this->load->view('vw_login');
	}

	public function forgot_password(){
            $this->check_login();
            $this->load->view('vw_login');
	}

    public function set_session($user) {
            $session_data = array();
            foreach ($user as $key=>$value) {
                $session_data[$key] = $value;
            }
            unset($session_data['password']);
            $this->session->set_userdata($session_data);
            return TRUE;
    }
}