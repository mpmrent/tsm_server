<?php
date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Description of Timesheet_services
 *
 * @author wawan.setiawan
 */
class Services_approve extends MY_Controller {

    protected $table_get = "timesheets";
    protected $pk = "id";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->helper('utility');
        $this->load->model(array('m_timesheet'));
        $this->load->model(array('m_user'));
        // $this->webportal = $this->load->database('webportal', TRUE);
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Method: PUT, GET, POST, DELETE, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, x-xsrf-token");
    }

    public function doLogin() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        if ($json_get == null) {
            $pwd = $_POST['txtPwd'];
            $una = $_POST['txtUname'];
        } else {
            $pwd = $json_get->passwd;
            $una = $json_get->uname;
        }
        
        $query = $this->m_user->service_validate_approve($pwd, $una);
        $rows_array = $query->row_array();
        $row = $query->num_rows();

        //get user id
        $uid = $this->_get_user_id($una);
        if ($row != 0) {
            $this->db->where('driver_gs', $una);
            $this->db->where('end_time', null);
            $check_gs =  $this->db->get('timesheets');
            if ($check_gs->num_rows() == 0) {
                $session = array (
    //                'session_id' => md5(rand()),
                    'session_id' => md5(uniqid($uid, true)),
                    'ip_address' => $this->input->ip_address(),
                    'user_agent' => $this->input->user_agent(),
                    'last_activity' => date('ymdHi'),
                    'user_data' => '{username:'.$una.',id:'.$uid.', logged in}'
                );
                
                $session2 = array (
                    'user_id' => $uid,
                    'is_logged_in' => true,
    //                'session_id' => md5(rand()),
                    'session_id' => $session['session_id'],
                    'ip_address' => $this->input->ip_address(),
                    'user_agent' => $this->input->user_agent(),
                    'last_activity' => date('ymdHi'),
                    'user_data' => '{username:'.$una.',id:'.$uid.', logged in}'
                );
                
                $this->m_timesheet->add('sessions', $session);
                $json['uid'] = $uid;
                $json['status'] = 1;
                $json['username'] = $una;
                $json['name'] = $rows_array['firstname'] . ' ' . $rows_array['lastname'];
                $json['user_type'] = ucfirst($rows_array['user_type']);
                $json['token'] = md5(rand());
                $this->session->set_userdata($session2);
            } else {
                $json['uid'] = null;
                $json['status'] = 3;
                $json['token'] = null;
            }
        } else {
            $json['uid'] = null;
            $json['status'] = 0;
            $json['token'] = null;
        }
        
        if ($json_get == null) {
            redirect('home/main');
        } else {
            echo json_encode($json);
        }
    }

    public function get_timesheets_notif( $user = ''){
        $this->db->where('approved_by', null);
        if (($user == 'superpic') || ($user == 'administrator')) {
            $data_pic = $this->db->where('username', $user)->get('users')->row_array();
            $this->db->where(" (`CustId` = '".$data_pic['custid']."') ", null, true);
        } else {
            $this->db->where('pic', $user);
        }
        $num = $this->db->get('rpt_timesheet_2')->num_rows();
        if ($num > 0) {
            $data = array(
                'id' => '1',
                'title' => 'Informasi Timesheet',
                'text' => 'Ada ' . $num . ' Timesheets yang belum anda setujui.',
                'badge' => $num
            );
        }
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }

    public function get_timesheets($type = '', $user = ''){
            ini_set('max_execution_time', 0); 
            ini_set('memory_limit', '-1');
            $output = array();
            $data_pic = $this->db->where('username', str_replace('%20', ' ', $user))->get('users')->row_array();
            // echo json_encode($data_pic);
            if ($type == 'pending') {
                if (($data_pic['user_type'] == 'superpic') || ($data_pic['user_type'] == 'administrator')) {
                    $this->db->where(" (`CustId` = '".$data_pic['custid']."') ", null, true);
                } else {
                    $this->db->where('pic', str_replace('%20', ' ', $user));
                }
                $this->db->where('approved_by', null);
                $this->db->where('end_time is not null', null, true);
            } else 
            if ($type == 'approved') {
                if (($data_pic['user_type'] == 'superpic') || ($data_pic['user_type'] == 'administrator')) {
                    $this->db->where(" (`CustId` = '".$data_pic['custid']."') ", null, true);
                } else {
                    $this->db->where('pic', $user);
                }
                $this->db->where('approved_by IS NOT NULL', null, false);
                $this->db->order_by('id', 'desc');
            }
            $this->db->select("*, CONCAT_WS(''
                , CONCAT(LEFT(SUBSTRING_INDEX(TRIM(fullname), ' ', (-1*wordcount(fullname))), 1), '')
                , CONCAT(LEFT(SUBSTRING_INDEX(TRIM(fullname), ' ', (-1*(wordcount(fullname)-1))), 1), '')) AS shortname");
            $data = $this->data['table'] = $this->db->limit(30)->get('rpt_timesheet_2');
            echo json_encode($data->result_array());
    }

    public function get_driver($id = null){
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);
        $cek = $this->db->where('username', $json_get->username)->get('users')->row_array();

        $this->db->select("*
         , CONCAT_WS('', CONCAT(LEFT(SUBSTRING_INDEX(TRIM(CONCAT(firstname, ' ', lastname)), ' ', (-1*wordcount(CONCAT(firstname, ' ', lastname)))), 1), '')
         , CONCAT(LEFT(SUBSTRING_INDEX(TRIM(CONCAT(firstname, ' ', lastname)), ' ', (-1*(wordcount(CONCAT(firstname, ' ', lastname))-1))), 1), '')) AS shortname");
        if ($id == null) {
            if (($cek['user_type'] == 'superpic') || ($cek['user_type'] == 'administrator')) 
                $this->db->where('custid', $cek['custid']);
            if ($cek['user_type'] == 'pic')
                $this->db->where('pic', $cek['username']);
            $this->db->where('user_type', 'driver');
            $data = $this->db->limit(30)->get('users');
            echo json_encode($data->result_array());
        } else {
            $data = $this->db->where('id', $id)->limit(30)->get('users')->row_array();
            $output = array(
                'Kode Driver' => $data['username'],
                'Nama Driver' => $data['firstname'] .' '. $data['lastname'],
                'No Telepon' => $data['phone'],
                'Email' => $data['email'],
                'User' => $data['CustName'],
                'PIC' => $data['pic'],
                'status' => $data['status'],
            );
            echo json_encode($output);
        }
    }

    public function get_timesheets_by_id($id = ''){
            $aColumns = $this->db->list_fields('rpt_timesheet_2');
            $this->db->where('id', $id);
            $this->db->select("*, CONCAT_WS(''
                ,CONCAT(LEFT(SUBSTRING_INDEX(fullname, ' ', -2), 1), '')
                ,CONCAT(LEFT(SUBSTRING_INDEX(fullname, ' ', -1), 1), '')) as shortname");
            $data = $this->db->limit(1)->get('rpt_timesheet_2')->row_array();
            $output = array(
                    
                    'Kode Driver' => $data['username'],
                    'Nama Driver' => $data['fullname'],
                    'Jam Masuk' => $data['start_time'],
                    'Jam Pulang' => $data['end_time'],
                    'User' => $data['CustName'],
                    'KM Awal' => $data['km_awal'],
                    'KM Akhir' => $data['km_akhir'],
                    'Keluar Kota' => ($data['out_of_town'] == 'no' ? 'No' : ($data['out_of_town'] == 'pp' ? 'Round-Trip' : 'Stay Overnight')),
                    // 'Nama Kota' => ($data['town'] == '' ? '-' : $data['town']),
                    'Plat Nomor' => $data['nopol'],
                    // 'Driver GS' => ($data['driver_gs'] == '' ? '-' : $data['driver_gs']),
                    // 'Pemakaian' => $data['type_of_use'],
                    'PIC' => $data['pic'],
                );
            if ($data['approved_by'] != null) {
                $output['Approved By'] = $data['approved_by'];
                $output['Approved Time'] = $data['approved_time'];
            }
            if ($data['not_approve_by'] != null) {
                $output['Not Approve By'] = $data['not_approve_by'];
                $output['Not Approve Desc'] = $data['not_approve_desc'];
                $output['Not Approve Time'] = $data['not_approve_time'];
            }
            if ($data['edit_admin_by'] != null) {
                $output['Edit Admin'] = $data['edit_admin_by'];
                $output['Edit Admin Time'] = $data['edit_admin_time'];
            }
            $output['Mandatory'] = '';
            echo json_encode($output);
    }

    public function list_details($id = '') {
        $time = $this->m_timesheet->get_id_timesheet($id);
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($time->result()));
    }

    public function get_location () {
    	$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
	    echo json_encode($query);
    }

    public function addLocation() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);


        $last = $this->db->where('user_id' , $json_get->created_by)
            ->limit(1)
            ->order_by('id', 'DESC')
            ->get('timesheets')->row_array();

        $data = array(
                'location' => $json_get->location,
                'location_name' => $json_get->location_name
            );
        $this->db->where('id', $last['id']);
        $this->db->update('timesheets', $data);
        echo $this->db->affected_rows();
    }

    public function changePassword() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);
        $cek = $this->db->where('username', $json_get->username)->get('users')->row_array();
        if ($json_get->new_password != $json_get->konf_new_password) {
            $output = array(
                'status' => 2,
                'comment' => 'passconfgagal'
            );
        } else if (($json_get->old_password) != simple_decrypt($cek['password'])) {
            $output = array(
                'status' => 2,
                'comment' => 'passoldgagal'
            );
        } else {
            $data = array(
                    'password' => simple_encrypt($json_get->new_password)
                );
            $this->db->where('username', $json_get->username);
            if ($this->db->update('users', $data)) {
                $output = array(
                    'status' => 1,
                    'comment' => 'passberhasil'
                );
            } else {
                $output = array(
                    'status' => 2,
                    'comment' => 'passgagal'
                );
            }
        }
        echo json_encode($output);
    }

    public function get_province() {
        $output = array();
        $data = $this->data['table'] = $this->db->get('province_timezone');
        echo json_encode($data->result_array());
    }

    public function check_update($version) {
        $this->db->where('version_app > "'.$version.'"' , null, true);
        $this->db->order_by('id', 'DESC');
        $this->db->where('name_app', 'TimesheetApprove');
        $result = $this->db->limit(1)->get('app_versions');
        $row = $result->row_array();
        $data = array(
                'row' => $result->num_rows(),
                'version_app' => $row['version_app'],
                'update_desc' => $row['update_desc'],
                'datetime' => $row['datetime'],
            );
        echo json_encode($data);
    }

    private function _get_user_id($uname) {
        $sql = $this->m_user->get_user_id($uname);
        $uid = '';
        foreach ($sql->result() as $id) {
            $uid = $id->id;
        }
        return $uid;
    }

    public function get_pic_approve() {
        $get = file_get_contents('php://input');
        $json = array();
        $json_get = json_decode($get);

        $uid = $json_get->uid;

        $getPIC = $this->db->where('id', $uid)->get('users')->row_array();
        $getPICName = $this->db->where('username', $getPIC['pic'])->get('users');

        if ($getPICName->num_rows() > 0) {
            $PIC = $getPICName->row_array();
            $json['status'] = 1;
            $json['name'] = $PIC['firstname'] . ' ' . $PIC['lastname'];
        } else {
            $json['status'] = 0;
            $json['name'] = '';
        }
        echo json_encode($json);
    }

    public function approve_timesheet() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);
        $check_end = $this->db->where('id', $json_get->id)
                ->where('end_time', null)
                ->get('timesheets');
        if ($check_end->num_rows() == 0) {
            $data = array(
                'survey' => $json_get->survey,
                'approved_by' => $json_get->username,
                'approved_time' => date('Y-m-d H:i:s')
            );
            if ($json_get->survey) {

            }
            $this->db->where('id', $json_get->id)->update('timesheets', $data);
            $output = array(
                'status' => 1,
                'comment' => 'setujub'
            );
        } else {
            $output = array(
                'status' => 2,
                'comment' => 'setujug'
            );
        }
        echo json_encode($output);
    }

    public function not_approve_timesheet() {
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);
        $check_end = $this->db->where('id', $json_get->id)
                ->where('end_time', null)
                ->get('timesheets');
         $timesheet = $this->db->where('id', $json_get->id)->get('rpt_timesheet_2')->row_array();
        
        if ($check_end->num_rows() == 0) {
            $data = array(
                'not_approve_by' => $json_get->username,
                'not_approve_desc' => $json_get->desc,
                'not_approve_time' => date("Y-m-d H:i:s")
            );

            //if ($this->db->update('timesheets', $data)) {
             $data_tsm['data_timesheet'] = array(
                   'ID'        => $timesheet['id'], 
                    'Username'        => $timesheet['username'],
                    'Fullname'        => $timesheet['fullname'],
                    'Check In'        => $timesheet['start_time'],
                    'Check Out'        => $timesheet['end_time'], 
                    'Note'        => $timesheet['desc'],
                    'Cust Id'        => $timesheet['CustId'],
                    'Cust Name'        => $timesheet['CustName'],
                    'KM In'        => $timesheet['km_awal'],
                    'KM Out'        => $timesheet['km_akhir'],
                    'Out of Town'        => $timesheet['out_of_town'],
                    'Town'        => $timesheet['town'],
                    'No Police'        => $timesheet['nopol'],
                    'Driver GS'        => $timesheet['driver_gs'],
                    'Type of Use'        => $timesheet['type_of_use'],
                    'Approved By'        => $timesheet['approved_by'],
                    'Approved Time'        => $timesheet['approved_time'],
                    'Update Time'        => $timesheet['updated_time'],
                    'PIC'        => $timesheet['pic']
                );
                $data_tsm['description'] = 'Tidak di setujui oleh '. $this->session->userdata('firstname') . ' ('.$this->session->userdata('username').') dengan keterangan : ' . $json_get->desc . '.';
                $to = array('muhamad.arifin@mpm-rent.com', 'yudi.e@mpm-rent.com','contact@mpm-rent.com','rachman.purnama@mpm-rent.com');
                $this->send_mail($to, 'Timesheet Report - Not Approve '. $json_get->id, 'email/not_approved', $data_tsm);
            //}
            $this->db->where('id', $json_get->id)->update('timesheets', $data);
            $output = array(
                'status' => 1,
                'comment' => 'tdksetujub'
            );
        } else {
            $output = array(
                'status' => 2,
                'comment' => 'tdksetujug'
            );
        }
        echo json_encode($output);
    }

    public function getMessage($user) {
        // $data = $this->db
        //     ->select('id, title, text')
        //     ->where('received1 IS NULL', null, true)
        //     ->where('(for like "%' . $name .'%"', null, true)
        //     ->or_where('for = \'["%"]\')', null, true)
        //     ->get('vw_broadcast');
        $this->db->select('id, title, text, "file:///android_asset/www/assets/img/mpmr-icon.png" as smallIcon, "file:///android_asset/www/assets/img/mpmr-icon.png" as icon');
        $this->db->where('receive_name', $user);
        $this->db->where('id is not null', null, true);
        $this->db->where('received IS NULL', null, true);
        $data = $this->db->get('vw_broadcast_check');
        if ($data->num_rows() > 0) {
            foreach($data->result() as $row) {
                $ins = array(
                        'id_message' => $row->id,
                        'username' => $user,
                        'received' => date('Y-m-d H:i:s')
                    );
                $this->db->insert('broadcast_logs', $ins);
            }
        }
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data->result()));
    }

    public function getUnreadMessage($user, $limit = 100) {
            // ini_set('max_execution_time', 0); 
            // ini_set('memory_limit', '-1');
            $this->db->select('id, title, text, create_by, DATE_FORMAT(datetime, \'%d %M %Y @%H:%i\') as datetime');
            $this->db->where('username', $user);
            $this->db->where('id is not null', null, true);
            $this->db->where('read is null', null, true);
            $result = $this->db->limit($limit)->get('vw_broadcast');
            // $result = $this->db->limit($limit)->get('vw_broadcast_user');
            $output['unread'] = $result->num_rows();
            // $output['messages'] = $result->result_array();
            echo json_encode($output);
    }

    public function getAllMessage($user, $limit = 100) {
            // ini_set('max_execution_time', 0); 
            // ini_set('memory_limit', '-1');
            $this->db->select('id, title, text, create_by, DATE_FORMAT(datetime, \'%d %M %Y @%H:%i\') as datetime, read');
            $this->db->where('username', $user);
            $this->db->order_by('id', 'DESC');
            $output['messages'] = $this->db->limit($limit)->get('vw_broadcast')->result_array();
            echo json_encode($output);
    }

    public function getMessageDetail($user, $id = '') {
        $upd = array(
                'read' => date('Y-m-d H:i:s')
            );
        $this->db->where('id_message' , $id)->where('username', $user)->update('broadcast_logs', $upd);
        $this->db->select('id, title, concat("", text, "") as text, create_by, DATE_FORMAT(datetime, \'%d %M %Y @%H:%i\') as datetime');
        $post = $this->db->where('id' , $id)->where('username', $user)->get('vw_broadcast');
        $data = $post->row_array();
        $data['text'] = $data['text'] ;
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }

    public function getLanguage() {
        $post = $this->db->get('language');
        $data = $post->result_array();
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }

    public function agree(){
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $username = $json_get->username;
        $device = json_encode($json_get->device);
        $data = array(
            'username' => $username,
            'ip_address' => $this->input->ip_address(),
            'device' => $device,
            'browser' => 'Timesheet Approve'
        );
        if ($this->db->insert('users_agree', $data)) {
            echo 'true';
        } else {
            echo 'false';
        }
    }


    public function getNotifAgree(){
        $get = file_get_contents('php://input');
        $json_get = json_decode($get);

        $username = $json_get->username;
        $data['agree'] = $this->db->where('username', $username)->get('users_agree')->num_rows();
        return $this->output->set_content_type('application/json')
                        ->set_output(json_encode($data));
    }
}
