<?php

defined('BASEPATH') or  exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_timesheet
 *
 * @author wawan.setiawan
 */
class M_timesheet extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_all_periode($date1, $date2) {
        $this->db->from('timesheets');
        $this->db->where('created_time >', $date1);
        $this->db->where('created_time <', $date2);
        return $this->db->get();
    }

    // get unapprove timesheets
    public function get_all_limit($uid, $limit) {
        $this->db->where('(approved_by IS NULL or approved_by = "")', null, true);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->order_by('start_time', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('timesheets');
    }
    
    //get unapprove timesheet to date
    public function get_all_to($uid, $to) {
        $this->db->where('approved_by IS NULL', null, false);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->where('end_time <=', $to);
        $this->db->order_by('start_time', 'DESC');
        return $this->db->get('timesheets');       
    }
    
    //get unapprove timesheet from date
    public function get_all_from($uid, $from) {
        $this->db->where('approved_by IS NULL', null, false);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->where('start_time >=', $from);
        $this->db->order_by('start_time', 'DESC');
        return $this->db->get('timesheets');    
    }
    
    //get unapprove timesheet from date to date
    public function get_all_period($uid, $from, $to) {
        $this->db->where('approved_by IS NULL', null, false);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->where('start_time >=', $from);
        $this->db->where('end_time <=', $to);
        $this->db->order_by('start_time', 'DESC');
        return $this->db->get('timesheets');    
    }

    //get approved timesheets
    public function get_all_approved_limit($uid, $limit) {
        $this->db->where('approved_by IS NOT NULL', null, false);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->order_by('start_time', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('timesheets');        
    }
    
    //get approved timesheet to date
    public function get_all_approved_to($uid, $to) {
        $this->db->where('approved_by IS NOT NULL', null, false);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->where('end_time <=', $to);
        $this->db->order_by('start_time', 'DESC');
        return $this->db->get('timesheets');       
    }
    
    //get approved timesheet from date
    public function get_all_approved_from($uid, $from) {
        $this->db->where('approved_by IS NOT NULL', null, false);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->where('start_time >=', $from);
        $this->db->order_by('start_time', 'DESC');
        return $this->db->get('timesheets');    
    }
    
    //get approved timesheet from date to date
    public function get_all_approved_period($uid, $from, $to) {
        $this->db->where('approved_by IS NOT NULL', null, false);
        $this->db->where('deleted', 0);
        $this->db->where('user_id', $uid);
        $this->db->where('start_time >=', $from);
        $this->db->where('end_time <=', $to);
        $this->db->order_by('start_time', 'DESC');
        return $this->db->get('timesheets');    
    }

    //get by id
    public  function get_id_timesheet($id) {
        $this->db->select('a.*, b.*, concat(c.province, " - ", c.capital) as province_name');
        $this->db->from('timesheets a');
        $this->db->join('users b', 'a.approved_by=b.username', 'left');
        $this->db->join('province_timezone c', 'a.province=c.id', 'left');
        $this->db->where('a.id', $id);
        return $this->db->get();
    }
    
    //delete batch
    public function delete_batch($tbl, $field, $array_value) {
        $this->db->where_in($field, $array_value);
        return $this->db->delete($tbl);
    }
}
