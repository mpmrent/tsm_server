<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Description of m_function
 *
 * @author Whisnu
 */
class M_function extends MY_Model {
    function getEnum($table, $field) {
        $query = "show columns from $table like '$field'";
        $row = $this->db->query($query)->row()->Type;
        $regex = "/'(.*?)'/";
        preg_match_all($regex, $row, $enumTmp);
        $enumArray = array();
        $enumFields = $enumTmp[1];
        foreach ($enumFields as $key => $value) {
            //$enums[$value] = $value;
            array_push($enumArray, $value);
        }
        $enumCriteria = join("','", $enumArray);
        $sqlReason = "SELECT `key`, indonesia FROM tsmTest.language where `key` in ('$enumCriteria')";
        $reasonRS = $this->db->query($sqlReason);
        $enumResult = array();
        foreach ($reasonRS->result() as $rowReason) {
            $enumResult[$rowReason->key] = $rowReason->indonesia;
        }
        return $enumResult;
    }

}
