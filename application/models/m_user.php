<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Description of m_user
 *
 * @author SETIAWAN
 */
class M_user extends MY_Model {
    protected $table = 'users';


    public function __construct() {
        parent::__construct();
         $this->load->helper(array('utility'));
    }
    
    public function get_all_user() {
        $this->db->from($this->table);
        return $this->db->get();
    }
    
    public function get_id_user($username) {
        $this->db->from('user a');
        $this->db->join('user_group b','a.user_group_id=b.user_group_id','left');
        $this->db->where('username',$username);
        return $this->db->get()->result();
    }
    
    public function validate($username = null, $password = null) {
        if ($username == null) 
            $username = $this->input->post('txtUname');
        if ($password == null)
            $password = $this->input->post('txtPwd');
        $pw= simple_encrypt($password);       
        $this->db->where('username',  $username);
        $this->db->where(' user_type != "driver" ', null, true);
        $this->db->where('status', 'active');
        // $this->db->where('user_type', 'admin')->or_where('user_type', 'administrator');
        $this->db->where('password', $pw);
        $query=$this->db->get($this->table);
        return $query;
    }
    
    public function service_validate($pwd, $una) {
        $pw=  simple_encrypt($pwd);       
        $this->db->where('username', $una);
        $this->db->where('password', $pw);
        $query=$this->db->get($this->table);
        return $query;
    }
    
    public function service_validate_approve($pwd, $una) {
        $pw=  simple_encrypt($pwd);       
        $this->db->where('username', $una);
        $this->db->where('password', $pw);
        $this->db->where('user_type != "driver"', null, true);
        $query=$this->db->get($this->table);
        return $query;
    }
    
    public function get_user_id ($username) {
        $this->db->where('username',$username);
        return $this->db->get($this->table);
    }

}
