var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'node-server-tsm',
  description: 'Service node.js tsm',
  script: 'C:\\xampp\\htdocs\\tsm_live\\server.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();