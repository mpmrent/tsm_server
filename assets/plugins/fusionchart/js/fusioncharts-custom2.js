var makeCRCTable = function(){
    var c;
    var crcTable = [];
    for(var n =0; n < 256; n++){
        c = n;
        for(var k =0; k < 8; k++){
            c = ((c&1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        }
        crcTable[n] = c;
    }
    return crcTable;
}

function crc32(str) {
    var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
    var crc = 0 ^ (-1);

    for (var i = 0; i < str.length; i++ ) {
        crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
    }

    return (crc ^ (-1)) >>> 0;
};

function dechex (number) {
  if (number < 0) {
    number = 0xFFFFFFFF + number + 1;
  }
  return parseInt(number, 10)
    .toString(16);
}

function makeid() {
    var text = "";
    var possible = "ABCDEF0123456789";

    for( var i=0; i < 1; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function loadChart(jsonString, renderAt, height, caption, subCaption) {
    var color = '!';
    $.each(jsonString.datajs, function( index, value ) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
//    alert(JSON.stringify(jsonString));
    var revenueChart = new FusionCharts({
        type: 'pie3d',
        renderAt: renderAt,
        width: '100%',
        height: height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": caption,
                "subCaption": subCaption,
                "showvalues": "1",
                "showpercentvalues": "1",
                "showborder": "0",
                "showplotborder": "0",
                "showlegend": "1",
                "legendborder": "0",
                "legendposition": "bottom",
                "enablesmartlabels": "1",
                "use3dlighting": "0",
                "showshadow": "0",
                "legendbgcolor": "#CCCCCC",
                "legendbgalpha": "20",
                "legendborderalpha": "0",
                "legendshadow": "0",
                "legendnumcolumns": "3",
                "palettecolors": color
            },   
            "data": jsonString.datajs
        }
    }).render();
}

function loadChartSeries(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msline',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": datas.caption,
                "subCaption": datas.subCaption,
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "paletteColors": color,
                "bgcolor": "#ffffff",
                "showBorder": "0",
                "showShadow": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "showAxisLines": "0",
                "showAlternateHGridColor": "0",
                "divlineThickness": "1",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "xAxisName": datas.xAxisName,
                "showValues": "0"               
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname,
                    "data": data
                }
            ], 
            "trendlines": [
                {
                    "line": [
                        {
                            "startvalue": datas.startvalue,
                            "color": "#6baa01",
                            "valueOnRight": "1",
                            "displayvalue": datas.displayvalue
                        }
                    ]
                }
            ]
        }
    }).render();
}

function loadChartSpline(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mssplinearea',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Number of Footfalls Last Week",
                "subCaption": "Garden Groove harbour vs Crompton-Rancho Dom",
                "xAxisName": "Day",
                "yAxisName": "No. of Footfalls",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "subcaptionFontBold": "0",
                "paletteColors": "#6baa01,#008ee4",
                "usePlotGradientColor": "0",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showPlotBorder": "0",
                "showValues": "0",
                "showShadow": "0",
                "showAlternateHGridColor": "0",
                "showCanvasBorder": "0",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "canvasBgColor": "#ffffff",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Mon"
                        },
                        {
                            "label": "Tue"
                        },
                        {
                            "label": "Wed"
                        },
                        {
                            "vline": "true",
                            "lineposition": "0",
                            "color": "#6baa01",
                            "labelHAlign": "right",
                            "labelPosition": "0",
                            "label": "National holiday"
                        },
                        {
                            "label": "Thu"
                        },
                        {
                            "label": "Fri"
                        },
                        {
                            "label": "Sat"
                        },
                        {
                            "label": "Sun"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Garden Groove harbour",
                    "data": [
                        {
                            "value": "15123"
                        },
                        {
                            "value": "14233"
                        },
                        {
                            "value": "21507"
                        },
                        {
                            "value": "9110"
                        },
                        {
                            "value": "14829"
                        },
                        {
                            "value": "17503"
                        },
                        {
                            "value": "20202"
                        }
                    ]
                },
                {
                    "seriesname": "Crompton-Rancho Dom",
                    "data": [
                        {
                            "value": "11400"
                        },
                        {
                            "value": "12800"
                        },
                        {
                            "value": "17800"
                        },
                        {
                            "value": "10400"
                        },
                        {
                            "value": "11800"
                        },
                        {
                            "value": "13100"
                        },
                        {
                            "value": "20800"
                        }
                    ]
                }
            ]
        }
    }).render();
}