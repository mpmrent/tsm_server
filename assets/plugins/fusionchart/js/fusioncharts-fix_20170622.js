
function linearChart(data, type) {
    var datas = JSON.parse(data);
    var datatarget = JSON.parse(JSON.stringify( eval("[" + datas.target + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    
    var value = 0;
    var array = $.map(datas.data, function(dataSeries, index) {
        var data = JSON.parse(JSON.stringify( eval("[" + dataSeries + "]")));
        var array = $.map(data, function(values, index) {
            value = value+parseFloat(values.value); 
//            if (value < 1)
//                alert(value);
        });
    });
    
    var target = [];
    var array = $.map(datatarget, function(value, index) {
        target.push(value.value);
    });
    
    var min = 0;
    var max = 0;
    if (value>target[0])
        min = target[0]-parseInt(target[0])/5;
    else
        min = value;
    
    if (value>target[4])
        max = value;
    else
        max = target[4];
    
    var color = [];
    var array = $.map(category, function(value, index) {
        color.push('#' + dechex(crc32(value.label+'r')).substr(4, 1) + dechex(crc32(value.label+'fe254s')).substr(0, 5)); 
    });
    if (type == 'asc')
        color = ["#9068be", "#e62739", "#e05038", "#e6af4b", "#00c853"];
    if (type == 'desc')
        color = ["#00c853", "#e6af4b", "#e05038", "#e62739", "#9068be"];
    var visitChart = new FusionCharts({
        type: 'HLinearGauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "manageresize": "1",
            "bgcolor": "FFFFFF",
            "bgalpha": "0",
            "showborder": "0",
            "upperlimit": max,
            "lowerlimit": min,
            "gaugeroundradius": "5",
            "chartbottommargin": "10",
            "showValue":"1",
            "valueAbovePointer": "1",
            "pointerOnTop": "1",
            "ticksbelowgauge": "0",
            "showgaugelabels": "1",
            "showTickValues": "0",
            "gaugeFillMix": "{light-0},{light-0},{dark-0}",
            "gaugeFillRatio": "0,0,0",
            "baseFontColor": "#000000",
//            "valueabovepointer": "0",
//            "pointerontop": "1",
//            "pointerradius": "9",
            "numberprefix": ""
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": min,
                    "maxvalue": target[0],
                    "label": "-Q4",
                    "code": color[0]
                },
                {
                    "minvalue": target[0],
                    "maxvalue": target[1],
                    "label": "Q1",
                    "code": color[1]
                },
                {
                    "minvalue": target[1],
                    "maxvalue": target[2],
                    "label": "Q2",
                    "code": color[2]
                },
                {
                    "minvalue": target[2],
                    "maxvalue": target[3],
                    "label": "Q3",
                    "code": color[3]
                },
                {
                    "minvalue": target[3],
                    "maxvalue": target[4],
                    "label": "Q4",
                    "code": color[4]
                },
            ]
        },
        "trendPoints": {
            "point": [
                {
                    "startValue": target[0],
                    "color": "#000000",
                    "dashed": "0",
                    "showValue":"1"
//                    "dashlen": "3",
//                    "dashgap": "3",
//                    "thickness": "2"
                },
                {
                    "startValue": target[1],
                    "color": "#000000",
                    "dashed": "0",
//                    "dashlen": "3",
//                    "dashgap": "3",
//                    "thickness": "2"
                },
                {
                    "startValue": target[2],
                    "color": "#000000",
                    "dashed": "0",
//                    "dashlen": "3",
//                    "dashgap": "3",
//                    "thickness": "2"
                },
                {
                    "startValue": target[3],
                    "color": "#000000",
                    "dashed": "0",
//                    "dashlen": "3",
//                    "dashgap": "3",
//                    "thickness": "2"
                },
                {
                    "startValue": target[4],
                    "color": "#000000",
                    "dashed": "0",
//                    "dashlen": "3",
//                    "dashgap": "3",
//                    "thickness": "2"
                }
            ]
        },
        "pointers": {
            "pointer": [
                {
                    "borderColor": "#333333",
                    "borderThickness": "2",
                    "borderAlpha": "60",
                    "bgColor": "#0075c2",
                    "bgAlpha": "75",
                    "radius": "6",
                    "sides": "4",
                    "baseFontColor": "#000000",
                    "value": value
                }
            ]
        }
    }
    }).render();
}

function combyCar(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify( eval("[" + datas.data[0] + "]")));
    var data2 = JSON.parse(JSON.stringify( eval("[" + datas.data[1] + "]")));
    var data3 = JSON.parse(JSON.stringify( eval("[" + datas.data[2] + "]")));
    var data4 = JSON.parse(JSON.stringify( eval("[" + datas.data[3] + "]")));
    var data5 = JSON.parse(JSON.stringify( eval("[" + datas.data[4] + "]")));
    var data6 = JSON.parse(JSON.stringify( eval("[" + datas.data[5] + "]")));
    var data7 = JSON.parse(JSON.stringify( eval("[" + datas.data[6] + "]")));
    var data8 = JSON.parse(JSON.stringify( eval("[" + datas.data[7] + "]")));
    var target = JSON.parse(JSON.stringify( eval("[" + datas.target + "]")));
    var baseline = JSON.parse(JSON.stringify( eval("[" + datas.baseline + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var color = '!';
    var targetSeries = '';
    if (target != '')
        targetSeries = 'Target';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'r')).substr(4, 1) + dechex(crc32(value.label+'fe254s')).substr(0, 5); 
    });
    alert(baseline);
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscombidy2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "numberPrefix": "",
                "showBorder": "0",
                "showValues": "1",
                "paletteColors": color,
                "bgColor": "#ffffff",
                "showCanvasBorder": "0",
                "sYAxisMaxValue": "100",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "baseFontColor": "#000000",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "showAlternateHGridColor": "0",
                "usePlotGradientColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666",
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesName": datas.seriesName[0],
                    "renderAs": datas.types[0],
                    "data": data1
                },
                {
                    "seriesName": datas.seriesName[1],
                    "renderAs": datas.types[1],
                    "data": data2
                },
                {
                    "seriesName": datas.seriesName[2],
                    "renderAs": datas.types[2],
                    "data": data3
                },
                {
                    "seriesName": datas.seriesName[3],
                    "renderAs": datas.types[3],
                    "data": data4
                },
                {
                    "seriesName": datas.seriesName[4],
                    "renderAs": datas.types[4],
                    "data": data5
                },
                {
                    "seriesName": datas.seriesName[5],
                    "renderAs": datas.types[5],
                    "data": data6
                },
                {
                    "seriesName": datas.seriesName[6],
                    "renderAs": datas.types[6],
                    "data": data7
                },
                {
                    "seriesName": datas.seriesName[7],
                    "renderAs": datas.types[7],
                    "data": data8
                },
                {
                    "seriesName": targetSeries,
                    "renderAs": "line",
                    "data": target
                },
            ],
            "trendlines": [
                {
                    "line": [
                        {
                            "startvalue": "18500",
                            "color": "#1aaf5d",
                            "displayvalue": "Average{br}weekly{br}footfall",
                            "valueOnRight": "1",
                            "thickness": "2"
                        }
                    ]
                }
            ]
        }
    }).render();
}

function stackedcolumn(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify( eval("[" + datas.data[0] + "]")));
    var data2 = JSON.parse(JSON.stringify( eval("[" + datas.data[1] + "]")));
    var data3 = JSON.parse(JSON.stringify( eval("[" + datas.data[2] + "]")));
    var data4 = JSON.parse(JSON.stringify( eval("[" + datas.data[3] + "]")));
    var data5 = JSON.parse(JSON.stringify( eval("[" + datas.data[4] + "]")));
    var data6 = JSON.parse(JSON.stringify( eval("[" + datas.data[5] + "]")));
    var data7 = JSON.parse(JSON.stringify( eval("[" + datas.data[6] + "]")));
    var data8 = JSON.parse(JSON.stringify( eval("[" + datas.data[7] + "]")));
    var target = JSON.parse(JSON.stringify( eval("[" + datas.target + "]")));
    var baseline = JSON.parse(JSON.stringify( eval("[" + datas.baseline + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var color = '!';
    var targetSeries = '';
    if (target != '')
        targetSeries = 'Target';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'r')).substr(4, 1) + dechex(crc32(value.label+'fe254s')).substr(0, 5); 
    });
    alert(baseline);
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Revenue split by product category",
                "subCaption": "For current year",
                "xAxisname": "Quarter",
                "yAxisName": "Revenues (In USD)",
                "showSum": "1",
                "numberPrefix": "$",
                "theme": "fint"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Q1"
                        },
                        {
                            "label": "Q2"
                        },
                        {
                            "label": "Q3"
                        },
                        {
                            "label": "Q4"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Food Products",
                    "data": [
                        {
                            "value": "11000"
                        },
                        {
                            "value": "15000"
                        },
                        {
                            "value": "13500"
                        },
                        {
                            "value": "15000"
                        }
                    ]
                },
                {
                    "seriesname": "Non-Food Products",
                    "data": [
                        {
                            "value": "11400"
                        },
                        {
                            "value": "14800"
                        },
                        {
                            "value": "8300"
                        },
                        {
                            "value": "11800"
                        }
                    ],
                },
            ]
        }
    }).render();
}

function angularChart(data, type, title, maxVal, dataVal) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify( eval("[" + datas.data[0] + "]")));
    var datatarget = JSON.parse(JSON.stringify( eval("[" + datas.target + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var titles = JSON.parse(JSON.stringify( eval(title)));
    
    var value = [];
    var array = $.map(data, function(values, index) {
//        value = value+parseInt(values.value); 
        value.push(values.value);
    });
    
    var target = [];
    var array = $.map(datatarget, function(value, index) {
        target.push(value.value);
    });
    
    var min = 0;
    var max = parseInt(value[0])+parseInt(value[1]);
    if (maxVal != null) 
        max = value[maxVal];
    var val = max;
    if (dataVal != null) 
        val = value[dataVal];
        
//    alert(max);
//    if (value>target[0])
//        min = target[0]-parseInt(target[0])/5;
//    else
//        min = value;
//    
//    if (value>target[2])
//        max = value;
//    else
//        max = target[2];
    
    var color = [];
    var array = $.map(category, function(value, index) {
        color.push(dechex(crc32(value.label+'r')).substr(4, 1) + dechex(crc32(value.label+'fe254s')).substr(0, 5)); 
    });
    if (type == 'asc')
        color = ["6baa01", "f8bd19", "e44a00"];
    else
        color = ["e44a00", "f8bd19", "#6baa01"];
    var visitChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
//            "caption": "Jatuh Tempo ("+datas.jatuh_tempo+") / GT & EXT ("+datas.gt_ext+")",
//            "subcaption": "Units",
            "plotToolText": "Current Score: $value",
            "theme": "fint",
            "numberprefix": "",
            "chartBottomMargin": "50",
            "gaugeBorderColor": "{dark-30}",
            "gaugeBorderThickness": "5",
            "gaugeBorderAlpha": "100",
            "showValue": "1",
            "valueBelowPivot": "1",
            "bgcolor": "FFFFFF",
            "pivotRadius": "0",
            "showborder": "0",
            "manageresize": "1",
            "origw": "400",
            "origh": "250",
            "managevalueoverlapping": "1",
            "autoaligntickvalues": "1",
            "fillangle": "45",
            "upperlimit": target[2],
            "lowerlimit": "0",
            "majortmnumber": "10",
            "majortmheight": "8",
            "showgaugeborder": "0",
            "gaugeouterradius": "140",
            "charttopmargin": "0",
            "gaugeoriginx": "205",
            "gaugeoriginy": "206",
            "gaugeinnerradius": "2",
            "formatnumberscale": "1",
            "decmials": "1",
            "tickmarkdecimals": "1",
            "pivotradius": "10",
            "showpivotborder": "1",
            "pivotbordercolor": "000000",
            "pivotborderthickness": "10",
            "pivotfillmix": "666666",
            "tickvaluedistance": "10",
            "valuebelowpivot": "1",
            "showvalue": "1",
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "0",
                    "maxvalue": target[0],
                    "code": color[0],
                    "borderColor": "#409C40",
                    "borderAlpha": "100"
                },
                {
                    "minvalue": target[0],
                    "maxvalue": target[1],
                    "code": "f8bd19",
                    "borderColor": "#696840",
                    "borderAlpha": "100"
                },
                {
                    "minvalue": target[1],
                    "maxvalue": target[2],
                    "code": color[2],
                    "borderColor": "#780101",
                    "borderAlpha": "100"
                }
            ]
        },
        "dials": {
            "dial": [
                {
//                    datas.value
                    "value": val,
                    "borderalpha": "0",
                    "bgcolor": "000000",
                    "basewidth": "20",
                    "topwidth": "1",
                    "radius": "130"
                }
            ]
        },
//        "trendPoints": {
//            "point": [
//                {
//                    "startValue": "0",
//                    "endValue": parseInt(value[0]),
//                    "color": "#71c1f5",
//                    "radius": "185",
//                    "innerRadius": "80"
//                },
//                {
//                    "startValue": parseInt(value[0]),
//                    "endValue": max,
//                    "color": "#9068be",
//                    "radius": "185",
//                    "innerRadius": "80"
//                }
//            ]
//        },
        "annotations": {
            "origw": "450",
            "origh": "300",
            "autoscale": "1",
            "showBelow": "0",
            "groups": [
                {
                    "id": "arcs",
                    "items": [
                        {
                            "id": "national-cs-bg",
                            "type": "rectangle",
                            "x": "$chartCenterX+2",
                            "y": "$chartEndY - 45",
                            "tox": "$chartCenterX + 200",
                            "toy": "$chartEndY - 25",
                            "fillcolor": "#9068be"
                        },
                        {
                            "id": "national-cs-text",
                            "type": "Text",
                            "color": "#ffffff",
                            "label": titles[1] +" : "+value[1],
                            "fontSize": "12",
                            "align": "left",
                            "x": "$chartCenterX + 7",
                            "y": "$chartEndY - 35"
                        },
                        {
                            "id": "state-cs-bg",
                            "type": "rectangle",
                            "x": "$chartCenterX-2",
                            "y": "$chartEndY - 45",
                            "tox": "$chartCenterX - 200",
                            "toy": "$chartEndY - 25",
                            "fillcolor": "#71c1f5"
                        },
                        {
                            "id": "state-cs-text",
                            "type": "Text",
                            "color": "#ffffff",
                            "label": titles[0] +" : "+value[0],
                            "fontSize": "12",
                            "align": "right",
                            "x": "$chartCenterX - 7",
                            "y": "$chartEndY - 35"
                        },
                        {
                            "id": "store-cs-bg",
                            "type": "rectangle",
                            "x": "$chartCenterX-200",
                            "y": "$chartEndY - 22",
                            "tox": "$chartCenterX + 200",
                            "toy": "$chartEndY - 2",
                            "fillcolor": "#0075c2"
                        },
                        {
                            "id": "state-cs-text",
                            "type": "Text",
                            "color": "#ffffff",
                            "label":  Math.round(parseInt(value[1])/max*100)+ "%",
                            "fontSize": "12",
                            "align": "center",
                            "x": "$chartCenterX + 10",
                            "y": "$chartEndY - 12"
                        }
                    ]
                }
            ]
        }
    }
    }).render();
}

//Math.round(parseInt(value[0])/max*100)+ "%" + " | " +