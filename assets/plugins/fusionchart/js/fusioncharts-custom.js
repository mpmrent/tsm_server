var makeCRCTable = function(){
    var c;
    var crcTable = [];
    for(var n =0; n < 256; n++){
        c = n;
        for(var k =0; k < 8; k++){
            c = ((c&1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        }
        crcTable[n] = c;
    }
    return crcTable;
}

function crc32(str) {
    var crcTable = window.crcTable || (window.crcTable = makeCRCTable());
    var crc = 0 ^ (-1);

    for (var i = 0; i < str.length; i++ ) {
        crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
    }

    return (crc ^ (-1)) >>> 0;
};

function dechex (number) {
  if (number < 0) {
    number = 0xFFFFFFFF + number + 1;
  }
  return parseInt(number, 10)
    .toString(16);
}

function makeid() {
    var text = "";
    var possible = "ABCDEF0123456789";

    for( var i=0; i < 1; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function loadChart(jsonString, renderAt, height, caption, subCaption) {
    var color = '!';
    $.each(jsonString.datajs, function( index, value ) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
//    alert(JSON.stringify(jsonString));
    var revenueChart = new FusionCharts({
        type: 'pie3d',
        renderAt: renderAt,
        width: '100%',
        height: height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": caption,
                "subCaption": subCaption,
                "showvalues": "1",
                "showpercentvalues": "1",
                "showborder": "0",
                "showplotborder": "0",
                "showlegend": "1",
                "legendborder": "0",
                "legendposition": "bottom",
                "enablesmartlabels": "1",
                "use3dlighting": "0",
                "showshadow": "0",
                "legendbgcolor": "#CCCCCC",
                "legendbgalpha": "20",
                "legendborderalpha": "0",
                "legendshadow": "0",
                "legendnumcolumns": "3",
                "palettecolors": color,     
                "exportenabled": "1",
                "exportatclient": "1",
                "exporthandler": "http://export.api3.fusioncharts.com",
                "html5exporthandler": "http://export.api3.fusioncharts.com"
            },   
            "data": jsonString.datajs
        }
    }).render();
}

function loadChartSeries(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msline',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": datas.caption,
                "subCaption": datas.subCaption,
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "paletteColors": color,
                "bgcolor": "#ffffff",
                "showBorder": "0",
                "showShadow": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "showAxisLines": "0",
                "showAlternateHGridColor": "0",
                "divlineThickness": "1",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1",
                "xAxisName": datas.xAxisName,
                "showValues": "0",     
                "exportenabled": "1",
                "exportatclient": "1",
                "exporthandler": "http://export.api3.fusioncharts.com",
                "html5exporthandler": "http://export.api3.fusioncharts.com"          
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname,
                    "data": data
                }
            ], 
            "trendlines": [
                {
                    "line": datas.lines
                }
            ]
        }
    }).render();
}

function loadChartSpline(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mssplinearea',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Number of Footfalls Last Week",
                "subCaption": "Garden Groove harbour vs Crompton-Rancho Dom",
                "xAxisName": "Day",
                "yAxisName": "No. of Footfalls",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "subcaptionFontBold": "0",
                "paletteColors": "#6baa01,#008ee4",
                "usePlotGradientColor": "0",
                "bgColor": "#ffffff",
                "showBorder": "0",
                "showPlotBorder": "0",
                "showValues": "0",
                "showShadow": "0",
                "showAlternateHGridColor": "0",
                "showCanvasBorder": "0",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "canvasBgColor": "#ffffff",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Mon"
                        },
                        {
                            "label": "Tue"
                        },
                        {
                            "label": "Wed"
                        },
                        {
                            "vline": "true",
                            "lineposition": "0",
                            "color": "#6baa01",
                            "labelHAlign": "right",
                            "labelPosition": "0",
                            "label": "National holiday"
                        },
                        {
                            "label": "Thu"
                        },
                        {
                            "label": "Fri"
                        },
                        {
                            "label": "Sat"
                        },
                        {
                            "label": "Sun"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Garden Groove harbour",
                    "data": [
                        {
                            "value": "15123"
                        },
                        {
                            "value": "14233"
                        },
                        {
                            "value": "21507"
                        },
                        {
                            "value": "9110"
                        },
                        {
                            "value": "14829"
                        },
                        {
                            "value": "17503"
                        },
                        {
                            "value": "20202"
                        }
                    ]
                },
                {
                    "seriesname": "Crompton-Rancho Dom",
                    "data": [
                        {
                            "value": "11400"
                        },
                        {
                            "value": "12800"
                        },
                        {
                            "value": "17800"
                        },
                        {
                            "value": "10400"
                        },
                        {
                            "value": "11800"
                        },
                        {
                            "value": "13100"
                        },
                        {
                            "value": "20800"
                        }
                    ]
                }
            ]
        }
    }).render();
}

function loadChartCircle(data, type) {
    var datas = JSON.parse(data);
//    alert(JSON.stringify(eval("[" + datas.data + "]")));
//alert(JSON.stringify(datas.data));
    var data = JSON.parse(JSON.stringify(datas.data));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: type,
        renderAt: datas.renderAt,
        width: '120%',
        width: datas.width,
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "paletteColors": color,
                "bgColor": "#ffffff",
                "showBorder": "0",
                "use3DLighting": "0",
                "showShadow": "0",
                "startingAngle": "0",
                "showPercentValues": "1",
                "showPercentInTooltip": "1",
                "legendPosition": "bottom",
                "decimals": "1",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "defaultCenterLabel": datas.insidecircle,
                "toolTipPadding": "5",
                "showHoverEffect": "1",
                "showLegend": "1",
                "enableSmartLabels": "1",
                "showLabels": "0",
                "showValues": "1",
                "placevaluesInside ": "1",
//                "labelDistance  ": "2",
//                "showLegend ": "0",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "legendItemFontSize": "11",
                "legendItemFontColor": "#666666",
                "useDataPlotColorForLabels": "0"
            },
            "data": data
        }
    }).render();
}

function loadChartAngular(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'doughnut3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Customer Satisfaction Score",
                "lowerlimit": "0",
                "upperlimit": "100",
                "lowerlimitdisplay": "Bad",
                "upperlimitdisplay": "Good",
                "palette": "1",
                "numbersuffix": "%",
                "tickvaluedistance": "10",
                "showvalue": "0",
                "gaugeinnerradius": "0",
                "bgcolor": "FFFFFF",
                "pivotfillcolor": "333333",
                "pivotradius": "8",
                "pivotfillmix": "333333, 333333",
                "pivotfilltype": "radial",
                "pivotfillratio": "0,100",
                "showtickvalues": "1",
                "showborder": "0"
            },
            "colorrange": {
                "color": [
                    {
                        "minvalue": "0",
                        "maxvalue": "45",
                        "code": "e44a00"
                    },
                    {
                        "minvalue": "45",
                        "maxvalue": "75",
                        "code": "f8bd19"
                    },
                    {
                        "minvalue": "75",
                        "maxvalue": "100",
                        "code": "6baa01"
                    }
                ]
            },
            "dials": {
                "dial": [
                    {
                        "value": "92",
                        "rearextension": "15",
                        "radius": "100",
                        "bgcolor": "333333",
                        "bordercolor": "333333",
                        "basewidth": "8"
                    }
                ]
            }
        }
    }).render();
}


function loadChartBar(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'column2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "xAxisname": "",
                "yAxisName": "Revenues (In USD)",
                "numberPrefix": "$",
                "plotFillAlpha": "80",
                "paletteColors": "#0075c2,#1aaf5d",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "showBorder": "0",
                "bgColor": "#ffffff",
                "showShadow": "0",
                "canvasBgColor": "#ffffff",
                "canvasBorderAlpha": "0",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "usePlotGradientColor": "0",
                "showplotborder": "0",
                "valueFontColor": "#ffffff",
                "placeValuesInside": "1",
                "showHoverEffect": "1",
                "rotateValues": "1",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "showAlternateHGridColor": "0",
                "legendBgAlpha": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname,
                    "data": data
                }
            ]
        }
    }).render();
}


function loadChartLines(data, type) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify( eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: type,
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "paletteColors": color,
                "bgcolor": "#ffffff",
                "valueFontColor": "#000000",
                "showBorder": "0",
                "showShadow": "0",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "showLegend": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "1",
                "showAxisLines": "1",
                "showAlternateHGridColor": "0",
                "xAxisLineColor": "#999999",
                "showValues": "1",
                "divLineDashed": "1",
//                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "divLineGapLen": "1"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    /*"seriesname": "Bakersfield Central",*/
                    "data": data
                }
            ],
            "trendlines": [
                {
                    "line": datas.lines
                }
            ]
        }
    }).render();
}

function loadChartStackedCol(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msspline',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "showvalues": "1",
                "plotgradientcolor": "",
                "formatnumberscale": "0",
                "showplotborder": "0",
                "palettecolors": "#EED17F,#97CBE7,#074868,#B0D67A,#2C560A,#DD9D82",
                "canvaspadding": "0",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showcanvasborder": "0",
                "legendborderalpha": "0",
                "legendshadow": "0",
                "interactivelegend": "0",
                "showpercentvalues": "1",
                "showsum": "1",
                "canvasborderalpha": "0",
                "showborder": "0"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": "Units",
                    "renderas": "Area",
                    "data": data
                },
            ]
        }
    }).render();
}

function loadChartStackedCol2(data, type) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify(eval("[" + datas.data1 + "]")));
    var data2 = JSON.parse(JSON.stringify(eval("[" + datas.data2 + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: type,
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "showBorder": "0",
                "legendShadow": "0",
                "showValue": "#CCCCCC",
                "valueFontColor": "#ffffff",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "16",
                "showHoverEffect": "1",
                "placevaluesInside": "1",
                "palettecolors": color,
                "baseFontColor": "#999999"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname1,
                    "renderas": "Area",
                    "data": data1
                },
                {
                    "seriesname": datas.seriesname2,
                    "renderas": "Area",
                    "data": data2
                },
            ]
        }
    }).render();
}

function loadChartStackedColLine(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'stackedColumn2DLine',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "showvalues": "0",
                "plotgradientcolor": "",
                "formatnumberscale": "0",
                "showplotborder": "0",
                "palettecolors": "#EED17F,#97CBE7,#074868,#B0D67A,#2C560A,#DD9D82",
                "canvaspadding": "0",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showcanvasborder": "0",
                "legendborderalpha": "0",
                "legendshadow": "0",
                "interactivelegend": "0",
                "showpercentvalues": "1",
                "showsum": "1",
                "canvasborderalpha": "0",
                "showborder": "0"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": "Dalam Kota",
//                    "renderas": "stackedcolumn2d",
                    "data": data
                },
                {
                    "seriesname": "Luar Kota",
//                    "renderas": "column2d",
                    "data": datas.data2
                },
                {
                    "seriesname": "Quantity",
                    "renderas": "Line",
                    "color": "f8bd19",
                    "data": datas.datalines
                }
            ]
        }
    }).render();
}

function loadChartBar2(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Terminate = JSON.parse(JSON.stringify(eval("[" + data.Terminate + "]")));
    var Agreement = JSON.parse(JSON.stringify(eval("[" + data.Agreement + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msbar2d',
        renderAt: datas.renderAt,
        width: '120%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "xAxisname": "",
                "yAxisName": "Qty (Units)",
                "numberPrefix": "",
                "plotFillAlpha": "80",
                "paletteColors": "#4d77ba, #5606e5",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "showBorder": "0",
                "bgColor": "#ffffff",
                "showShadow": "0",
                "canvasBgColor": "#ffffff",
                "canvasBorderAlpha": "0",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "usePlotGradientColor": "0",
                "showplotborder": "0",
                "valueFontColor": "#ffffff",
                "placeValuesInside": "1",
                "showHoverEffect": "1",
                "rotateValues": "1",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "showAlternateHGridColor": "0",
                "legendBgAlpha": "0",
                "legendBorderAlpha": "0",
                "showLegend": "1",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset":  [
                {"seriesname":"Terminate",
                    "data":Terminate
                },
                {"seriesname":"New Rent",
                    "data": Agreement
                },
            ]
            
        }
    }).render();
}

function loadColumn5(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify( eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Rupiah (Milliar)",
                "showvalues": "1",
                "plotgradientcolor": "",
                "formatnumberscale": "0",
                "showplotborder": "0",
                "palettecolors": color,
                "valueFontColor": "#000000",
                "canvaspadding": "0",
                "bgcolor": "FFFFFF",
                "showalternatehgridcolor": "0",
                "divlinecolor": "CCCCCC",
                "showcanvasborder": "0",
                "showLegend": "0",
                "legendborderalpha": "0",
                "legendshadow": "0",
                "interactivelegend": "0",
                "showpercentvalues": "1",
                "showsum": "1",
                "canvasborderalpha": "0",
                "showborder": "0",
                "scrollToDate": "02/02/2017"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname,
                    "renderas": "Area",
                    "data": data
                },
            ]
        }
    }).render();
}

function loadChartBar3(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Terminate = JSON.parse(JSON.stringify(eval("[" + data.Terminate + "]")));
    var Agreement = JSON.parse(JSON.stringify(eval("[" + data.Agreement + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msbar2d',
        renderAt: datas.renderAt,
        width: '110%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "xAxisname": "",
                "yAxisName": "Qty (Units)",
                "numberPrefix": "",
                "plotFillAlpha": "80",
                "paletteColors": "#4d77ba, #5606e5",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "showBorder": "0",
                "bgColor": "#ffffff",
                "showShadow": "0",
                "canvasBgColor": "#ffffff",
                "canvasBorderAlpha": "0",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "usePlotGradientColor": "0",
                "showplotborder": "0",
                "valueFontColor": "#ffffff",
                "placeValuesInside": "1",
                "showHoverEffect": "1",
                "rotateValues": "1",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "showAlternateHGridColor": "0",
                "legendBgAlpha": "0",
                "legendBorderAlpha": "0",
                "showLegend": "1",
                "legendShadow": "0",
                "legendItemFontSize": "14",
                "legendItemFontColor": "#666666"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset":  [
                {"seriesname":"Terminate",
                    "data":Terminate
                },
            ]
            
        }
    }).render();
}

function loadChartStackedBar3(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify(eval("[" + datas.data1 + "]")));
    var data2 = JSON.parse(JSON.stringify(eval("[" + datas.data2 + "]")));
    var data3 = JSON.parse(JSON.stringify(eval("[" + datas.data3 + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: datas.renderAt,
        width: '110%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#000000",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect": "1",
                "palettecolors": color,
                "baseFontColor": "#999999"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname1,
                    "renderas": "Area",
                    "data": data1
                },
                {
                    "seriesname": datas.seriesname2,
                    "renderas": "Area",
                    "data": data2
                },
                {
                    "seriesname": datas.seriesname3,
                    "renderas": "Area",
                    "data": data3
                },
            ]
        }
    }).render();
}

function loadChartStackedBar4(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify(eval("[" + datas.data1 + "]")));
    var data2 = JSON.parse(JSON.stringify(eval("[" + datas.data2 + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var colors = JSON.parse(JSON.stringify(eval("[" + datas.color + "]")));
    var color = '!';
    var array = $.map(colors, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.color)).substr(4, 1) + dechex(crc32(value.color)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscombidy2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "bgColor": "#ffffff",
                "showLegend": "1",
                "displayValue": "0",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#73879c",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect": "1",
                "palettecolors": color,
                "baseFontColor": "#999999"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname1,
                    "renderas": "column2d",
                    "showValues": "1",
                    "data": data1
                },
                {
                    "seriesname": 'Target',
                    "renderas": "Line",
                    "showValues": "0",
                    "data": [ 
                        {
                            "value": "600"
                        },
                        {
                            "value": "1500"
                        },
                        {
                            "value": "2000"
                        },
                        {
                            "value": "4000"
                        }
                    ]
                }
            ]
        }
    }).render();
}

function loadChartBar22(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msbar2d',
        renderAt: datas.renderAt,
        width: '120%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "xAxisname": "",
                "yAxisName": "Days (24H)",
                "numberPrefix": "",
                "plotFillAlpha": "80",
                "paletteColors": "#4d77ba, #5606e5",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "showBorder": "0",
                "bgColor": "#ffffff",
                "showShadow": "0",
                "canvasBgColor": "#ffffff",
                "canvasBorderAlpha": "0",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "usePlotGradientColor": "0",
                "showplotborder": "0",
                "valueFontColor": "#ffffff",
                "placeValuesInside": "1",
                "showHoverEffect": "1",
                "rotateValues": "1",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "showAlternateHGridColor": "0",
                "legendBgAlpha": "0",
                "legendBorderAlpha": "0",
                "showLegend": "1",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset":  [
                {"seriesname":"Lead",
                    "data":Lead
                }
            ]
            
        }
    }).render();
}

function loadChart1(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'stackedcolumn3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Product-wise quarterly revenue in current year",
                "subCaption": "Harry's SuperMart",
                "xAxisname": "Quarter",
                "yAxisName": "Revenue (In USD)",
                "numberPrefix": "$",
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect": "1"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Q1"
                        },
                        {
                            "label": "Q2"
                        },
                        {
                            "label": "Q3"
                        },
                        {
                            "label": "Q4"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Food Products",
                    "data": [
                        {
                            "value": "121000"
                        },
                        {
                            "value": "135000"
                        },
                        {
                            "value": "123500"
                        },
                        {
                            "value": "145000"
                        }
                    ]
                },
                {
                    "seriesname": "Non-Food Products",
                    "data": [
                        {
                            "value": "131400"
                        },
                        {
                            "value": "154800"
                        },
                        {
                            "value": "98300"
                        },
                        {
                            "value": "131800"
                        }
                    ]
                }
            ]
        }
    }).render();
}
function loadChart2(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mssplinearea',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Number of Footfalls Last Week",
            "subCaption": "Garden Groove harbour vs Crompton-Rancho Dom",
            "xAxisName": "Day",
            "yAxisName": "No. of Footfalls",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "baseFontColor": "#333333",
            "baseFont": "Helvetica Neue,Arial",
            "subcaptionFontBold": "0",
            "paletteColors": "#6baa01,#008ee4",
            "usePlotGradientColor": "0",
            "bgColor": "#ffffff",
            "showBorder": "0",
            "showPlotBorder": "0",
            "showValues": "0",
            "showShadow": "0",
            "showAlternateHGridColor": "0",
            "showCanvasBorder": "0",
            "showXAxisLine": "1",
            "xAxisLineThickness": "1",
            "xAxisLineColor": "#999999",
            "canvasBgColor": "#ffffff",
            "divlineAlpha": "100",
            "divlineColor": "#999999",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "toolTipColor": "#ffffff",
            "toolTipBorderThickness": "0",
            "toolTipBgColor": "#000000",
            "toolTipBgAlpha": "80",
            "toolTipBorderRadius": "2",
            "toolTipPadding": "5"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Mon"
                    },
                    {
                        "label": "Tue"
                    },
                    {
                        "label": "Wed"
                    },
                    {
                        "vline": "true",
                        "lineposition": "0",
                        "color": "#6baa01",
                        "labelHAlign": "right",
                        "labelPosition": "0",
                        "label": "National holiday"
                    },
                    {
                        "label": "Thu"
                    },
                    {
                        "label": "Fri"
                    },
                    {
                        "label": "Sat"
                    },
                    {
                        "label": "Sun"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "Garden Groove harbour",
                "data": [
                    {
                        "value": "15123"
                    },
                    {
                        "value": "14233"
                    },
                    {
                        "value": "21507"
                    },
                    {
                        "value": "9110"
                    },
                    {
                        "value": "14829"
                    },
                    {
                        "value": "17503"
                    },
                    {
                        "value": "20202"
                    }
                ]
            },
            {
                "seriesname": "Crompton-Rancho Dom",
                "data": [
                    {
                        "value": "11400"
                    },
                    {
                        "value": "12800"
                    },
                    {
                        "value": "17800"
                    },
                    {
                        "value": "10400"
                    },
                    {
                        "value": "11800"
                    },
                    {
                        "value": "13100"
                    },
                    {
                        "value": "20800"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart3(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Harry's SuperMart",
            "subCaption": "Sales by quarter",
            "xAxisName": "Quarter",
            "yAxisName": "Sales (In USD)",
            "numberPrefix": "$",
            "paletteColors": "#0075c2,#1aaf5d,#f2c500",
            "bgColor": "#ffffff",
            "showBorder": "0",
            "showCanvasBorder": "0",
            "usePlotGradientColor": "0",
            "plotBorderAlpha": "10",
            "legendBorderAlpha": "0",
            "legendBgAlpha": "0",
            "legendShadow": "0",
            "showHoverEffect": "1",
            "valueFontColor": "#ffffff",
            "rotateValues": "1",
            "placeValuesInside": "1",
            "divlineColor": "#999999",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "canvasBgColor": "#ffffff",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Q1"
                    },
                    {
                        "label": "Q2"
                    },
                    {
                        "label": "Q3"
                    },
                    {
                        "label": "Q4"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "Previous Year",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
            {
                "seriesname": "Current Year",
                "data": [
                    {
                        "value": "25400"
                    },
                    {
                        "value": "29800"
                    },
                    {
                        "value": "21800"
                    },
                    {
                        "value": "26800"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart4(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'stackedcolumn2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "Product-wise quarterly revenue in current year",
                "subCaption": "Harry's SuperMart",
                "xAxisname": "Quarter",
                "yAxisName": "Revenue (In USD)",
                "numberPrefix": "$",
                "paletteColors": "#0075c2,#1aaf5d",
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect": "1"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Q1"
                        },
                        {
                            "label": "Q2"
                        },
                        {
                            "label": "Q3"
                        },
                        {
                            "label": "Q4"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Food Products",
                    "data": [
                        {
                            "value": "121000"
                        },
                        {
                            "value": "135000"
                        },
                        {
                            "value": "123500"
                        },
                        {
                            "value": "145000"
                        }
                    ]
                },
                {
                    "seriesname": "Non-Food Products",
                    "data": [
                        {
                            "value": "131400"
                        },
                        {
                            "value": "154800"
                        },
                        {
                            "value": "98300"
                        },
                        {
                            "value": "131800"
                        }
                    ]
                }
            ]
        }
    }).render();
}
function loadChart5(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msline',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Number of visitors last week",
            "subCaption": "Bakersfield Central vs Los Angeles Topanga",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "paletteColors": "#0075c2,#1aaf5d",
            "bgcolor": "#ffffff",
            "showBorder": "0",
            "showShadow": "0",
            "showCanvasBorder": "0",
            "usePlotGradientColor": "0",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "showAxisLines": "0",
            "showAlternateHGridColor": "0",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "xAxisName": "Day",
            "showValues": "0"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Mon"
                    },
                    {
                        "label": "Tue"
                    },
                    {
                        "label": "Wed"
                    },
                    {
                        "vline": "true",
                        "lineposition": "0",
                        "color": "#6baa01",
                        "labelHAlign": "center",
                        "labelPosition": "0",
                        "label": "National holiday",
                        "dashed": "1"
                    },
                    {
                        "label": "Thu"
                    },
                    {
                        "label": "Fri"
                    },
                    {
                        "label": "Sat"
                    },
                    {
                        "label": "Sun"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "Bakersfield Central",
                "data": [
                    {
                        "value": "15123"
                    },
                    {
                        "value": "14233"
                    },
                    {
                        "value": "25507"
                    },
                    {
                        "value": "9110"
                    },
                    {
                        "value": "15529"
                    },
                    {
                        "value": "20803"
                    },
                    {
                        "value": "19202"
                    }
                ]
            },
            {
                "seriesname": "Los Angeles Topanga",
                "data": [
                    {
                        "value": "13400"
                    },
                    {
                        "value": "12800"
                    },
                    {
                        "value": "22800"
                    },
                    {
                        "value": "12400"
                    },
                    {
                        "value": "15800"
                    },
                    {
                        "value": "19800"
                    },
                    {
                        "value": "21800"
                    }
                ]
            }
        ],
        "trendlines": [
            {
                "line": [
                    {
                        "startvalue": "17022",
                        "color": "#6baa01",
                        "valueOnRight": "1",
                        "displayvalue": "Average"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart6(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'column2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Monthly revenue for last year",
            "subCaption": "Harry's SuperMart",
            "xAxisName": "Month",
            "yAxisName": "Revenues (In USD)",
            "numberPrefix": "$",
            "paletteColors": "#0075c2",
            "bgColor": "#ffffff",
            "borderAlpha": "20",
            "canvasBorderAlpha": "0",
            "usePlotGradientColor": "0",
            "plotBorderAlpha": "10",
            "placevaluesInside": "1",
            "rotatevalues": "1",
            "valueFontColor": "#ffffff",
            "showXAxisLine": "1",
            "xAxisLineColor": "#999999",
            "divlineColor": "#999999",
            "divLineDashed": "1",
            "showAlternateHGridColor": "0",
            "subcaptionFontBold": "0",
            "subcaptionFontSize": "14"
        },
        "data": [
            {
                "label": "Jan",
                "value": "420000"
            },
            {
                "label": "Feb",
                "value": "810000"
            },
            {
                "label": "Mar",
                "value": "720000"
            },
            {
                "label": "Apr",
                "value": "550000"
            },
            {
                "label": "May",
                "value": "910000"
            },
            {
                "label": "Jun",
                "value": "510000"
            },
            {
                "label": "Jul",
                "value": "680000"
            },
            {
                "label": "Aug",
                "value": "620000"
            },
            {
                "label": "Sep",
                "value": "610000"
            },
            {
                "label": "Oct",
                "value": "490000"
            },
            {
                "label": "Nov",
                "value": "900000"
            },
            {
                "label": "Dec",
                "value": "730000"
            }
        ],
        "trendlines": [
            {
                "line": [
                    {
                        "startvalue": "700000",
                        "color": "#1aaf5d",
                        "valueOnRight": "1",
                        "displayvalue": "Monthly Target"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart7(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'pie2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Split of Visitors by Age Group",
            "subCaption": "Last year",
            "paletteColors": "#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000",
            "bgColor": "#ffffff",
            "showBorder": "0",
            "use3DLighting": "0",
            "showShadow": "0",
            "enableSmartLabels": "0",
            "startingAngle": "0",
            "showPercentValues": "1",
            "showPercentInTooltip": "0",
            "decimals": "1",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "toolTipColor": "#ffffff",
            "toolTipBorderThickness": "0",
            "toolTipBgColor": "#000000",
            "toolTipBgAlpha": "80",
            "toolTipBorderRadius": "2",
            "toolTipPadding": "5",
            "showHoverEffect": "1",
            "showLegend": "1",
            "legendBgColor": "#ffffff",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666",
            "useDataPlotColorForLabels": "1"
        },
        "data": [
            {
                "label": "Teenage",
                "value": "1250400"
            },
            {
                "label": "Adult",
                "value": "1463300"
            },
            {
                "label": "Mid-age",
                "value": "1050700"
            },
            {
                "label": "Senior",
                "value": "491000"
            }
        ]
    }
    }).render();
}
function loadChart8(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msline',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Number of visitors last week",
            "subCaption": "Bakersfield Central vs Los Angeles Topanga",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "paletteColors": "#0075c2,#1aaf5d",
            "bgcolor": "#ffffff",
            "showBorder": "0",
            "showShadow": "0",
            "showCanvasBorder": "0",
            "usePlotGradientColor": "0",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "showAxisLines": "0",
            "showAlternateHGridColor": "0",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "xAxisName": "Day",
            "showValues": "0"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Mon"
                    },
                    {
                        "label": "Tue"
                    },
                    {
                        "label": "Wed"
                    },
                    {
                        "vline": "true",
                        "lineposition": "0",
                        "color": "#6baa01",
                        "labelHAlign": "center",
                        "labelPosition": "0",
                        "label": "National holiday",
                        "dashed": "1"
                    },
                    {
                        "label": "Thu"
                    },
                    {
                        "label": "Fri"
                    },
                    {
                        "label": "Sat"
                    },
                    {
                        "label": "Sun"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "Bakersfield Central",
                "data": [
                    {
                        "value": "15123"
                    },
                    {
                        "value": "14233"
                    },
                    {
                        "value": "25507"
                    },
                    {
                        "value": "9110"
                    },
                    {
                        "value": "15529"
                    },
                    {
                        "value": "20803"
                    },
                    {
                        "value": "19202"
                    }
                ]
            },
            {
                "seriesname": "Los Angeles Topanga",
                "data": [
                    {
                        "value": "13400"
                    },
                    {
                        "value": "12800"
                    },
                    {
                        "value": "22800"
                    },
                    {
                        "value": "12400"
                    },
                    {
                        "value": "15800"
                    },
                    {
                        "value": "19800"
                    },
                    {
                        "value": "21800"
                    }
                ]
            }
        ],
        "trendlines": [
            {
                "line": [
                    {
                        "startvalue": "17022",
                        "color": "#6baa01",
                        "valueOnRight": "1",
                        "displayvalue": "Average"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart9(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msline',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Number of visitors last week",
            "subCaption": "Bakersfield Central vs Los Angeles Topanga",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "paletteColors": "#0075c2,#1aaf5d",
            "bgcolor": "#ffffff",
            "showBorder": "0",
            "showShadow": "0",
            "showCanvasBorder": "0",
            "usePlotGradientColor": "0",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "showAxisLines": "0",
            "showAlternateHGridColor": "0",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "xAxisName": "Day",
            "showValues": "0"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Mon"
                    },
                    {
                        "label": "Tue"
                    },
                    {
                        "label": "Wed"
                    },
                    {
                        "vline": "true",
                        "lineposition": "0",
                        "color": "#6baa01",
                        "labelHAlign": "center",
                        "labelPosition": "0",
                        "label": "National holiday",
                        "dashed": "1"
                    },
                    {
                        "label": "Thu"
                    },
                    {
                        "label": "Fri"
                    },
                    {
                        "label": "Sat"
                    },
                    {
                        "label": "Sun"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "Bakersfield Central",
                "data": [
                    {
                        "value": "15123"
                    },
                    {
                        "value": "14233"
                    },
                    {
                        "value": "25507"
                    },
                    {
                        "value": "9110"
                    },
                    {
                        "value": "15529"
                    },
                    {
                        "value": "20803"
                    },
                    {
                        "value": "19202"
                    }
                ]
            },
            {
                "seriesname": "Los Angeles Topanga",
                "data": [
                    {
                        "value": "13400"
                    },
                    {
                        "value": "12800"
                    },
                    {
                        "value": "22800"
                    },
                    {
                        "value": "12400"
                    },
                    {
                        "value": "15800"
                    },
                    {
                        "value": "19800"
                    },
                    {
                        "value": "21800"
                    }
                ]
            }
        ],
        "trendlines": [
            {
                "line": [
                    {
                        "startvalue": "17022",
                        "color": "#6baa01",
                        "valueOnRight": "1",
                        "displayvalue": "Average"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart10(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify(eval("[" + datas.data1 + "]")));
    var data2 = JSON.parse(JSON.stringify(eval("[" + datas.data2 + "]")));
    var data3 = JSON.parse(JSON.stringify(eval("[" + datas.data3 + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'stackedcolumn3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "xAxisname": "",
                "numberPrefix": "",
                "paletteColors": color,
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "showCanvasBorder": "0",
                "usePlotGradientColor": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#ffffff",
                "showXAxisLine": "1",
                "legendBgColor": "#ffffff",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect": "1"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname1,
                    "data": data1
                },
                {
                    "seriesname": datas.seriesname2,
                    "data": data2
                },
                {
                    "seriesname": datas.seriesname3,
                    "data": data3
                }
            ]
        }
    }).render();
}
function loadChart11(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify( eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "caption": "",
                "subCaption": "",
                "xAxisname": "",
                "numberPrefix": "",
                "paletteColors": color,
                "bgColor": "#ffffff",
                "borderAlpha": "20",
                "legendBgColor": "#ffffff",
                "showCanvasBorder": "0",
                "plotBorderAlpha": "10",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "valueFontColor": "#5a738e",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666",
                "showXAxisLine": "1",
                "xAxisLineColor": "#999999",
                "divlineColor": "#999999",
                "divLineDashed": "1",
                "showAlternateHGridColor": "0",
                "subcaptionFontBold": "0",
                "subcaptionFontSize": "14",
                "showHoverEffect": "1",
                //Setting gradient fill to true
                "usePlotGradientColor": "1",
                //Setting the gradient formation color
                "plotGradientColor": "#1aaf5d"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": 'Balance',
                    "data": data
                }
            ]
        }
    }).render();
}
function loadChart12(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify(eval("[" + datas.data1 + "]")));
    var data2 = JSON.parse(JSON.stringify(eval("[" + datas.data2 + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'stackedbar3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "numberPrefix": "",
            "paletteColors": color,
            "bgColor": "#ffffff",
            "borderAlpha": "20",
            "showCanvasBorder": "0",
            "usePlotGradientColor": "0",
            "plotBorderAlpha": "10",
            "legendBgColor": "#ffffff",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "valueFontColor": "#ffffff",
            "xAxisLineColor": "#999999",
            "divlineColor": "#999999",
            "subcaptionFontBold": "0",
            "subcaptionFontSize": "14"
            },
            "categories": [
                {
                    "category": category
                }
            ],
            "dataset": [
                {
                    "seriesname": datas.seriesname1,
                    "data": data1
                },
                {
                    "seriesname": datas.seriesname2,
                    "data": data2
                }
            ]
    }
    }).render();
}
function loadChart13(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify(eval("[" + datas.data1 + "]")));
    var data2 = JSON.parse(JSON.stringify(eval("[" + datas.data2 + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscombi2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "numberPrefix": "",
            "showBorder": "0",
            "showValues": "0",
            "paletteColors": color,
            "bgColor": "#ffffff",
            "showCanvasBorder": "0",
            "canvasBgColor": "#ffffff",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "divlineColor": "#999999",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "showAlternateHGridColor": "0",
            "usePlotGradientColor": "0",
            "toolTipColor": "#ffffff",
            "toolTipBorderThickness": "0",
            "toolTipBgColor": "#000000",
            "toolTipBgAlpha": "80",
            "toolTipBorderRadius": "2",
            "toolTipPadding": "5",
            "legendBgColor": "#ffffff",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666",
                //Setting gradient fill to true
                "usePlotGradientColor": "1",
                //Setting the gradient formation color
                "plotGradientColor": "#1aaf5d"
        },
        "categories": [
            {
                "category": category
            }
        ],
        "dataset": [
            {
                "seriesName": "Target",
                "renderAs": "line",
                "data": [
                    {
                        "value": "2"
                    },
                    {
                        "value": "2"
                    },
                    {
                        "value": "2"
                    },
                    {
                        "value": "2"
                    }
                ]
            },
            {
                "seriesName": "On Time",
                "renderAs": "area",
                "data": data1
            }
        ]
    }
    }).render();
}
function loadChart131(data) {
    var datas = JSON.parse(data);
    var data1 = JSON.parse(JSON.stringify(eval("[" + datas.data1 + "]")));
    var data2 = JSON.parse(JSON.stringify(eval("[" + datas.data2 + "]")));
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fekuhs')).substr(4, 1) + dechex(crc32(value.label+'fe254s')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'msline',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "numberPrefix": "",
            "showBorder": "0",
            "showValues": "0",
            "paletteColors": color,
            "bgColor": "#ffffff",
            "showCanvasBorder": "0",
            "canvasBgColor": "#ffffff",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "divlineColor": "#999999",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "showAlternateHGridColor": "0",
            "usePlotGradientColor": "0",
            "toolTipColor": "#ffffff",
            "toolTipBorderThickness": "0",
            "toolTipBgColor": "#000000",
            "toolTipBgAlpha": "80",
            "toolTipBorderRadius": "2",
            "toolTipPadding": "5",
            "legendBgColor": "#ffffff",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666",
                //Setting gradient fill to true
                "usePlotGradientColor": "1",
                //Setting the gradient formation color
                "plotGradientColor": "#1aaf5d"
        },
        "categories": [
            {
                "category": category
            }
        ],
        "dataset": [
            {
                "seriesName": "Target",
                "renderAs": "line",
                "data": [
                    {
                        "value": "2"
                    },
                    {
                        "value": "2"
                    },
                    {
                        "value": "2"
                    },
                    {
                        "value": "2"
                    }
                ]
            },
            {
                "seriesName": "On Time",
                "renderAs": "area",
                "data": data1
            }
        ]
    }
    }).render();
}
function loadChart14(data) {
    var datas = JSON.parse(data);
//    var data = JSON.parse(JSON.stringify(datas.data));
////    alert(data.Terminate);
//    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
//    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
////    alert(dataset);
//    var color = '!';
//    var array = $.map(category, function(value, index) {
//        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
//    });
//    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource:  {
            "chart": {
                "caption": "New Rental ("+datas.new_rental+") / Rental ("+datas.all_rental+")",
                "subcaption": "Units",
                "lowerLimit": "0",
                "upperLimit": "100",
                "showGaugeBorder": "1",
                "gaugeBorderColor": "{dark-50}",
                "gaugeBorderThickness": "4",
                "gaugeBorderAlpha": "100"
            },
            "colorRange": {
                "color": [
                    {
                        "minValue": "0",
                        "maxValue": "45",
                        "code": "#e44a00"
                    },
                    {
                        "minValue": "45",
                        "maxValue": "75",
                        "code": "#f8bd19"
                    },
                    {
                        "minValue": "75",
                        "maxValue": "100",
                        "code": "#6baa01"
                    }
                ]
            },
            "dials": {
                "dial": [
                    {
                        "value": datas.value*100,
                    }
                ]
            },
        }
    }).render();
}
function loadChart15(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify( eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "showvalues": "0",
                "numberprefix": "",
                "showBorder": "0",
                "paletteColors": "#1aaf5d,#f2c500",
                "bgColor": "#ffffff",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Quarter 1"
                        },
                        {
                            "label": "Quarter 2"
                        },
                        {
                            "label": "Quarter 3"
                        },
                        {
                            "label": "Quarter 4"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Profit and Loss",
                    "data": [
                        {
                            "value": "235000"
                        },
                        {
                            "value": "225100"
                        },
                        {
                            "value": "222000"
                        },
                        {
                            "value": "230500"
                        }
                    ]
                },
                {
                    "seriesname": "Balance Sheet",
                    "data": [
                        {
                            "value": "230000"
                        },
                        {
                            "value": "143000"
                        },
                        {
                            "value": "198000"
                        },
                        {
                            "value": "327600"
                        }
                    ]
                },
            ]
        }
    }).render();
}
        
function loadChart16(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify( eval("[" + datas.data + "]")));
    var category = JSON.parse(JSON.stringify( eval("[" + datas.category + "]")));
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label)).substr(4, 1) + dechex(crc32(value.label)).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn3d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
                "numberPrefix": "",
                "showBorder": "0",
                "showValues": "0",
                "paletteColors": "#0075c2,#1aaf5d,#f2c500",
                "bgColor": "#ffffff",
                "showCanvasBorder": "0",
                "canvasBgColor": "#ffffff",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "divlineColor": "#999999",
                "divLineIsDashed": "1",
                "divLineDashLen": "1",
                "showAlternateHGridColor": "0",
                "usePlotGradientColor": "0",
                "toolTipColor": "#ffffff",
                "toolTipBorderThickness": "0",
                "toolTipBgColor": "#000000",
                "toolTipBgAlpha": "80",
                "toolTipBorderRadius": "2",
                "toolTipPadding": "5",
                "legendBgColor": "#ffffff",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Jan"
                        },
                        {
                            "label": "Feb"
                        },
                        {
                            "label": "Mar"
                        },
                        {
                            "label": "Apr"
                        },
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesName": "Motor Vehicle",
                    "showValues": "1",
                    "data": [
                        {
                            "value": "16000"
                        },
                        {
                            "value": "20000"
                        },
                        {
                            "value": "18000"
                        },
                        {
                            "value": "19000"
                        },
                    ]
                },
                {
                    "seriesName": "Sparepart And Others",
                    "renderAs": "line",
                    "data": [
                        {
                            "value": "15000"
                        },
                        {
                            "value": "16000"
                        },
                        {
                            "value": "17000"
                        },
                        {
                            "value": "18000"
                        },
                    ]
                }
            ]
        }
    }).render();
}
function loadChart17(data) {
    var datas = JSON.parse(data);
//    var data = JSON.parse(JSON.stringify(datas.data));
////    alert(data.Terminate);
//    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
//    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
////    alert(dataset);
//    var color = '!';
//    var array = $.map(category, function(value, index) {
//        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
//    });
//    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
//            "caption": "Jatuh Tempo ("+datas.jatuh_tempo+") / GT & EXT ("+datas.gt_ext+")",
//            "subcaption": "Units",
            "plotToolText": "Current Score: $value",
            "theme": "fint",
            "numberprefix": "%",
            "chartBottomMargin": "50",
            "gaugeBorderColor": "{dark-30}",
            "gaugeBorderThickness": "5",
            "gaugeBorderAlpha": "100",
            "showValue": "1",
            "valueBelowPivot": "1",
            "bgcolor": "FFFFFF",
            "pivotRadius": "0",
            "showborder": "0"
//            "manageresize": "1",
//            "origw": "400",
//            "origh": "250",
//            "managevalueoverlapping": "1",
//            "autoaligntickvalues": "1",
//            "fillangle": "45",
//            "upperlimit": "1",
//            "lowerlimit": "0",
//            "majortmnumber": "10",
//            "majortmheight": "8",
//            "showgaugeborder": "0",
//            "gaugeouterradius": "140",
//            "charttopmargin": "0",
//            "gaugeoriginx": "205",
//            "gaugeoriginy": "206",
//            "gaugeinnerradius": "2",
//            "formatnumberscale": "1",
//            "decmials": "2",
//            "tickmarkdecimals": "1",
//            "pivotradius": "10",
//            "showpivotborder": "1",
//            "pivotbordercolor": "000000",
//            "pivotborderthickness": "10",
//            "pivotfillmix": "666666",
//            "tickvaluedistance": "10",
//            "valuebelowpivot": "1",
//            "showvalue": "1",
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "0",
                    "maxvalue": "20",
                    "code": "6baa01",
                    "borderColor": "#409C40",
                    "borderAlpha": "100"
                },
                {
                    "minvalue": "20",
                    "maxvalue": "60",
                    "code": "f8bd19",
                    "borderColor": "#696840",
                    "borderAlpha": "100"
                },
                {
                    "minvalue": "60",
                    "maxvalue": "100",
                    "code": "e44a00",
                    "borderColor": "#780101",
                    "borderAlpha": "100"
                }
            ]
        },
        "dials": {
            "dial": [
                {
                    "value": datas.value,
//                    "borderalpha": "0",
//                    "bgcolor": "000000",
//                    "basewidth": "20",
//                    "topwidth": "1",
//                    "radius": "130"
                }
            ]
        },
    }
    }).render();
}
function loadChart18(data) {
    var datas = JSON.parse(data);
//    var data = JSON.parse(JSON.stringify(datas.data));
////    alert(data.Terminate);
//    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
//    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
////    alert(dataset);
//    var color = '!';
//    var array = $.map(category, function(value, index) {
//        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
//    });
//    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Rented Average ("+Math.round(datas.value * 100) / 100+")",
            "subcaption": "Rupiah (Milliar)",
            "theme": "fint",
            "chartBottomMargin": "50",
            "showGaugeBorder": "1",
            "gaugeBorderThickness": "5",
            "showValue": "1",
            "valueBelowPivot": "1",
            "bgcolor": "FFFFFF",
            "pivotRadius": "0",
            "showborder": "0"
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "3",
                    "maxvalue": "5",
                    "code": "e44a00"
                },
                {
                    "minvalue": "5",
                    "maxvalue": "7",
                    "code": "f8bd19"
                },
                {
                    "minvalue": "8",
                    "maxvalue": "10",
                    "code": "6baa01"
                }
            ]
        },
        "dials": {
            "dial": [
                {
                    "value": datas.value,
                    "borderalpha": "0",
                    "bgcolor": "000000",
                    "basewidth": "20",
                    "topwidth": "1",
                    "radius": "130"
                }
            ]
        },
        "annotations": {
            "groups": [
                {
                    "x": "205",
                    "y": "207.5",
                    "items": [
                        {
                            "type": "circle",
                            "x": "0",
                            "y": "2.5",
                            "radius": "150",
                            "startangle": "0",
                            "endangle": "180",
                            "fillpattern": "linear",
                            "fillasgradient": "1",
                            "fillcolor": "dddddd,666666",
                            "fillalpha": "100,100",
                            "fillratio": "50,50",
                            "fillangle": "0",
                            "showborder": "1",
                            "bordercolor": "444444",
                            "borderthickness": "2"
                        },
                        {
                            "type": "circle",
                            "x": "0",
                            "y": "0",
                            "radius": "145",
                            "startangle": "0",
                            "endangle": "180",
                            "fillpattern": "linear",
                            "fillasgradient": "1",
                            "fillcolor": "666666,ffffff",
                            "fillalpha": "100,100",
                            "fillratio": "50,50",
                            "fillangle": "0"
                        }
                    ]
                }
            ]
        }
    }
    }).render();
}
function loadChart19(data) {
    var datas = JSON.parse(data);
    var visitChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "caption": "Idle Average ("+Math.round(datas.value * 100) / 100+")",
            "subcaption": "Days",
            "theme": "fint",
            "chartBottomMargin": "50",
            "showGaugeBorder": "1",
            "gaugeBorderThickness": "5",
            "showValue": "1",
            "valueBelowPivot": "1",
            "bgcolor": "FFFFFF",
            "pivotRadius": "0",
            "showborder": "0"
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "0",
                    "maxvalue": "300",
                    "code": "e44a00"
                },
                {
                    "minvalue": "300",
                    "maxvalue": "550",
                    "code": "f8bd19"
                },
                {
                    "minvalue": "550",
                    "maxvalue": "1000",
                    "code": "6baa01"
                }
            ]
        },
        "dials": {
            "dial": [
                {
                    "value": datas.value,
                    "borderalpha": "0",
                    "bgcolor": "000000",
                    "basewidth": "20",
                    "topwidth": "1",
                    "radius": "130"
                }
            ]
        },
        "annotations": {
            "groups": [
                {
                    "x": "205",
                    "y": "207.5",
                    "items": [
                        {
                            "type": "circle",
                            "x": "0",
                            "y": "2.5",
                            "radius": "150",
                            "startangle": "0",
                            "endangle": "180",
                            "fillpattern": "linear",
                            "fillasgradient": "1",
                            "fillcolor": "dddddd,666666",
                            "fillalpha": "100,100",
                            "fillratio": "50,50",
                            "fillangle": "0",
                            "showborder": "1",
                            "bordercolor": "444444",
                            "borderthickness": "2"
                        },
                        {
                            "type": "circle",
                            "x": "0",
                            "y": "0",
                            "radius": "145",
                            "startangle": "0",
                            "endangle": "180",
                            "fillpattern": "linear",
                            "fillasgradient": "1",
                            "fillcolor": "666666,ffffff",
                            "fillalpha": "100,100",
                            "fillratio": "50,50",
                            "fillangle": "0"
                        }
                    ]
                }
            ]
        }
    }
    }).render();
}
function loadChart201(data) {
    var datas = JSON.parse(data);
//    var data = JSON.parse(JSON.stringify(datas.data));
////    alert(data.Terminate);
//    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
//    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
////    alert(dataset);
//    var color = '!';
//    var array = $.map(category, function(value, index) {
//        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
//    });
//    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
//            "caption": "Comparison of Quarterly Revenue",
            "xAxisname": "Rp (Juta)",
//            "yAxisName": "Revenues (In USD)",
            "numberPrefix": "",
            "plotFillAlpha": "80",
            "paletteColors": "#0075c2,#1aaf5d,#1a335d,#1a115d",
            "baseFontColor": "#333333",
            "baseFont": "Helvetica Neue,Arial",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "showBorder": "0",
            "bgColor": "#ffffff",
            "showShadow": "0",
            "canvasBgColor": "#ffffff",
            "canvasBorderAlpha": "0",
            "divlineAlpha": "100",
            "divlineColor": "#999999",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "usePlotGradientColor": "0",
            "showplotborder": "0",
            "valueFontColor": "#ffffff",
            "placeValuesInside": "1",
            "showHoverEffect": "1",
            "rotateValues": "1",
            "showXAxisLine": "1",
            "xAxisLineThickness": "1",
            "xAxisLineColor": "#999999",
            "showAlternateHGridColor": "0",
            "legendBgAlpha": "0",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Q1"
                    },
                    {
                        "label": "Q2"
                    },
                    {
                        "label": "Q3"
                    },
                    {
                        "label": "Q4"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "New Cars",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
            {
                "seriesname": "Used Cars",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
        ],
        "trendlines": [
            {
                "line": [
                    {
                        "startvalue": "12250",
                        "color": "#0075c2",
                        "displayvalue": "Previous{br}Average",
                        "valueOnRight": "1",
                        "thickness": "1",
                        "showBelow": "1",
                        "tooltext": "Previous year quarterly target  : $13.5K"
                    },
                    {
                        "startvalue": "25950",
                        "color": "#1aaf5d",
                        "displayvalue": "Current{br}Average",
                        "valueOnRight": "1",
                        "thickness": "1",
                        "showBelow": "1",
                        "tooltext": "Current year quarterly target  : $23K"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart201(data) {
    var datas = JSON.parse(data);
//    var data = JSON.parse(JSON.stringify(datas.data));
////    alert(data.Terminate);
//    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
//    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
////    alert(dataset);
//    var color = '!';
//    var array = $.map(category, function(value, index) {
//        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
//    });
//    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
//            "caption": "Comparison of Quarterly Revenue",
//            "xAxisname": "Rp (Juta)",
//            "yAxisName": "Revenues (In USD)",
            "numberPrefix": "",
            "plotFillAlpha": "80",
            "paletteColors": "#0075c2,#1aaf5d,#1a335d,#1a115d",
            "baseFontColor": "#333333",
            "baseFont": "Helvetica Neue,Arial",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "showBorder": "0",
            "bgColor": "#ffffff",
            "showShadow": "0",
            "canvasBgColor": "#ffffff",
            "canvasBorderAlpha": "0",
            "divlineAlpha": "100",
            "divlineColor": "#999999",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "usePlotGradientColor": "0",
            "showplotborder": "0",
            "valueFontColor": "#ffffff",
            "placeValuesInside": "1",
            "showHoverEffect": "1",
            "rotateValues": "1",
            "showXAxisLine": "1",
            "xAxisLineThickness": "1",
            "xAxisLineColor": "#999999",
            "showAlternateHGridColor": "0",
            "legendBgAlpha": "0",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Q1"
                    },
                    {
                        "label": "Q2"
                    },
                    {
                        "label": "Q3"
                    },
                    {
                        "label": "Q4"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "New Customers",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
            {
                "seriesname": "Existing Customers",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
        ],
        "trendlines": [
            {
                "line": [
                    {
                        "startvalue": "12250",
                        "color": "#0075c2",
                        "displayvalue": "Previous{br}Average",
                        "valueOnRight": "1",
                        "thickness": "1",
                        "showBelow": "1",
                        "tooltext": "Previous year quarterly target  : $13.5K"
                    },
                    {
                        "startvalue": "25950",
                        "color": "#1aaf5d",
                        "displayvalue": "Current{br}Average",
                        "valueOnRight": "1",
                        "thickness": "1",
                        "showBelow": "1",
                        "tooltext": "Current year quarterly target  : $23K"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart20(data) {
    var datas = JSON.parse(data);
//    var data = JSON.parse(JSON.stringify(datas.data));
////    alert(data.Terminate);
//    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
//    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
////    alert(dataset);
//    var color = '!';
//    var array = $.map(category, function(value, index) {
//        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
//    });
//    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
//            "caption": "Comparison of Quarterly Revenue",
            "xAxisname": "Rp (Juta)",
//            "yAxisName": "Revenues (In USD)",
            "numberPrefix": "",
            "plotFillAlpha": "80",
            "paletteColors": "#0075c2,#1aaf5d,#1a335d,#1a115d",
            "baseFontColor": "#333333",
            "baseFont": "Helvetica Neue,Arial",
            "captionFontSize": "14",
            "subcaptionFontSize": "14",
            "subcaptionFontBold": "0",
            "showBorder": "0",
            "bgColor": "#ffffff",
            "showShadow": "0",
            "canvasBgColor": "#ffffff",
            "canvasBorderAlpha": "0",
            "divlineAlpha": "100",
            "divlineColor": "#999999",
            "divlineThickness": "1",
            "divLineDashed": "1",
            "divLineDashLen": "1",
            "usePlotGradientColor": "0",
            "showplotborder": "0",
            "valueFontColor": "#ffffff",
            "placeValuesInside": "1",
            "showHoverEffect": "1",
            "rotateValues": "1",
            "showXAxisLine": "1",
            "xAxisLineThickness": "1",
            "xAxisLineColor": "#999999",
            "showAlternateHGridColor": "0",
            "legendBgAlpha": "0",
            "legendBorderAlpha": "0",
            "legendShadow": "0",
            "legendItemFontSize": "10",
            "legendItemFontColor": "#666666"
        },
        "categories": [
            {
                "category": [
                    {
                        "label": "Q1"
                    },
                    {
                        "label": "Q2"
                    },
                    {
                        "label": "Q3"
                    },
                    {
                        "label": "Q4"
                    }
                ]
            }
        ],
        "dataset": [
            {
                "seriesname": "Luxury",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
            {
                "seriesname": "Passanger",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
            {
                "seriesname": "Mining",
                "data": [
                    {
                        "value": "10000"
                    },
                    {
                        "value": "11500"
                    },
                    {
                        "value": "12500"
                    },
                    {
                        "value": "15000"
                    }
                ]
            },
            {
                "seriesname": "Commercial",
                "data": [
                    {
                        "value": "25400"
                    },
                    {
                        "value": "29800"
                    },
                    {
                        "value": "21800"
                    },
                    {
                        "value": "26800"
                    }
                ]
            }
        ],
        "trendlines": [
            {
                "line": [
                    {
                        "startvalue": "12250",
                        "color": "#0075c2",
                        "displayvalue": "Previous{br}Average",
                        "valueOnRight": "1",
                        "thickness": "1",
                        "showBelow": "1",
                        "tooltext": "Previous year quarterly target  : $13.5K"
                    },
                    {
                        "startvalue": "25950",
                        "color": "#1aaf5d",
                        "displayvalue": "Current{br}Average",
                        "valueOnRight": "1",
                        "thickness": "1",
                        "showBelow": "1",
                        "tooltext": "Current year quarterly target  : $23K"
                    }
                ]
            }
        ]
    }
    }).render();
}
function loadChart21(data) {
    var datas = JSON.parse(data);
//    var data = JSON.parse(JSON.stringify(datas.data));
////    alert(data.Terminate);
//    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
//    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
////    alert(dataset);
//    var color = '!';
//    var array = $.map(category, function(value, index) {
//        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
//    });
//    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
            "chart": {
//                "caption": "Comparison of Quarterly Revenue",
                "xAxisname": "Months",
//                "yAxisName": "Revenues (In USD)",
                "numberPrefix": "",
                "plotFillAlpha": "80",
            "paletteColors": "#0075c2,#1aaf5d,#1a335d,#1a115d",
                "baseFontColor": "#333333",
                "baseFont": "Helvetica Neue,Arial",
                "captionFontSize": "14",
                "subcaptionFontSize": "14",
                "subcaptionFontBold": "0",
                "showBorder": "0",
                "bgColor": "#ffffff",
                "showShadow": "0",
                "canvasBgColor": "#ffffff",
                "canvasBorderAlpha": "0",
                "divlineAlpha": "100",
                "divlineColor": "#999999",
                "divlineThickness": "1",
                "divLineDashed": "1",
                "divLineDashLen": "1",
                "usePlotGradientColor": "0",
                "showplotborder": "0",
                "valueFontColor": "#ffffff",
                "placeValuesInside": "1",
                "showHoverEffect": "1",
                "rotateValues": "1",
                "showXAxisLine": "1",
                "xAxisLineThickness": "1",
                "xAxisLineColor": "#999999",
                "showAlternateHGridColor": "0",
                "legendBgAlpha": "0",
                "legendBorderAlpha": "0",
                "legendShadow": "0",
                "legendItemFontSize": "10",
                "legendItemFontColor": "#666666"
            },
            "categories": [
                {
                    "category": [
                        {
                            "label": "Q1"
                        },
                        {
                            "label": "Q2"
                        },
                        {
                            "label": "Q3"
                        },
                        {
                            "label": "Q4"
                        }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "Luxury",
                    "data": [
                        {
                            "value": "10000"
                        },
                        {
                            "value": "11500"
                        },
                        {
                            "value": "12500"
                        },
                        {
                            "value": "15000"
                        }
                    ]
                },
                {
                    "seriesname": "Passanger",
                    "data": [
                        {
                            "value": "10000"
                        },
                        {
                            "value": "11500"
                        },
                        {
                            "value": "12500"
                        },
                        {
                            "value": "15000"
                        }
                    ]
                },
                {
                    "seriesname": "Mining",
                    "data": [
                        {
                            "value": "10000"
                        },
                        {
                            "value": "11500"
                        },
                        {
                            "value": "12500"
                        },
                        {
                            "value": "15000"
                        }
                    ]
                },
                {
                    "seriesname": "Commercial",
                    "data": [
                        {
                            "value": "25400"
                        },
                        {
                            "value": "29800"
                        },
                        {
                            "value": "21800"
                        },
                        {
                            "value": "26800"
                        }
                    ]
                }
            ],
            "trendlines": [
                {
                    "line": [
                        {
                            "startvalue": "12250",
                            "color": "#0075c2",
                            "displayvalue": "Previous{br}Average",
                            "valueOnRight": "1",
                            "thickness": "1",
                            "showBelow": "1",
                            "tooltext": "Previous year quarterly target  : $13.5K"
                        },
                        {
                            "startvalue": "25950",
                            "color": "#1aaf5d",
                            "displayvalue": "Current{br}Average",
                            "valueOnRight": "1",
                            "thickness": "1",
                            "showBelow": "1",
                            "tooltext": "Current year quarterly target  : $23K"
                        }
                    ]
                }
            ]
        }
    }).render();
}
function loadChart22(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'angulargauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
//            "caption": "Revenue - 2013",
            "subcaption": "(luxury, passenger, mining, commercial)",
            "theme": "fint",
            "chartBottomMargin": "50",
            "showGaugeBorder": "1",
            "gaugeBorderThickness": "5",
            "showValue": "1",
            "valueBelowPivot": "1",
            "bgcolor": "FFFFFF",
            "pivotRadius": "0",
            "showborder": "0"
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "1600000",
                    "maxvalue": "1930000",
                    "code": "e44a00"
                },
                {
                    "minvalue": "1930000",
                    "maxvalue": "2170000",
                    "code": "f8bd19"
                },
                {
                    "minvalue": "2170000",
                    "maxvalue": "2500000",
                    "code": "6baa01"
                }
            ]
        },
        "dials": {
            "dial": [
                {
                    "value": "2100000",
                    "borderalpha": "0",
                    "bgcolor": "000000",
                    "basewidth": "20",
                    "topwidth": "1",
                    "radius": "130"
                }
            ]
        },
        "annotations": {
            "groups": [
                {
                    "x": "205",
                    "y": "207.5",
                    "items": [
                        {
                            "type": "circle",
                            "x": "0",
                            "y": "2.5",
                            "radius": "150",
                            "startangle": "0",
                            "endangle": "180",
                            "fillpattern": "linear",
                            "fillasgradient": "1",
                            "fillcolor": "dddddd,666666",
                            "fillalpha": "100,100",
                            "fillratio": "50,50",
                            "fillangle": "0",
                            "showborder": "1",
                            "bordercolor": "444444",
                            "borderthickness": "2"
                        },
                        {
                            "type": "circle",
                            "x": "0",
                            "y": "0",
                            "radius": "145",
                            "startangle": "0",
                            "endangle": "180",
                            "fillpattern": "linear",
                            "fillasgradient": "1",
                            "fillcolor": "666666,ffffff",
                            "fillalpha": "100,100",
                            "fillratio": "50,50",
                            "fillangle": "0"
                        }
                    ]
                }
            ]
        }
    }
    }).render();
}
function loadChart23(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'HLinearGauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "manageresize": "1",
            "bgcolor": "FFFFFF",
            "bgalpha": "0",
            "showborder": "0",
            "upperlimit": "100",
            "lowerlimit": "0",
            "gaugeroundradius": "5",
            "chartbottommargin": "10",
            "ticksbelowgauge": "0",
            "showgaugelabels": "1",
            "valueabovepointer": "0",
            "pointerontop": "1",
            "pointerradius": "9",
            "numberprefix": "$"
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "0",
                    "maxvalue": "35",
                    "label": "Low"
                },
                {
                    "minvalue": "35",
                    "maxvalue": "70",
                    "label": "Moderate"
                },
                {
                    "minvalue": "70",
                    "maxvalue": "100",
                    "label": "High"
                }
            ]
        },
        "pointers": {
            "pointer": [
                {
                    "value": "27.5"
                }
            ]
        }
    }
    }).render();
}


function loadChart201a(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'HLinearGauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "manageresize": "1",
            "bgcolor": "FFFFFF",
            "bgalpha": "0",
            "showborder": "0",
            "upperlimit": "500",
            "lowerlimit": "120",
            "gaugeroundradius": "5",
            "chartbottommargin": "10",
            "pointerOnTop": "1",
            "ticksbelowgauge": "0",
            "showgaugelabels": "1",
            "showTickValues": "0",
//            "valueabovepointer": "0",
//            "pointerontop": "1",
//            "pointerradius": "9",
            "numberprefix": ""
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "125",
                    "maxvalue": "150",
                    "label": "Q1",
                    "showValue": "1",
                },
                {
                    "minvalue": "150",
                    "maxvalue": "200",
                    "label": "Q2"
                },
                {
                    "minvalue": "200",
                    "maxvalue": "250",
                    "label": "Q3"
                },
                {
                    "minvalue": "300",
                    "maxvalue": "500",
                    "label": "Q4"
                },
            ]
        },
        "trendPoints": {
            "point": [
                {
                    "startValue": "150",
                    "color": "#cccccc",
                    "dashed": "1",
                    "dashlen": "3",
                    "dashgap": "3",
                    "thickness": "2"
                },
                {
                    "startValue": "200",
                    "color": "#cccccc",
                    "dashed": "1",
                    "dashlen": "3",
                    "dashgap": "3",
                    "thickness": "2"
                },
                {
                    "startValue": "250",
                    "color": "#cccccc",
                    "dashed": "1",
                    "dashlen": "3",
                    "dashgap": "3",
                    "thickness": "2"
                },
                {
                    "startValue": "500",
                    "color": "#cccccc",
                    "dashed": "1",
                    "dashlen": "3",
                    "dashgap": "3",
                    "thickness": "2"
                }
            ]
        },
        "pointers": {
            "pointer": [
                {
                    "borderColor": "#333333",
                    "borderThickness": "2",
                    "borderAlpha": "60",
                    "bgColor": "#0075c2",
                    "bgAlpha": "75",
                    "radius": "6",
                    "sides": "4",
                    "value": "120"
                }
            ]
        }
    }
    }).render();
}
function loadChart201b(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'HLinearGauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "manageresize": "1",
            "bgcolor": "FFFFFF",
            "bgalpha": "0",
            "showborder": "0",
            "upperlimit": "100",
            "lowerlimit": "0",
            "gaugeroundradius": "5",
            "chartbottommargin": "10",
            "ticksbelowgauge": "0",
            "showgaugelabels": "1",
            "showValue": "0",
//            "valueabovepointer": "0",
//            "pointerontop": "1",
//            "pointerradius": "9",
            "numberprefix": "%"
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "0",
                    "maxvalue": "35",
                    "label": "New Cust"
                },
                {
                    "minvalue": "35",
                    "maxvalue": "100",
                    "label": "Exist Cust"
                },
            ]
        },
    }
    }).render();
}
function loadChart201c(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'HLinearGauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "manageresize": "1",
            "bgcolor": "FFFFFF",
            "bgalpha": "0",
            "showborder": "0",
            "upperlimit": "100",
            "lowerlimit": "0",
            "gaugeroundradius": "5",
            "chartbottommargin": "10",
            "ticksbelowgauge": "0",
            "showgaugelabels": "1",
            "showValue": "0",
//            "valueabovepointer": "0",
//            "pointerontop": "1",
//            "pointerradius": "9",
            "numberprefix": "%"
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "0",
                    "maxvalue": "25",
                    "label": "Fast Moving"
                },
                {
                    "minvalue": "25",
                    "maxvalue": "100",
                    "label": "Slow Moving"
                },
            ]
        },
    }
    }).render();
}
function loadChart201d(data) {
    var datas = JSON.parse(data);
    var data = JSON.parse(JSON.stringify(datas.data));
//    alert(data.Terminate);
    var category = JSON.parse(JSON.stringify(eval("[" + datas.category + "]")));
    var Lead = JSON.parse(JSON.stringify(eval("[" + data.Lead + "]")));
//    alert(dataset);
    var color = '!';
    var array = $.map(category, function(value, index) {
        color =  color +  ',' + '#' + dechex(crc32(value.label+'fes')).substr(4, 1) + dechex(crc32(value.label+'fes')).substr(0, 5); 
    });
    color = color.replace('!,', '');
    var visitChart = new FusionCharts({
        type: 'HLinearGauge',
        renderAt: datas.renderAt,
        width: '100%',
        height: datas.height,
        dataFormat: 'json',
        dataSource: {
        "chart": {
            "manageresize": "1",
            "bgcolor": "FFFFFF",
            "bgalpha": "0",
            "showborder": "0",
            "upperlimit": "100",
            "lowerlimit": "0",
                "gaugeroundradius": "5",
                "chartbottommargin": "10",
            "ticksbelowgauge": "0",
            "showgaugelabels": "1",
            "showValue": "0",
//            "valueabovepointer": "0",
//            "pointerontop": "1",
//            "pointerradius": "9",
            "numberprefix": "%"
        },
        "colorrange": {
            "color": [
                {
                    "minvalue": "0",
                    "maxvalue": "25",
                    "label": "Luxury"
                },
                {
                    "minvalue": "25",
                    "maxvalue": "50",
                    "label": "Passanger"
                },
                {
                    "minvalue": "50",
                    "maxvalue": "75",
                    "label": "Mining"
                },
                {
                    "minvalue": "75",
                    "maxvalue": "100",
                    "label": "Commercial"
                },
            ]
        },
    }
    }).render();
}